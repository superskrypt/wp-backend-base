<?php
namespace  Superskrypt\WpBackendBase;


class WpBackendBaseFrontCleaner {
    public static function removeEmbeds() {
		add_action('init', function() {
		    remove_action('rest_api_init', 'wp_oembed_register_route');
			remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
			remove_action('wp_head', 'wp_oembed_add_discovery_links');
			remove_action('wp_head', 'wp_oembed_add_host_js');
		});
	}

	public static function removeEmoji() {
		add_action('init', function() {
		    remove_action('admin_print_styles', 'print_emoji_styles');
		    remove_action('wp_head', 'print_emoji_detection_script', 7);
		    remove_action('admin_print_scripts', 'print_emoji_detection_script');
		    remove_action('wp_print_styles', 'print_emoji_styles');
		    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
		    remove_filter('the_content_feed', 'wp_staticize_emoji');
		    remove_filter('comment_text_rss', 'wp_staticize_emoji');
		});
	}

	public static function removeScripts($scriptsNames) {
		if(empty($scriptsNames)) {
			return;
		}
		add_filter( 'wp_enqueue_scripts', function() use ($scriptsNames) {
			foreach($scriptsNames as $script) {
				wp_dequeue_script($script);
				wp_deregister_script($script);   
			}
		}, PHP_INT_MAX );
		
	}
}
