<?php 

namespace Superskrypt\WpBackendBase;

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Superskrypt\WpBackendBase\WpBackendBaseHelpers;

class ThemeOptions {
    static $optionsContainer = null;
    static protected $textDomain = "";
    static protected $languageCode = "";
    static protected $config = [];
    static protected $fieldsNotTranslated = array('switch_to_pl_label', 'switch_to_en_label'); // slugi pól które nie powinny mieć dodawanego sluga językowego – w każdej wersji językowej powinna byc ta sama wartość
    static protected $activeLanguages = [];

    public static function init($config = []) {
        self::$config = $config;
        self::$languageCode = self::getLanguageCode();
        self::$textDomain = isset($config['text_domain']) ? $config['text_domain'] : WpBackendBase::$backendTextDomain;
        self::$activeLanguages = self::getActiveLanguagesCodes();
        add_action('carbon_fields_register_fields', array(__CLASS__, 'registerThemeOptionPage'));
        
    }

    static public function registerThemeOptionPage () {

        $themeOptionsSets = apply_filters('wp_backend_base_theme_options', self::getThemeOptionFieldsSets(), self::getThemeOptionFieldsSets()) ;
        $themeOptionsFields = WpBackendBaseHelpers::flattenArray($themeOptionsSets);
        self::$optionsContainer = Container::make('theme_options', 'theme_options', __('Website options', self::$textDomain))
            ->set_page_menu_position( 60 )
            ->add_fields(
                $themeOptionsFields
            );
        self::addToTimberContext($themeOptionsFields);
    }

    static public function getTextDomain() {
        return self::$textDomain;
    }

    static public function getLanguageSuffix() {
        return !empty(self::$languageCode) ? '_' . self::$languageCode : "";
    }

    static public function getThemeOptionFieldsSets() {
        $fieldLanguageSuffix = self::getLanguageSuffix();
        return array(
            'skip_links' => array (
                Field::make( 'separator', 'skip_links', __('Skip links (accessibility)', self::$textDomain)),
                Field::make( 'text', 'skip_link_main' . $fieldLanguageSuffix  , __('Text for main skip navigation link', self::$textDomain)),
            ),
            'header' => array(
                Field::make( 'separator', 'header_separator', __('Header', self::$textDomain)),
                Field::make( 'text', 'header_logo_link_label' . $fieldLanguageSuffix   , __('Home logo link label (in header)', self::$textDomain)),

            ),
            'language_switcher' => !empty(self::$languageCode) ? self::generateLanguageSwitcherFields() : array(),
            'menu' => array(
                Field::make( 'separator', 'menu_separator', __('Menu ', self::$textDomain)),
                Field::make( 'text', 'menu_home_link_label' . $fieldLanguageSuffix , __('Home link label (in main menu)', self::$textDomain)),
                Field::make( 'text', 'menu_opened_label' . $fieldLanguageSuffix , __('Label "Open menu" (accessibility)', self::$textDomain) ),
                Field::make( 'text', 'menu_closed_label' . $fieldLanguageSuffix , __('Label "Close menu" (accessibility)', self::$textDomain) ),
            ),
        );
    }

    private static function generateLanguageSwitcherFields() {
        $langSwitcherFields = [Field::make( 'separator', 'language_switcher_separator', __('Language switcher', self::$textDomain))];
        $labels = array_map(function($langCode){
            return  Field::make( 'text', 'switch_to_' . $langCode . '_label' , __('Label: Switch to' , self::$textDomain) . ' ' .$langCode);
        }, self::$activeLanguages);

        return array_merge($langSwitcherFields, $labels);
    }

    static public function getLanguageCode() {
        return apply_filters( 'wpml_current_language', NULL );
    }
    static protected function getActiveLanguagesCodes() {
        $activeLanguagesData = apply_filters( 'wpml_active_languages', null);
        return !empty($activeLanguagesData) ? array_keys($activeLanguagesData) : [];
    }

    public static function getFieldNameWithoutLangSuffix($fieldName) {
        return preg_replace('/' . self::getLanguageSuffix() . '$/', '', $fieldName);
    }
    
    static protected function addToTimberContext($themeOptionsFields) {
        
        add_filter( 'timber/context', function ($context ) use($themeOptionsFields) {
            foreach($themeOptionsFields as $crbFieldInstance) {
                $fieldBaseName = $crbFieldInstance->get_base_name(); // jeśli w projekcie są wersje językowe to ta wartość zawiera suffix językowy np.: skip_link_main_en
                if(in_array($fieldBaseName, self::$fieldsNotTranslated)) {
                    $context[str_replace('-','_', $fieldBaseName)] = carbon_get_theme_option($fieldBaseName);
                }
                else {
                    $fieldName = str_replace('-','_', self::getFieldNameWithoutLangSuffix($fieldBaseName));
                    $context[$fieldName] = carbon_get_theme_option($fieldBaseName); // wartość w $context powinna byc bez suffiksa językowego
                }
            }
            return $context;
        } );
    }
}
