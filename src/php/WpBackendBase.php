<?php
namespace Superskrypt\WpBackendBase;

use Superskrypt\WpBackendBase\WpBackendBaseModuleLoader as WpModuleLoader;
use Superskrypt\WpBackendBase\WpBackendBaseAdminCleaner as WpAdminCleaner;
use Superskrypt\WpBackendBase\WpBackendBaseFrontCleaner as WpFrontCleaner;
use Superskrypt\WpBackendBase\WpBackendAdminConfig as WpAdminConfig;
use Superskrypt\WpBackendBase\WpBackendTimberTwig as WpTimberTwig;
use Superskrypt\WpBackendBase\WpBackendBaseHelpers as WpBackendBaseHelpers;
use Superskrypt\WpBackendBase\TemplateStructureGenerator\StructuresSchema as StructuresSchema;
use Superskrypt\WpBackendBase\ThemeOptions as ThemeOptions;
include 'GlobalFunctions/support_for_wpml_theme_options.php';

class WpBackendBase {

    public static $backendTextDomain = 'wp-backend-base';
    public static $modulesEnabled = array('frontend-filters','lazy-loading','preloader','spa','video');

	public static function setupBackend( $options = array() ) {

        // add_filter( 'woocommerce_template_path', function( $path ){
        // add_filter( 'template_directory', function( $path ){
        //     $my_path = plugin_dir_path(__FILE__) . '../modules/woocommerce/';
        //     return file_exists( $my_path ) ? $my_path : $path;
        // } );
        
        if (gettype($options) == 'string'){
            if(file_exists($options)){
                $options = json_decode(file_get_contents($options), true);
            }else{
                $options = array();
            }
        }

        $settings = self::setupInitConfig( $options );


        self::loadTextDomains( $settings );


        self::bootCarbonFields();
        WpAdminCleaner::themeSetup($settings['themeSetup']);


        WpAdminCleaner::hideTemplateSelection();
        WpAdminCleaner::hideThemesSubmenu();
        WpAdminCleaner::disableComments();
        if ($settings['disable_posts']) WpAdminCleaner::disablePosts();
        if ($settings['menu_pages_to_top']) WpAdminCleaner::reorderMenuPagesToTop();

        // potrzebne do osadzania i wyświetlania video w edytorze gutenberg
        if($settings['remove-embeds'] === true) {
            WpFrontCleaner::removeEmbeds();
        }
        if($settings['remove_scripts'] || is_array($settings['remove_scripts']) ) {
            WpFrontCleaner::removeScripts($settings['remove_scripts']);
        }
        WpFrontCleaner::removeEmoji();

        self::$modulesEnabled = [];
        array_walk($settings['modules_enabled'],function($value){
            self::$modulesEnabled[$value] = true;
        });
        WpModuleLoader::loadAllModules();

        WpAdminConfig::showMenuToEditor();
		WpAdminConfig::addSupportForSvg();
		WpAdminConfig::fixSvgImageAttachment();
		WpAdminConfig::registerMenu($settings['registerMenu']);


        WpTimberTwig::setupTimberTwig(WpBackendBaseHelpers::getBackendBaseContextPath());
        if(isset($settings['theme_options']) && ($settings['theme_options'] === true || is_array($settings['theme_options'])) ) {
            ThemeOptions::init($settings['theme_options']);
        }

        if(isset($settings['social_media_links']) &&  $settings['social_media_links'] === true || is_array($settings['social_media_links'])) {
            SocialMediaLinksComponent::setPathToIcons(WpBackendBaseHelpers::getBackendBaseContextPath());
            SocialMediaLinksComponent::init($settings['social_media_links']);
        }

        if(isset($settings['shortcodes'])) {
            $shortCodesList = $settings['shortcodes'];
            if(is_bool($settings['shortcodes']) && $settings['shortcodes']) {
                $shortCodesList = Shortcodes::REGISTERED_SHORTCODES;
            }
            Shortcodes::enable($shortCodesList);
        }
        
    }

    public static function loadTextDomains($settings) {
        $pluginPathArray = explode('/',WpBackendBaseHelpers::getBackendBaseContextPath());
        $wpBackendBaseRelPath = array_pop($pluginPathArray);

        if(array_key_exists('text_domain',$settings)){
            self::$backendTextDomain = $settings['text_domain'];
        }

        $textdomain_loaded = load_plugin_textdomain( self::$backendTextDomain, false, $wpBackendBaseRelPath . '/vendor/superskrypt/wp-backend-base/src/languages');

        $carbonfields_textdomain_loaded = load_plugin_textdomain( 'carbon-fields', false, $wpBackendBaseRelPath . '/vendor/superskrypt/wp-backend-base/src/languages');
        $carbonfields_textdomain_loaded = load_plugin_textdomain( 'carbon-fields-ui', false, $wpBackendBaseRelPath . '/vendor/superskrypt/wp-backend-base/src/languages');

        add_filter('crb_upload_image_button_html',function($button) {
            $button = str_replace('Add Media',__('Add Media','carbon-fields'),$button);
            return $button;
        });
    }

    public static function bootCarbonFields() {
        add_action( 'after_setup_theme', function () {
            \Carbon_Fields\Carbon_Fields::boot();
        }, 10 );
    }
    
    public static function setupInitConfig( $options ) {
        $defaultSettings = array(
            'social_media_links' => true,
            'shortcodes' => true,
            'theme_options' => true,
            'disable_posts' => true,
            'menu_pages_to_top' => true,
            'remove-embeds' => true,
            'remove_scripts' => array(),
            'modules_enabled' => array(),
            // 'plugins' => array(),
    		'themeSetup' => array(
                'show_admin_bar' => false,
	    		'add_theme_support' => array(
		    		'post-thumbnails' => array(
			    		'post', 'page'
                    ),
                    'html5' => array( 'excerpt', 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' )
				),
				'add_post_type_support' => array(
		    		'excerpt' => 'page'
				),
				'remove_post_type_support' => array(
		    		'editor' => 'page'
                ),
                'update_default_images_sizes' => array(
                    'large' => 1920,
                    'medium' => 800,
                    'card' => 300,
                    'thumbnail' => 500
                ),
                'add_custom_images_sizes' => array(),
                
			),
			'registerMenu' => array(
                'menu_main' => __('Menu main', self::$backendTextDomain),
                'menu_footer' => __('Menu footer', self::$backendTextDomain),
                'menu_header' => __('Menu header', self::$backendTextDomain),
			),
			'admin_styles' => array(),
			'admin_scripts' => array(),
            'text_domain' => 'wp-backend-base'
		
        );
        $settings = array();
    	foreach ($defaultSettings as $key => $value) {

    		if ( array_key_exists( $key , $options ) ) {
				if ( is_array($value) ) {
					$settings[$key] = array_merge($value, $options[$key]);
				}
				else {
					$settings[$key] = $options[$key];
				}
    		}
    		else {
    			$settings[$key] = $value;
    		}
        }
    	return $settings;
    }

    public static function getWpBackendBasePaths() {
        $currentDirPathsFragments = array_filter(explode(DIRECTORY_SEPARATOR,__DIR__));
        $rootPath = DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, array_slice($currentDirPathsFragments, 0, count($currentDirPathsFragments) - 1));
        return array(
            'images' => $rootPath . '/images/',
            'php' => $rootPath . '/php/',
            'scss' => $rootPath . '/scss/',
            'languages' => $rootPath . '/languages/',
            'modules' => $rootPath . '/modules/',
            'root' => $rootPath . '/',
        );
    }
}
