<?php
namespace Superskrypt\WpBackendBase;

use \Superskrypt\WpBackendBase\WpBackendBase;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

class SocialMediaLinksComponent { 

    const SOCIAL_MEDIA_LINKS_SLUG_FILTER_NAME = 'bb_social_media_links_slug';
    const SOCIAL_MEDIA_LINKS_SLUG = 'social_media_links';

    private static $pathToIcons = "";

    /**
     * @param string $wpBaseUri  plugins lub theme uri np.: /wp-content/plugins lub /wp-content/themes
     * @return string pełna ścieżka do pliku svg z ikonami
     */
    public static function setPathToIcons($wpBaseUri) {
        $backendBaseInstallationRelPath = '/vendor/superskrypt/wp-backend-base/src';
        self::$pathToIcons = $wpBaseUri . $backendBaseInstallationRelPath . '/images/social-media-icons.svg';

        return self::$pathToIcons;
    }

    private static $socialMedia = array('facebook', 'twitter', 'linkedin', 'youtube', 'instagram', 'medium', 'tiktok');

    public static function init ($socialMediaList) {
        if(is_array($socialMediaList)) {
            self::$socialMedia = $socialMediaList;
        }
        add_action('carbon_fields_register_fields', array(__CLASS__, 'registerSocialMediaLinksSubPage'));
        self::addToTimberContext();
    }

    public static function registerSocialMediaLinksSubPage () {

        $socialMediaComplexField = self::createComplexField();
        self::generateSocialMediaFields($socialMediaComplexField);
        self::createContainer($socialMediaComplexField);

    }

    private static function createContainer($fields) {
        return Container::make( 'theme_options', __( 'Social media links', WpBackendBase::$backendTextDomain ) )
            ->set_page_parent( 'themes.php' )
            ->add_fields(array($fields));
    }

    private static function createComplexField() {
        $socialMediaLinksFieldSlug = apply_filters(SocialMediaLinksComponent::SOCIAL_MEDIA_LINKS_SLUG_FILTER_NAME, SocialMediaLinksComponent::SOCIAL_MEDIA_LINKS_SLUG, SocialMediaLinksComponent::SOCIAL_MEDIA_LINKS_SLUG);
        return Field::make( 'complex',  $socialMediaLinksFieldSlug , __( 'Social media links', WpBackendBase::$backendTextDomain ))
            ->set_duplicate_groups_allowed(false)
            ->setup_labels(array(
                'singular_name' =>  __( 'social media link', WpBackendBase::$backendTextDomain ) ,
                'plural_name' =>  __( 'social media links', WpBackendBase::$backendTextDomain ) 
            ));
    }

    private static function generateSocialMediaFields($complexField) {
        foreach(self::$socialMedia as $socialMedia) {
            $complexField->add_fields( $socialMedia, __(ucfirst($socialMedia), WpBackendBase::$backendTextDomain), array(
                Field::make( 'text', strtolower($socialMedia) . '_link', __(ucfirst($socialMedia) . ' ' . 'link', WpBackendBase::$backendTextDomain))->set_required(true),
                Field::make( 'text', strtolower($socialMedia) . '_label' , __(ucfirst($socialMedia) . ' ' . 'link label (accessibility)', WpBackendBase::$backendTextDomain))->set_required(true),
                ) 
            );
        }
    }

    private static function addToTimberContext() {
        
        add_filter( 'timber/context', function ($context ) {
            $socialMediaLinksFieldSlug = apply_filters(SocialMediaLinksComponent::SOCIAL_MEDIA_LINKS_SLUG_FILTER_NAME, SocialMediaLinksComponent::SOCIAL_MEDIA_LINKS_SLUG);
            $context ['social_media_links'] = carbon_get_theme_option( $socialMediaLinksFieldSlug);
            $context ['social_media_icons_path'] = self::$pathToIcons; // można nadpisać w motywie dodając do TimberContext
            
            return $context;
        } );
    }
}