<?php
namespace Superskrypt\WpBackendBase;

use Superskrypt\WpBackendBase\WpBackendBase as WpBackendBase;

class WpBackendBaseModuleLoader {
    public static function loadAllModules(){
        // Tu trzeba dodać dependencje
        foreach(WpBackendBase::$modulesEnabled as $module => $params){
            // TODO: DO SPRAWDZENIA CZY BEDA POTRZEBNE $PARAMS
            self::loadModule($module);
        }
    }
    public static function loadModule($moduleName){
        call_user_func(array("Superskrypt\\WpBackendBase\\Modules\\$moduleName\\$moduleName", 'init') );
    }
}