<?php

add_action( 'init', 'includeFilesUsingCarbonFunction' );

function includeFilesUsingCarbonFunction() {
    function crb_get_i18n_theme_option( $option_name ) {
        $language_code =  apply_filters( 'wpml_current_language', NULL );
        $suffix = '_' . $language_code;
        return carbon_get_theme_option( $option_name . $suffix );
    }
}

// ustawienie właściwego języka dla sluga carbon fields dla linków do serwisów społecznościowych
add_filter('bb_social_media_links_slug', function($slug) {
    $lang = apply_filters( 'wpml_current_language', NULL );
    return !empty($lang) ?  $slug . '_' . $lang : $slug;
});