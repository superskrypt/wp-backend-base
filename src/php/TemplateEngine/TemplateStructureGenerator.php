<?php

namespace  Superskrypt\WpBackendBase\TemplateEngine;

use \Superskrypt\WpBackendBase\WpBackendBase;
use \Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator\StructuresSchema;
use \Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator\TSBlock;
use \Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator\TSField;
use \Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator\TSStyles;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

class TemplateStructureGenerator { 

    public static $allowVideoInBlocks = true;
    public static $allowVideoInHero = false;
    public static $structureConfigCache = array();
    public static $pageContentAdditionalFieldPrefix = 'page_additional_';
    
    /** 
     * @param array $opts tablica opcji przekazana w funkcji inicjalizującej TemplateStructureGenerator (TemplateStructureGenerator::init($opts)).
     * @return void
     */
    public static function init($opts) {
        TemplateLoader::init($opts);

        add_action('after_setup_theme' , function () use ($opts) {
            TemplateStructureGenerator::optionsResolver($opts);
        }, 11);
    }
    
    /**
     * Metoda pozwalająca na:
     * - ustawienie text Domain dla tłumaczeń interfejsu generowanego przez TemplateStructureGenerator
     * - aktywowanie renderowania video w blokach które na to pozwalają
     * - aktywowanie video w hero
     * 
     * @todo: do weryfikacji mechanizm renderowania video w blokach i w hero. Obecna implementacja jest eksperymentalna.
     * @param array $opts tablica opcji przekazana w funkcji inicjalizującej TemplateStructureGenerator (TemplateStructureGenerator::init($opts)).
     */
    public static function optionsResolver($opts) {
        if (is_array($opts) && array_key_exists('allow_video_in_blocks', $opts)) {
            self::$allowVideoInBlocks = $opts['allow_video_in_blocks'];
        }
        if (is_array($opts) && array_key_exists('allow_video_in_hero', $opts)) {
            self::$allowVideoInHero = $opts['allow_video_in_hero'];
        }
        TSBlock::setOptions(self::$allowVideoInBlocks,self::$allowVideoInHero);
        // TSField::setOptions(); // funkcja usunięta
    }
    
    /**
     * @param array $structureConfig Tablica asocjacyjna z opcjami konfiguracyjnymi bloków i sekcji przekazanymi przez usera
     * @return void
     */
    public static function generateStructure($structureConfig) {
        TSStyles::setup($structureConfig);
        add_action('after_setup_theme', function() use ($structureConfig) {
            self::addStructure($structureConfig);
        }, 11);
    }


    /**
     * @param string $containerSlug slug kontenera carbon fields
     * @param string $containerName nazwa kontenera Carbon Fields wyświetlana w panelu administracyjnym
     * @param array $showOn flaga określająca w jakich typach postów kontener powinien być wyświetlany np. $showIn = array('page', 'front_page')
     * @param array $dontShowOn flaga określająca w jakich typach postów kontener nie powinien być wyświetlany np. $dontShowOn = array('front_page')
     * 
     * @return Carbon_Fields\Container
     */
    private static function setContainer($containerSlug = '', $containerName = '', $showOn = array(), $dontShowOn = array()) {
        $containerLabel = __($containerName, WpBackendBase::$backendTextDomain);
        $container = Container::make( 'post_meta',$containerSlug, $containerLabel);
        foreach ($showOn as $whereCondition) {
            if ($whereCondition === 'front_page') {
                $container->or_where('post_id', '=', get_option( 'page_on_front' ) );
            }
            else if($whereCondition === 'blog_home_page') {
                $container->or_where('post_id', '=',  get_option( 'page_for_posts' ) );
            }
            else {
                $container->or_where('post_type', '=', $whereCondition);
            }
        }

        forEach ($dontShowOn as $dontCondition) {
            if ($dontCondition === 'front_page') {
                $container->where('post_id', '!=', get_option( 'page_on_front' ) );
            }
            else if($dontCondition === 'blog_home_page') {
                $container->where('post_id', '!=',  get_option( 'page_for_posts' ) );
            }
            else {
                $container->where('post_type', '!=', $dontCondition);
            }
        }
        return $container ;
    }

    /**
     * @param mixed $structureConfig Tablica asocjacyjna z opcjami konfiguracyjnymi bloków i sekcji przekazanymi przez usera
     * 
     * @return void
     */
    private static function addStructure($structureConfig) {
        $showOn = $structureConfig['show_on'];
        $dontShowOn = array_key_exists('dont_show_on', $structureConfig) ? $structureConfig['dont_show_on'] : [];
        $hero = $structureConfig['hero'];
        $primaryContent = isset($structureConfig['primary_content']) ? $structureConfig['primary_content'] : false;
        $heroCustomSettings = isset($structureConfig['hero_settings']) ? $structureConfig['hero_settings'] : false;
        $sectionBlocks = $structureConfig['section_blocks'];
        $sectionsOptions =  $structureConfig['section'];
        $pageContentAdditionalFields = isset($structureConfig['page_content_additional_fields']) ? $structureConfig['page_content_additional_fields'] : false;

        foreach($showOn as $postType){
            self::$structureConfigCache[$postType] = $structureConfig;
        }

        $contentContainer = false;

        if($hero) {
            self::generateHeroBlock($hero, $showOn, $dontShowOn, $heroCustomSettings);
        }

        $contentContainer = self::setContainer('page_sections', __('Page content', WpBackendBase::$backendTextDomain), $showOn, $dontShowOn);
        if($pageContentAdditionalFields) {
            self::generatePageContentAdditionalFields($pageContentAdditionalFields, $contentContainer);
        }

        if($primaryContent) {
            self::generatePrimaryContent($showOn, $dontShowOn, $primaryContent, $sectionBlocks, $contentContainer);
        }
        
        if($sectionBlocks) {
            self::generatePageSections($showOn, $dontShowOn, $sectionBlocks, $sectionsOptions, $contentContainer);
        }

    }

    private static function generateHeroBlock($hero, $showOn = array(), $dontShowOn = array(), $heroCustomSettings = array()) {
        if ($hero === true) {
            $heroSchema = TSBlock::updateBlockSchema($heroCustomSettings, TSBlock::getDefaultHeroSchema());
            $hero = [ 'standard' =>  TSBlock::addBlockFields('hero', 'hero', $heroSchema) ];
        } else {
            $hero = array_map(function ($field_renderer) {return $field_renderer();}, $hero);
        }
        $container = self::setContainer('hero', 'Page hero', $showOn, $dontShowOn);

        $container->add_fields([Field::make('select', 'hero_type', __('Hero type', WpBackendBase::$backendTextDomain))->add_options(
            array_combine(array_keys($hero), array_keys($hero))
        )]);

        array_walk($hero, function ($fields, $twig_name) use ($container) {
            array_walk($fields, function ($field) use ($twig_name) {
                $field->set_name("hero_$twig_name".$field->get_name());
                $field->set_base_name("hero_$twig_name".'_'.$field->get_base_name());

                $fieldConditionalLogic = $field->get_conditional_logic();
                $currentConditionalLogicRules = isset($fieldConditionalLogic['rules']) ? $fieldConditionalLogic['rules'] : [];
                
                $defaultRules = array(
                        'field' => 'hero_type',
                        'value' => $twig_name,
                    );

                $rules = array_merge([$defaultRules], [...$currentConditionalLogicRules]);
                $rules['relation'] = 'AND';
                $field->set_conditional_logic( $rules );
            });
            $container->add_fields($fields);
        });
    }

    private static function generatePrimaryContent($showOn, $dontShowOn, $blockList, $blocks, &$container) {
        if(is_array($blockList)){
            if(is_array($blocks)){
                $blockContent = self::createPrimaryContentBlocks($blockList,$blocks);
                $container->add_fields(
                    $blockContent
                );
            }
        }

    }

    private static function generatePageContentAdditionalFields ($fieldsClosure, &$container) {
        if($fieldsClosure && $fieldsClosure instanceof \Closure) {
            $fields = $fieldsClosure();
            $prefix = self::$pageContentAdditionalFieldPrefix;
            $fields = array_map(function($field) use ($prefix) {
                $field->set_name($prefix.$field->get_name());
                $field->set_base_name($prefix.'_'.$field->get_base_name());
                return $field;
            }, $fields);
            $container->add_fields(
                $fields
            );
        }
    }   

    private static function generatePageSections($showOn, $dontShowOn, $blocks, $sectionsOptions, &$container) {

        $blocksInSection = self::createSectionsBlocks($blocks);
        if($blocksInSection) {
            $container->add_fields(
                array(self::generateSectionsContainer($blocksInSection, $sectionsOptions))
            );
        }
        return $container;
    }
    
    private static function generateSectionsContainer($blocksInSection, $sectionsOptions) {
        $sectionOptions = self::createSectionFields($sectionsOptions);

        $sectionsContainer = Field::make( 'complex', 'page_sections___container', __('Sections', WpBackendBase::$backendTextDomain));
        $sectionsContainer->set_layout('grid');
        $complexFieldLabels = array(
            'plural_name' => __('Page Sections', WpBackendBase::$backendTextDomain),
            'singular_name' => __('Page Section', WpBackendBase::$backendTextDomain),
        );
        $sectionsContainer->setup_labels( $complexFieldLabels );
        $sectionsContainer->add_fields(
            array_merge( $sectionOptions, [$blocksInSection]  )
        );
        return $sectionsContainer;
    }

    private static function createSectionFields ($sectionConfig) {
        $sectionFields = [];

        $sectionHeadingConfig = isset($sectionConfig['section_heading']) ? $sectionConfig['section_heading'] : false;
        $sectionOptionsConfig = isset($sectionConfig['section_options']) ? $sectionConfig['section_options'] : false;

        $sectionFields = self::addSectionConfigFields('options',  $sectionOptionsConfig , $sectionFields);
        $sectionFields = self::addSectionConfigFields('heading', $sectionHeadingConfig  , $sectionFields);
        $sectionFields = self::getAdditionalFieldsToSection($sectionConfig, $sectionFields);

        return array_merge($sectionFields); 
    }

    private static function addSectionConfigFields($configTypeName, $sectionConfig, $sectionFields ) {
        $defaultFieldsConfigPath = false;
        switch($configTypeName) {
            case 'options':
                $defaultFieldsConfigPath = StructuresSchema::SECTION_OPTIONS_SCHEMA['section']['section_options'];
                break;
            case 'heading':
                $defaultFieldsConfigPath = StructuresSchema::SECTION_OPTIONS_SCHEMA['section']['section_heading'];
                break;
        }

        if(!$defaultFieldsConfigPath) {
            return $sectionFields;
        }

        $fields = [];

        foreach($sectionConfig  as $option => $render) {
            $field = $render === true ? TSField::generateField($defaultFieldsConfigPath[$option]) : (is_callable($render) ? $render() : $render);
            
            if ($field) { 
                if(is_array($field)) {
                    foreach($field as $subfield) {
                        array_push( $fields , $subfield);
                    }
                }
                else {
                    array_push( $fields, $field);
                }
            }
        }
        return array_merge($sectionFields, $fields);

    }

    private static function getAdditionalFieldsToSection($sectionOptionsConfig, $sectionStandardFields = []) {
        $additionalFields = isset($sectionOptionsConfig['additional_fields']) && $sectionOptionsConfig['additional_fields'] instanceof \Closure ? (is_callable($sectionOptionsConfig['additional_fields']) ? $sectionOptionsConfig['additional_fields']() : $sectionOptionsConfig['additional_fields']) : false;
        if(isset($sectionOptionsConfig['additional_fields']) && !empty($sectionOptionsConfig['additional_fields']) ) {
            if($sectionOptionsConfig['additional_fields'] instanceof \Closure && is_callable($sectionOptionsConfig['additional_fields']) ) {
                    $additionalFields =  $sectionOptionsConfig['additional_fields']();
            }
            if(is_array($sectionOptionsConfig['additional_fields'])) {
                $additionalFields = $sectionOptionsConfig['additional_fields'];
            }
        }
        
        if( $additionalFields ) {
            $sectionStandardFields = array_merge($sectionStandardFields, $additionalFields);
        }
        return $sectionStandardFields;
    }


    private static function createSectionsBlocks($blocks) {

        $sectionBlocks = Field::make( 'complex', 'sections__blocks', __('Section Blocks', WpBackendBase::$backendTextDomain) );
        
        foreach($blocks as $blockName => $render) {

            $blockSchema = self::getBlockSchema($render, $blockName);

            $blockLabel = TSBlock::getBlockTitle($blockName, $blockSchema );

            $fieldsToAdd = self::getFieldsToAdd($render, $blockSchema, $blockName, $blockLabel);

            if(!empty($fieldsToAdd)) {
                $sectionBlocks->add_fields($blockName , $blockLabel, $fieldsToAdd)->set_header_template( $blockLabel );
            }
        }
        $sectionBlocks->set_layout('grid');
        $sectionBlocks->set_collapsed(true);
        return $sectionBlocks;
    }
    private static function createPrimaryContentBlocks($blockList,$blocks){
        $container = Field::make( 'complex', 'primary_content', __('Primary Content', WpBackendBase::$backendTextDomain) )
        ->set_min(1)
        ->set_max(1);

        $fieldsToAdd = [];
        $counter = 0;
        foreach($blockList as $blockName){
            $render = isset($blocks[$blockName]) ? $blocks[$blockName] : false;

            $blockSchema = self::getBlockSchema($render, $blockName);

            $blockLabel = TSBlock::getBlockTitle( $blockName, $blockSchema );

            // $fieldsToAdd[] = Field::make( 'separator', 'block_separator_hidden_'.$counter, "");
            // $fieldsToAdd[] = Field::make( 'separator', 'block_separator_'.$counter, __( $blockLabel, WpBackendBase::$backendTextDomain ) )->set_width(15);

            $newFields = self::getFieldsToAdd($render, $blockSchema, $blockName, $blockLabel);

            if(count($newFields)>0){
                $field_width = floor(100 / count($newFields));
                foreach($newFields as $key=>$field){
                    $newFields[$key]->set_width($field_width);
                    $newName = "primary_content__".$blockName."__".$field->get_base_name();
                    $newFields[$key]->set_base_name($newName);
                    $newFields[$key]->set_name("_".$newName);
                }
                $newFieldsBlockKeys = array();
                foreach($newFields as $key=>$field){
                    unset($newFields[$key]);
                    $newFieldsBlockKeys[$blockName."_".$key] = $field;
                }
                $fieldsToAdd = array_merge($fieldsToAdd,$newFieldsBlockKeys);
            }

            $counter++;
        }
        if(count($fieldsToAdd)>0){
            $container->add_fields($fieldsToAdd);
        }
        return array($container);
    }

    private static function getBlockSchema($render, $blockName){

        $customUserBlockSettings =  is_array($render) ? $render : false;
        
        $blockSchemaName = $customUserBlockSettings && isset($customUserBlockSettings['use_schema']) ? $customUserBlockSettings['use_schema'] : $blockName;

        $defaultBlockSchema = TSBlock::getDefaultBlockSchema ( $blockSchemaName );

        return TSBlock::updateBlockSchema($customUserBlockSettings, $defaultBlockSchema);
    }

    private static function getFieldsToAdd($render, $blockSchema, $blockName, $blockLabel){
        $fieldsToAdd = [];
        if($render === true || is_array($render) ) {
            // Poniższe linie są redundantne, bo już wcześniej 
            //
            // if(is_array($render)){
            //     if(!is_array($blockSchema)){
            //         $blockSchema = array();
            //     }
            //     $blockSchema = array_replace_recursive($blockSchema,$render);
            // }
            $fieldsToAdd = TSBlock::addBlockFields($blockName, $blockLabel, $blockSchema);
        }
        elseif ($render instanceof \Closure ) {
            $fieldsToAdd = $render();
        }
        return $fieldsToAdd;
    }
}
