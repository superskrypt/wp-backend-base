<?php

use Timber\Timber;
use Superskrypt\WpBackendBase\TemplateEngine\ContentProcessor;
$data = ContentProcessor::getData();
// var_dump($data['menu_footer']);

Timber::render( array('page.twig', 'page_base.twig') , $data );
