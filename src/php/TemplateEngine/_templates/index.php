<?php

use Timber\Timber;
use Superskrypt\WpBackendBase\WpBackendBase;

$context = Timber::get_context();

Timber::render( 'index.twig' , $context );