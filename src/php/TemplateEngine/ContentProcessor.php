<?php
namespace Superskrypt\WpBackendBase\TemplateEngine;
use Superskrypt\WpBackendBase\WpBackendBase as WpBackendBase;
use Superskrypt\WpBackendBase\TemplateEngine\ContentProcessor\AutoCards as AutoCards;
use Superskrypt\WpBackendBase\TemplateEngine\ContentProcessor\PrimaryContent as PrimaryContent;
use Timber\Timber;
use Timber\Post;

class ContentProcessor {

    public static function getData( ){
        $timber_page = new Post();
        $data = Timber::get_context();
        $data['modules_enabled'] = WpBackendBase::$modulesEnabled;
        $data['page'] = $timber_page;
        // TODO: lepiej by było definiować hero w strukturze tak samo jak bloki, żeby mieć gotową tablicę pól dla hero, a nie wyciągać je z tablicy
        $data['hero'] = self::getHeroFields($data['page']);
        $data['is_front_page'] = is_front_page();

        $data['page_sections_container'] = $pageSectionsContainer = carbon_get_the_post_meta('page_sections___container');
        $data['page_content_additional_fields'] = self::getPageContentAdditionalFields($data['page']);

        PrimaryContent::processPrimaryContent($data);

        AutoCards::processAutoCards($data);

        return $data;
    }

    public static function getPageContentAdditionalFields($page) {
        $additionalFields = [];
        if($page instanceof \Timber\Post) {
            $field_prefix = TemplateStructureGenerator::$pageContentAdditionalFieldPrefix;
            $additionalFieldsRaw = array_filter($page->__get('custom'), function ($value, $fieldSlug) use ($field_prefix) {
                return str_contains($fieldSlug, $field_prefix); 
            }, ARRAY_FILTER_USE_BOTH);
            foreach( $additionalFieldsRaw as $fieldName => $fieldValue) {
                $additionalFields[ltrim($fieldName, '_')] = $fieldValue;
            }
        }
        return $additionalFields;
    }

    public static function getHeroFields($page){
        $return = array();
        foreach($page->custom as $key => $val){
            if(substr($key,0,5) == '_hero'){
                $heroTypeStart = false;
                if(array_key_exists('_hero_type',$page->custom)){
                    $heroTypeStart = strpos($key,$page->custom['_hero_type']);
                }
                if($heroTypeStart === false) {
                    $keyArray = explode('_',$key);
                    if(count($keyArray) == 3){
                        $return[$keyArray[2]] = $val;
                    }elseif(count($keyArray) > 3){
                        $return[$keyArray[2]][implode('_',array_splice($keyArray,3))] = $val;
                    }
                }else{
                    $return[$page->custom['_hero_type']][substr($key,$heroTypeStart + strlen($page->custom['_hero_type']) + 1)] = $val;
                    $return[substr($key,$heroTypeStart + strlen($page->custom['_hero_type']) + 1)] = $val;
                }
            }
        }
        return $return;
    }
}