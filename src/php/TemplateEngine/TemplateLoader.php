<?php

namespace Superskrypt\WpBackendBase\TemplateEngine;

use Timber\Timber;

class TemplateLoader {
    public static function init ($opts) {

        add_action('after_setup_theme', function() {
            add_action('get_header', array(__CLASS__,'overwriteTemplatesDirectoryForHeader'));
            add_action('get_footer', array(__CLASS__,'overwriteTemplatesDirectoryForFooter'));
        },999, 0);
                
        add_filter('page_template', array(__CLASS__, 'loadTemplate'));
        add_filter('single_template', array(__CLASS__, 'loadTemplate'));

        Timber::$locations[] = dirname(__FILE__) . '/_templates/twig';

        if (is_array($opts) && array_key_exists('twig_dir', $opts) && is_string($opts['twig_dir'])) {
            array_unshift(Timber::$locations, $opts['twig_dir']);
        }
    }

    public static function overwriteTemplatesDirectoryForHeader(){
        add_filter('stylesheet_directory', function($template_dir, $template, $theme_root) {
            return !stream_resolve_include_path( $template_dir . '/header.php') ? dirname(__FILE__) . '/_templates' : $template_dir;
        }, 10, 3);
    }

    public static function overwriteTemplatesDirectoryForFooter(){
        add_filter('stylesheet_directory', function($template_dir, $template, $theme_root) {
            return !stream_resolve_include_path( $template_dir . '/footer.php') ? dirname(__FILE__) . '/_templates' : $template_dir;
        }, 10, 3);
    }

    public static function loadTemplate ($postType) {
        global $post;
        $fileName = basename($postType);
        if ( $post->post_type == 'page' ) {
            $source = ($fileName && file_exists(get_stylesheet_directory() . '/' . $fileName)) ? get_stylesheet_directory() . '/' . $fileName : dirname(__FILE__) . '/_templates/page.php';
            return $source;
        }
        if ( $post->post_type == 'post' ) {
            $source = ($fileName && file_exists(get_stylesheet_directory() . '/' . $fileName)) ? get_stylesheet_directory() . '/' . $fileName : dirname(__FILE__) . '/_templates/page.php';
            return $source;
        }
        return $postType;
    }
}
