<?php


namespace  Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator;

// TODO: powinien być mechnizm dzięki któremu bedzie można przekazać paletę kolorów
define('DEFAULT_COLOR_PALETTE', array(
    'bg-transparent' => "none",
    "bg-neutral200" => "light" ,
    "bg-neutral700" => "dark" ,
));

define('DEFAULT_CARDS_PALETTE', array(
    'bg-transparent' => "none",
));

define('DEFAULT_HERO_IMAGE_LAYOUT_VARIANT', array(
    'cover' => "Cover",
    'below-texts' => "Below texts",
));

define('CARDS_LAYOUT_VARIATIONS', array(
    'col-2' => "Two columns",
    'col-3' => "Three columns",
));
define('COLUMN_LAYOUT_VARIATIONS', array(
    'col-1' => "One column",
    'col-2' => "Two columns",
    'col-3' => "Three columns",
));

define('GALLERY_LAYOUT_VARIATIONS', array(
    'square' => 'Square images',
    'landscape' => 'Landscape images',
    'portrait' => 'Portrait images',
));
// define('POST_TYPES',get_taxonomies());

class StructuresSchema {
    public static function getColorPalette() {
        return defined('CUSTOM_COLOR_PALETTE') ? CUSTOM_COLOR_PALETTE : DEFAULT_COLOR_PALETTE; 
    }
    public static function getCardsColorPalette() {
        return defined('CUSTOM_CARDS_PALETTE') ? CUSTOM_CARDS_PALETTE : DEFAULT_CARDS_PALETTE; 
    }
    public static function getHeroImageLayoutVariants() {
        return defined('CUSTOM_DEFAULT_HERO_IMAGE_LAYOUT_VARIANT') ? CUSTOM_DEFAULT_HERO_IMAGE_LAYOUT_VARIANT : DEFAULT_HERO_IMAGE_LAYOUT_VARIANT; 
    }
    public static function getPostTypes() {
        $base_post_types = array("post"=>"post","page"=>"page");
        $args=array(
            'public'                => true,
            'exclude_from_search'   => false,
            '_builtin'              => false
        );
        $post_types=array_merge($base_post_types,get_post_types($args,'names','and'));
        return $post_types;
    }
    public static function getTaxonomies() {
        $taxonomies = get_taxonomies();
        return $taxonomies;        
    }
    public static function getTerms() {
        // $taxonomies = get_taxonomies();
        // $terms = [0=>''];
        // foreach($taxonomies as $tax){
        //     $cterms = get_terms( array(
        //         'taxonomy' => $tax,
        //         'hide_empty' => false,
        //     ) );
        //     foreach($cterms as $term){
        //         // error_log(print_r($term,true));
        //         $terms[$tax.' | '.$term->slug] = $tax.' | '.$term->name;
        //     }
        // }
        // return $terms;
        
        $terms = [0=>''];
        $cterms = get_terms( array(
            'taxonomy' => 'category',
            'hide_empty' => false,
        ) );
        foreach($cterms as $term){
            $terms[$term->slug] = $term->name;
        }
        return $terms;
    }
    const BLOCK_STYLES_DEFAULT = true;
    const SECTION_OPTIONS_SCHEMA = [
        "section" => array(
            "section_options" => array(
                'bg_color' => array('type' => 'select', 'slug'=> 'page_sections__container_color', 'title' => 'Section color', "choice_options"=> array(__CLASS__, 'getColorPalette') , ),
                'bg_image' => array('type' => 'image', 'slug'=> 'section__background_img', 'title' => 'Section background image'),
            ),
            "section_heading" => array(
                'headings_text_color' => array('type' => 'select', 'slug'=> 'section__headings_text_color', 'title' => 'Section headings text color', "choice_options" => array('color-default' => 'black (default)', 'color-white' => 'white' )),
                'title' =>  array('type' => 'text', 'slug'=> 'section__title', 'title' => 'Title'),
                'subtitle' =>  array('type' => 'text', 'slug'=> 'section__subtitle', 'title' => 'Subtitle'),
            ),
        ),
    
        "additional_fields" => false,
        
    ];

    const HERO_SCHEMA = [
        'settings' => array(
            'complex' => false,
            ),
        'block_options' => false,
        'fields' => [
            // 'variant' => array('type' => 'checkbox', 'slug'=> 'variant', 'title' => 'Place image in background', 'field_configs'=> [
            //     'required' =>  false,
            //     'checkbox_default_value' => true
            // ]),
            'image_layout_variant' => array('type' => 'select', 'slug'=> 'image_layout_variant', 'title' => 'Image layout variant', "choice_options"=> array(__CLASS__, 'getHeroImageLayoutVariants')),


            'image' => array('type' => 'image', 'slug'=> 'image', 'title' => 'Image file'),
            // 'video_hero' =>  array( 'type' => 'file',  'slug'=> 'video_id',  'title' => 'Video file', 'field_configs'=> [
            //     'required' =>  false,
            //     'type' => array('video')
            // ]),
            'video' =>  array( 'type' => 'text',  'slug'=> 'video_id',  'title' => 'Video id', 'field_configs'=> [
                'required' =>  false
            ]),
            'lead' => array('type' => 'textarea', 'slug'=> 'lead', 'title' => 'Lead'),
            ],
        'additional_fields' => false, // bool | closure z Field::make z carbon fields
        'disabled_fields' => array(), 
        'disabled_block_options' => array(),
        "order" => []
    ];
    const SCHEMA = [
        "cards" => [
            'settings' => array(
                'complex' => true,
                "layout" => 'tabbed-horizontal',
                "block_label" => "Cards" 
                ),
            'block_options' => array(
                "columns" => CARDS_LAYOUT_VARIATIONS, 
                ),
            'post_type' => array('type' => 'hidden'),
            'fields' => [
                // 'bg_color' => array('type' => 'select', 'slug'=> 'bg_color', 'title' => 'Link box color', "choice_options"=> array(__CLASS__, 'getColorPalette') ),
                'bg_color' => array('type' => 'select', 'slug'=> 'bg_color', 'title' => 'Link box color', "choice_options"=> array(__CLASS__, 'getCardsColorPalette') ),
                'image' => array('type' => 'image', 'slug'=> 'image', 'title' => 'Image'),
                // 'video' =>  array( 'type' => 'text',  'slug'=> 'video_id',  'title' => 'Video id', 'field_configs'=> [
                //     'required' =>  false
                // ]),
                'tagline' => array('type' => 'text', 'slug'=> 'tagline', 'title' => 'Tagline'),
                'title' => array('type' => 'text', 'slug'=> 'title', 'title' => 'Title'),
                'content' => array('type' => 'rich_text', 'slug'=> 'content', 'title' => 'Content'),
                'button_text' => array('type' => 'text', 'slug'=> 'button_text', 'title' => 'Button text'),
                'href' => array('type' => 'text', 'slug'=> 'href', 'title' => 'Link url'),
            ],
            'additional_fields' => false, // bool | closure z Field::make z carbon fields
            'disabled_fields' => array(), 
            'additional_item_fields' => false, // bool | closure z Field::make z carbon fields tylko dla bloków complex
            'disabled_block_options' => array(),
            "custom_block_label" => false,
            "order" => [],
            'styles' => self::BLOCK_STYLES_DEFAULT,
        ],

        'auto_cards' => [
            'settings' => array(
                "frontend_taxonomies_filtering" => false,
                'complex' => false,
                'block_label' => 'Auto Cards',
                'show_descendants' => true,
                'auto_cards' => array(
                    'post_type' => 'post',
                    'taxonomies' => array('category'=>array()),
                    'taxonomy_relation' => 'sum', // intersection (taxonomy1_term1 || taxonomy1_term2) && (taxonomy2_term1 || taxonomy2_term2)
                    'sort' => array(
                        array(
                            'type' => 'date', // text, number
                            'field' => 'date',
                            'direction' => 'UP'
                        )
                    ),
                    'limit' => array(
                        array(
                            'type' => 'date', // text, number
                            'field' => 'date',
                            'lower_limit' => 'today',
                            'upper_limit' => '+3 months'
                        )
                    ),
                    'max' => -1,
                    'fields' => array(
                        'title' => 'post_title',
                        'image' => 'post_image',
                        'video' => '',
                        'tagline' => 'taxonomy_category',
                        'content' => 'post_content',
                        'button_text' => '',
                        'href' => 'post_guid'
                    )
                ),
            ),
            'block_options' => array(
                'columns' => CARDS_LAYOUT_VARIATIONS
                ),
            'fields' => [
                'bg_color' => array('type' => 'select', 'slug'=> 'bg_color', 'title' => 'Link box color', "choice_options"=> array(__CLASS__, 'getColorPalette') ),
            ],
            'additional_fields' => false, // bool | closure z Field::make z carbon fields
            'disabled_fields' => array(), 
            'additional_item_fields' => false, // bool | closure z Field::make z carbon fields tylko dla bloków complex
            'disabled_block_options' => array(),
            "custom_block_label" => false,
            "order" => [],
            'styles' => self::BLOCK_STYLES_DEFAULT,
        ],
        
        "quick_links" => [
            'settings' => array(
                'complex' => true, 
                'complex_field_slug' => '',
                "layout" => 'tabbed-horizontal',
                "block_label" => "Quick Links"
            ),
            'block_options' => false,
            'fields' => [
                'text' => array('type' => 'textarea', 'slug' => 'text','title' => 'Text'),
                'btn_label' => array('type' => 'text', 'slug'=> 'btn_label', 'title' => 'Link label'),
                'url' => array('type' => 'text', 'slug'=> 'url', 'title' => 'Link url'),
                ],
            'additional_fields' => false, // bool | closure z Field::make z carbon fields
            'disabled_fields' => array(), 
            'additional_item_fields' => false, // bool | closure z Field::make z carbon fields tylko dla bloków complex
            'disabled_block_options' => array(),
            "custom_block_label" => false,
            "order" => [],
            'styles' => self::BLOCK_STYLES_DEFAULT,
        ],
        "big_links" => [
            'settings' => array(
                'complex' => true,
                "layout" => 'tabbed-horizontal',
                "block_label" => "Big links"
                ),
            'block_options' => array(
                "columns" => CARDS_LAYOUT_VARIATIONS, 
                ),
            'fields' => [
                'image' => array('type' => 'image', 'slug'=> 'image', 'title' => 'Image'),
                'title' => array('type' => 'text', 'slug'=> 'title', 'title' => 'Title'),
                'text' => array('type' => 'textarea', 'slug'=> 'text', 'title' => 'Text'),
                'button_label' => array('type' => 'text', 'slug'=> 'button_label', 'title' => 'Button text'),
                'button_url' => array('type' => 'text', 'slug'=> 'button_url', 'title' => 'Button url'),
                ],
            'additional_fields' => false, // bool | closure z Field::make z carbon fields
            'disabled_fields' => array(),
            'additional_item_fields' => false, // bool | closure z Field::make z carbon fields tylko dla bloków complex
            'disabled_block_options' => array(),
            "custom_block_label" => false,
            "order" => [],
            'styles' => self::BLOCK_STYLES_DEFAULT,
        ],
        "subheading" => [
            'settings' => array(
                'complex' => false,
                "block_label" => "Subheading"
            ),
            'block_options' => false,
            'fields' => [
                'subheading' => array('type' => 'text', 'slug'=> 'subheading', 'title' => 'Subheading (h3)')
            ],
            'additional_fields' => false, // bool | closure z Field::make z carbon fields
            'disabled_fields' => array(),
            'disabled_block_options' => array(),
            "custom_block_label" => false,
            "order" => [],
            'styles' => self::BLOCK_STYLES_DEFAULT,
        ],
        "lead" => [
            'settings' => array(
                'complex' => false,
                "block_label" => "Lead"
                ),
            'block_options' => false,
            'fields' => [
                'image' =>array('type' => 'image', 'slug'=> 'image', 'title' => 'Image file'),
                'lead' => array('type' => 'textarea', 'slug'=> 'lead', 'title' => 'Lead content')
            ],
            'additional_fields' => false, // bool | closure z Field::make z carbon fields
            'disabled_fields' => array(),
            'disabled_block_options' => array(),
            "custom_block_label" => false,
            "order" => [],
            'styles' => self::BLOCK_STYLES_DEFAULT,

        ],
        "text" => [
            'settings' => array(
                'complex' => false,
                "block_label" => "Text"
                ),
            'block_options' => false,
            'fields' => [
                'content_rt' => array('type' => 'rich_text', 'slug'=> 'content_rt', 'title' => 'Content', 'conditional_logic' => [], ),
            ],
            'additional_fields' => false, // bool | closure z Field::make z carbon fields
            'disabled_fields' => array(),
            'disabled_block_options' => array(),
            "custom_block_label" => false,
            "order" => [],
            'styles' => self::BLOCK_STYLES_DEFAULT,
        ],
        // "multi_column_text" => [
        //     'settings' => array(
        //         'complex' => true,
        //         "block_label" => "Multi Column Text"
        //         ),
        //     'block_options' => array(
        //         "columns" => COLUMN_LAYOUT_VARIATIONS,
        //         ),
        //     'fields' => [
        //         'content_rt' => array('type' => 'rich_text', 'slug'=> 'content_rt', 'title' => 'Content')
        //     ],
        //     'additional_fields' => false, // bool | closure z Field::make z carbon fields
        //     'disabled_fields' => array(),
        //     'additional_item_fields' => false, // bool | closure z Field::make z carbon fields tylko dla bloków complex
        //     'disabled_block_options' => array(),
        //     "custom_block_label" => false,
        //     "order" => [],
        //     'styles' => self::BLOCK_STYLES_DEFAULT,
        // ],
        "cta" => [
            'settings' => array(
                'complex' => false,
                "block_label" => "CTA"
                ),
            'block_options' => false,
            'fields' => [
                'title' => array('type' => 'text', 'slug'=> 'title', 'title' => 'Title'),
                'text' => array('type' => 'text', 'slug'=> 'text', 'title' => 'Text'),
                'href' => array('type' => 'text', 'slug'=> 'href', 'title' => 'Link'),
                'button_text' => array('type' => 'text', 'slug'=> 'button_text', 'title' => 'Button text'),
            ],
            'additional_fields' => false, // bool | closure z Field::make z carbon fields
            'disabled_fields' => array(),
            'disabled_block_options' => array(),
            "custom_block_label" => false,
            "order" => [],
            'styles' => self::BLOCK_STYLES_DEFAULT,
            ],
        'image' => [
            'settings' => array(
                'complex' => false,
                "block_label" => "Image"
                ),
            'block_options' => false,
            'fields' => [
                'image' =>array('type' => 'image', 'slug'=> 'image', 'title' => 'Image'),
                // 'video' =>  array( 'type' => 'text',  'slug'=> 'video_id',  'title' => 'Video id', 'field_configs'=> [
                //         'required' =>  false
                //     ]),
                // 'audio' => array('type' => 'file', 'slug'=> 'audio_id', 'title' => 'Audio file','field_configs'=> [
                //     'required' =>  false,
                //     "type" => 'audio'
                // ]),
                'caption' => array('type' => 'textarea', 'slug'=> 'caption', 'title' => 'Caption'),
                'credits' => array('type' => 'text', 'slug'=> 'credits', 'title' => 'Credits'),
            ],
            'additional_fields' => false, // bool | closure z Field::make z carbon fields
            'disabled_fields' => array(),
            'disabled_block_options' => array(),
            "custom_block_label" => false,
            "order" => [],
            'styles' => self::BLOCK_STYLES_DEFAULT,
        ],
        'highlight' => [
            'settings' => array(
                'complex' => false,
                "block_label" => "Quote"
            ),
            'block_options' => false,
            'fields' => [
                'text' => array('type' => 'textarea', 'slug'=> 'text', 'title' => 'Text'),
                'caption' => array('type' => 'text', 'slug'=> 'caption', 'title' => 'Caption'),
            ],
            'additional_fields' => false, // bool | closure z Field::make z carbon fields
            'disabled_fields' => array(),
            'disabled_block_options' => array(),
            "custom_block_label" => false,
            "order" => [],
            'styles' => self::BLOCK_STYLES_DEFAULT,
        ],
        'audio' => [
            'settings' => array(
                'complex' => false,
                "block_label" => "Audio"
            ),
            'block_options' => false,
            'fields' => [
                'audio_id' => array('type' => 'file', 'slug'=> 'audio_id', 'title' => 'Audio file','field_configs'=> [
                    'required' =>  true,
                    "type" => 'audio'
                ]),
                'title' => array('type' => 'text', 'slug'=> 'title', 'title' => 'Title'),
                'subtitle' => array('type' => 'text', 'slug'=> 'subtitle', 'title' => 'Subtitle'),
                'description' => array('type' => 'textarea', 'slug'=> 'description', 'title' => 'Description'),
            ],
            'additional_fields' => false, // bool | closure z Field::make z carbon fields
            'disabled_fields' => array(),
            'disabled_block_options' => array(),
            "custom_block_label" => false,
            "order" => [],
            'styles' => self::BLOCK_STYLES_DEFAULT,
        ],
        "gallery" => [
            'settings' => array(
                'complex' => false,
                "layout" => 'tabbed-horizontal',
                "block_label" => "Gallery"
                ),
            'block_options' => array(
                "layout" => GALLERY_LAYOUT_VARIATIONS, 
                ),
            'fields' => [
                'image' => array('type' => 'media_gallery', 'slug'=> 'image_list', 'title' => 'List of images', 'field_configs'=> [
                        'duplicates_allowed' =>  false,
                        'type' => array('image','audio')
                    ]),
                ],
            'additional_fields' => false, // bool | closure z Field::make z carbon fields
            'disabled_fields' => array(),
            'additional_item_fields' => false, // bool | closure z Field::make z carbon fields tylko dla bloków complex
            'disabled_block_options' => array(),
            "custom_block_label" => false,
            "order" => [],
            'styles' => self::BLOCK_STYLES_DEFAULT,
        ],
    ];

}
