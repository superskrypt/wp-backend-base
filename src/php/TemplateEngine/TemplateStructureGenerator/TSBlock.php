<?php

namespace Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator;

use \Superskrypt\WpBackendBase\WpBackendBase;
use \Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator\StructuresSchema;
use \Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator\TSField;
use \Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator\TSHelpers;
use \Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator\AutoCardsFiltersGenerator;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

class TSBlock {
    public static $allowVideoInBlocks = true;
    public static $allowVideoInHero = false;
    public static $itemsOrderKey = 'item_order';

    public static function setOptions ($allowVideoInBlocks,$allowVideoInHero) {
        self::$allowVideoInBlocks = $allowVideoInBlocks;
        self::$allowVideoInHero = $allowVideoInHero;
    }

    public static function getDefaultBlockSchema ($blockName) {
        $defaultBlockSchema = isset(StructuresSchema::SCHEMA[$blockName]) ?  StructuresSchema::SCHEMA[$blockName] : false;

            if($defaultBlockSchema) {
                foreach($defaultBlockSchema['fields'] as $fieldName => $fieldValue) {
                    if(!self::checkIfFieldIsAllowed($fieldName)) {
                        unset($defaultBlockSchema['fields'][$fieldName]) ;
                    }
                }
            }
        return $defaultBlockSchema;
    }

    public static function getDefaultHeroSchema() {
        $heroSchema = StructuresSchema::HERO_SCHEMA;

        foreach($heroSchema['fields'] as $fieldName => $fieldValue) {
            if(!self::checkIfFieldIsAllowed($fieldName)) {
                unset($heroSchema['fields'][$fieldName]) ;
            }
        }
        return $heroSchema;
    }

    private static function getSortedFieldsByArrayKeys($arrayToSort, $order) {
        if( !is_array($order) ||  empty($order) ) { 
            return TSHelpers::flattenArrayAssocValues($arrayToSort); 
        }
        $sorted = array_replace(array_flip( $order), $arrayToSort);
        return array_filter(TSHelpers::flattenArrayAssocValues($sorted), function($val) {
            return is_int($val) ? false : true;
        });
    }

    /**
     * Aktualizuje schemę dla bloku o ustawienia usera
     */
    public static function updateBlockSchema($customUserBlockSettings, $defaultBlockSchema) {
        $blockSchema = [];

        if($customUserBlockSettings && is_array($defaultBlockSchema)) {
            
            foreach( $defaultBlockSchema as $key => $settingsValue) {
                $blockSchema[$key] = $settingsValue;
                if($key == 'settings' || $key == 'fields' || $key == 'block_options' ) {
                    if(array_key_exists($key,$customUserBlockSettings) && is_array($customUserBlockSettings[$key]) && is_array($defaultBlockSchema[$key])){
                        // $blockSchema[$key] = array_replace($defaultBlockSchema[$key],$customUserBlockSettings[$key]);
                        $blockSchema[$key] = $customUserBlockSettings[$key];
                    }else{
                        $blockSchema[$key] = $defaultBlockSchema[$key];
                    }
                    continue;
                }elseif($key == 'custom_block_label' && array_key_exists($key,$customUserBlockSettings)){
                    $blockSchema[$key] = $customUserBlockSettings[$key];
                }
                if(array_key_exists($key, $customUserBlockSettings)) {
                    $blockSchema[$key] = $customUserBlockSettings[$key];
                }
            }

            if(isset($customUserBlockSettings['use_schema'])) {
                $blockSchema['use_schema'] = $customUserBlockSettings['use_schema'];
            }
            
            $blockSchema = self::removeDisabledFieldsFromBlockSchema($blockSchema);
            $blockSchema = self::removeDisabledBlockOptionsFromBlockSchema($blockSchema);

            return $blockSchema;
        }
        if(is_array($customUserBlockSettings)){
            foreach($customUserBlockSettings as $key => $settingsValue) {
                if(!array_key_exists($key, $blockSchema)) {
                    $blockSchema[$key] = $settingsValue;
                }
            }
            return $blockSchema;
        }

        return $defaultBlockSchema;
    }

    private static function removeDisabledFieldsFromBlockSchema( $blockSchema ) {
        if(is_array($blockSchema) && isset($blockSchema['disabled_fields']) && !empty($blockSchema['disabled_fields'])) {

            $disabledFieldsNames = $blockSchema['disabled_fields'];
            foreach($disabledFieldsNames as $disabledField) {
                if(isset($blockSchema['fields']) && array_key_exists(trim($disabledField), $blockSchema['fields'])) {
                    unset($blockSchema['fields'][trim($disabledField)]);
                }
            }
            return $blockSchema;
        }
        return $blockSchema;
    }

    private static function removeDisabledBlockOptionsFromBlockSchema( $blockSchema ) {
        if(is_array($blockSchema) && isset($blockSchema['disabled_block_options']) && !empty($blockSchema['disabled_block_options'])) {
            $disabledBlockOptions = $blockSchema['disabled_block_options'];
            $blockOptions = isset($blockSchema['block_options']) ? $blockSchema['block_options'] : false;
            
            if(!$blockOptions ) {
                return $blockSchema;
            }

            foreach($disabledBlockOptions as $option) {
                if( array_key_exists(trim($option), $blockOptions)) {
                    unset($blockSchema['block_options'][trim($option)]);
                }
            }
            return $blockSchema;
        }
        return $blockSchema;
    }

    public static function getBlockTitle ($blockName, $blockSchema) {
        if(isset($blockSchema['custom_block_label']) && !empty($blockSchema['custom_block_label'])) {
            return __($blockSchema['custom_block_label'], WpBackendBase::$backendTextDomain);
        }
        return isset($blockSchema['settings']["block_label"]) && !empty($blockSchema['settings']["block_label"]) ?  __($blockSchema['settings']["block_label"], WpBackendBase::$backendTextDomain  ) : __(ucwords(str_replace(['_','-',], ' ', $blockName)), WpBackendBase::$backendTextDomain );
    }

    private static function configAdditionalBlockFields($fieldsConfig, $configKey) {
        $fields = [];
        if(is_array($fieldsConfig) && isset($fieldsConfig[$configKey]) && !empty($fieldsConfig[$configKey])){

            if($fieldsConfig[$configKey] instanceof \Closure) {
                $fields = $fieldsConfig[$configKey]->__invoke();
            }
            else {
                $fields = $fieldsConfig[$configKey];
            }
            // tworzy tablicę assocjacyjną gdzie klucze to nazwy pól a wartości to wygenerowane pola carbon fields
            $additionalFields = TSHelpers::flattenArrayAssocValues(array_map( function($field) {
                $fieldId =  TSHelpers::getCarbonFieldId($field);
                $fields[$fieldId] = $field;
                return [$fieldId => $field];

            }, $fields ));
            return  $additionalFields;
        }
        return $fields;
    }


    public static function addBlockFields($blockName,$blockLabel, $blockSchema) {

        $blockFields = array();
        $complexFieldLabels = array(
            'plural_name' => __($blockLabel,WpBackendBase::$backendTextDomain),
            'singular_name' => __($blockLabel, WpBackendBase::$backendTextDomain),
        );

        $isBlockComplex = isset($blockSchema['settings']['complex']) ? $blockSchema['settings']['complex'] : false;
        $blockLayoutDefined = isset($blockSchema['settings']["layout"]) && $isBlockComplex ? $blockSchema['settings']["layout"] : false ;
        $blockOptionsDefined = isset($blockSchema['block_options']) ? $blockSchema['block_options'] : [];
        $complexFieldSlug = $isBlockComplex && isset($blockSchema['settings']['complex_field_slug'])  && !empty($blockSchema['settings']['complex_field_slug']) ? $blockSchema['settings']['complex_field_slug'] : $blockName . '__block';
        $blockComplex = $isBlockComplex ? Field::make( 'complex', $complexFieldSlug, "" )->setup_labels( $complexFieldLabels ) : false;
        $blockFieldsOrder = isset( $blockSchema['order']) && $blockSchema['order'] ? $blockSchema['order'] : [];
        
        if($blockLayoutDefined) {
            $blockComplex->set_layout($blockLayoutDefined);
        }

        if(is_array($blockSchema) && array_key_exists('fields',$blockSchema) && is_array($blockSchema['fields'])) {
            foreach($blockSchema['fields'] as $fieldKey => $fieldSchema) {
                $field = TSField::generateField($fieldSchema);

                // dodaję klucz z nazwą pola do listy pól (blockFields) aby móc potem posortować według listy pól
                $blockFields[$fieldKey] =  $field;
            }
        }

        $blockOptions = [];
        if($blockOptionsDefined) {

            $blocksOptionsKeys = array_keys($blockOptionsDefined);
            foreach( $blocksOptionsKeys as $idx => $keyName ) {
                    if($blockOptionsDefined[$keyName]) {
                        $blockOptionsTranslated = array_map(function($optionLabel) {
                            return __($optionLabel, WpBackendBase::$backendTextDomain);
                        }, $blockOptionsDefined[$keyName]);

                        $blockOption = Field::make( 'select', $blockName . '_' . 'options__' . $keyName, __(ucfirst($keyName), WpBackendBase::$backendTextDomain) )->add_options($blockOptionsTranslated);
                        
                        array_push($blockOptions, $blockOption);
                    }
                }
        }
        // dołaczenie filtrów dla autocards
        $blockOptions = array_merge($blockOptions, AutoCardsFiltersGenerator::generate($blockSchema));

        $customFields =  self::configAdditionalBlockFields($blockSchema, 'additional_fields'); 

        if($blockComplex) {
            $itemsOrder = isset($blockFieldsOrder[self::$itemsOrderKey]) ? $blockFieldsOrder[self::$itemsOrderKey] : [];

            $customItemFields = self::configAdditionalBlockFields($blockSchema, 'additional_item_fields');
            $itemFieldsMerged = TSHelpers::convertFieldsListToArrayAssoc(array_merge($blockFields, $customItemFields));
            $itemFieldsSorted = self::getSortedFieldsByArrayKeys($itemFieldsMerged, $itemsOrder);
            
            $topLevelFieldsMerged = TSHelpers::convertFieldsListToArrayAssoc( array_merge($blockOptions, $customFields));
            $orderForBlockComplexFields = TSHelpers::setupOrderForComplexBlock( $blockFieldsOrder, self::$itemsOrderKey);

            $blockFieldsMerged = array_merge( $topLevelFieldsMerged, array( self::$itemsOrderKey =>  $blockComplex->add_fields($itemFieldsSorted)));

            $sortedBlockFields = self::getSortedFieldsByArrayKeys($blockFieldsMerged, $orderForBlockComplexFields);
            return $sortedBlockFields;
        }
        
        $blockFieldsMerged = TSHelpers::convertFieldsListToArrayAssoc( array_merge($blockOptions, $blockFields, $customFields ));
        $blockFieldsSorted = self::getSortedFieldsByArrayKeys($blockFieldsMerged, $blockFieldsOrder );

        return  $blockFieldsSorted;
    }

    private static function checkIfFieldIsAllowed($fieldName ) {
        
        switch($fieldName) {
            case 'video':
                $allow = self::$allowVideoInBlocks;
                break;
            case 'video_hero':
                $allow = self::$allowVideoInHero;
                break;
            default:
                $allow = true;
                break;
        }
        return $allow ;
    }
}
?>