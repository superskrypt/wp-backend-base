<?php

namespace Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator;

use Superskrypt\WpBackendBase\WpBackendBase; 
use Carbon_Fields\Field;
use Superskrypt\WpBackendBase\ThemeOptions;
class AutoCardsFiltersGenerator {
    static $filterHeaders = [];
    /**
     * @param array $blockSchema 
     * @return \Carbon_Fields\Field[]
     */
    public static function generate($blockSchema) {
        $filtersFields = [];
        if(isset($blockSchema['use_schema']) && $blockSchema['use_schema'] === 'auto_cards') {
            if(isset($blockSchema['settings']['frontend_taxonomies_filtering'])) {
                $taxonomiesSlugs = $blockSchema['settings']['frontend_taxonomies_filtering'];
                $postType = isset($blockSchema['settings']['auto_cards']['post_type']) ? $blockSchema['settings']['auto_cards']['post_type'] : false;
                $taxonomyFiltersObjects = self::getTaxonomyFiltersObjects($taxonomiesSlugs, $postType);
                add_filter('wp_backend_base_theme_options', array(__CLASS__, 'registerThemeOptionsFiltersHeaders'), 9, 1);
                return !empty($taxonomyFiltersObjects) ? self::getFilterFields($taxonomyFiltersObjects) : [];
            }
        }
        
        return $filtersFields;
    }
    /**
     * @param array $taxonomiesSlugs
     * @param string $postType
     * @return array|false
     */
    public static function getTaxonomyFiltersObjects($taxonomiesSlugs, $postType) {
        if(empty($taxonomiesSlugs) || !$postType) {
            return false;
        }
        $taxonomiesObjects = [];
        foreach($taxonomiesSlugs as $taxonomySlug) {
            $taxonomyObject = get_taxonomy( $taxonomySlug );
            $taxonomyObject && in_array($postType, $taxonomyObject->object_type) ? array_push($taxonomiesObjects, $taxonomyObject) : "";
        }

        return $taxonomiesObjects;
    }

    public static function registerThemeOptionsFiltersHeaders($themeOptions) {
        $args = array(
            'public'   => true,
            '_builtin' => false
        ); 
        $output = 'names';
        $operator = 'and';
        $taxonomiesSlugs = get_taxonomies( $args, $output, $operator );
        $taxonomiesObjects = [];
        foreach($taxonomiesSlugs as $taxonomySlug) {
            $taxonomyObject = get_taxonomy( $taxonomySlug );
            array_push($taxonomiesObjects, $taxonomyObject);
        }
        self::$filterHeaders = self::getFiltersHeaders($taxonomiesObjects);
        array_push($themeOptions, self::$filterHeaders);
        return $themeOptions;
    }

    /**
     * @param WP_Term[] $taxonomiesObjects
     * @return \Carbon_Fields\Field[]
     */
    public static function getFiltersHeaders($taxonomiesObjects) {
        $langSuffix = ThemeOptions::getLanguageSuffix();
        return array_map(function($taxonomy) use ($langSuffix) {
            return  Field::make( 'text', 'filter_label_' . $taxonomy->name . $langSuffix , __('Add heading for filters', ThemeOptions::getTextDomain()) . ' (' . __( $taxonomy->label , ThemeOptions::getTextDomain()) . ')');
        }, $taxonomiesObjects);
    }

    /**
     * @param WP_Term[] $taxonomiesObjects
     * @return \Carbon_Fields\Field[]
     */
    private static function getFilterFields($taxonomiesObjects) {
        return array_map(function($taxonomy) {
            return Field::make( 'checkbox', 'enable_filters__' . $taxonomy->name, __('Enable filters',WpBackendBase::$backendTextDomain) . ' (' .  __( $taxonomy->label ,WpBackendBase::$backendTextDomain) . ')')->set_width(40);
        }, $taxonomiesObjects);
    }
}