<?php

namespace Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator;

use Exception;

class TSHelpers {

    public static function flattenArray($arr) {
        $flattenArr= [];
        array_walk_recursive($arr, function($item, $key) use (&$flattenArr) {
            array_push($flattenArr, $item);
        });
        return $flattenArr;
    }
    
    public static function flattenArrayAssocValues($arr) {
        $flatten = [];
        if(!$arr) { return $flatten; }
        $keysToRemove = [];
        foreach($arr as $key => $val) {
            if(is_array($val) && count($val) > 0) {
                array_push($keysToRemove, $key);
                $flatten = array_merge($flatten, $val);
            } 
            $flatten = array_merge($flatten, [$key => $val]);	
        }
        
        foreach($keysToRemove as $keyName) {
            unset($flatten[$keyName]);
        }
        return $flatten;
    }

    public static function getCarbonFieldId($carbonFieldObject) {
        if(method_exists($carbonFieldObject,'get_base_name' )) {
            return $carbonFieldObject->get_base_name();
        }
        throw new \Exception('TSHelpers::getCarbonFieldId -  Parameter $carbonFieldObject is not valid Carbon Field Object.');
    }

    /**
     * @param orderListArray {Array} - tablica zawierająca liste nazw pól według których należy posortować 
     * @param $itemsOrderKey {String} - klucz pod jakim znajduje się kolejność itemów w bloku complex
     * @return {Array} - Metoda pomocnicza przy ustwianiu kolejności pól pierwszego poziomu w blokach typu complex
     */ 
    public static function setupOrderForComplexBlock($orderListArray, $itemsOrderKey) {
        $order = [];
        if(!$orderListArray || empty($orderListArray) ) { return $order; }
        foreach($orderListArray as $key => $fieldName) {
            $key === $itemsOrderKey ? array_push( $order, $itemsOrderKey) : array_push($order,$fieldName );
        }
        return $order ;
    }

    public static function convertFieldsListToArrayAssoc($fields) {
        $namedFieldsList = [];
        foreach($fields as $key=>$fieldObject) {
            $id = self::getCarbonFieldId($fieldObject);
            $namedFieldsList[$id] = $fieldObject;
        }
        return $namedFieldsList;
    }
}
