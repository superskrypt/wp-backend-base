<?php

namespace Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator;

use \Superskrypt\WpBackendBase\WpBackendBase;
use \Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator\StructuresSchema;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

class TSField {

    public static function generateField($fieldSchema) {
        $fieldLabel =  __($fieldSchema['title'], WpBackendBase::$backendTextDomain);

        $field = Field::make( $fieldSchema['type'], $fieldSchema['slug'], $fieldLabel );
        $fieldConfigs = isset($fieldSchema['field_configs']) ? $fieldSchema['field_configs'] : [];

        if (isset($fieldSchema['conditional_logic'])) {
            $field->set_conditional_logic($fieldSchema['conditional_logic']);
        }

        if($fieldSchema['type'] === 'select' || $fieldSchema['type'] === 'radio' ) {
            
            $choiceOptionsTranslated = self::getTranslatedChoiceOptions($fieldSchema["choice_options"]);
            $field->add_options($choiceOptionsTranslated);
        }
        if($fieldSchema['type'] === 'checkbox' && isset($fieldConfigs["checkbox_default_value"])) {
            $field->set_default_value( $fieldConfigs["checkbox_default_value"] );
        }
        if($fieldSchema['type'] === 'complex' && isset($fieldSchema['sub_fields'])) {
            $subFieldsSchema = $fieldSchema["sub_fields"];
            self::generateComplexField($field, $subFieldsSchema);
        }
        if(isset($fieldSchema['options']) && is_array($fieldSchema['options'])){
            foreach($fieldSchema['options'] as $method=>$param){
                $field->{$method}($param);
            }
        }
        self::generateFieldConfigs($field, $fieldConfigs);
        return $field;
    }

    public static function generateComplexField($fieldContainer, $subFieldsSchema ) {
        $subFields = [];
        foreach($subFieldsSchema as $fieldSchema) {

            $field = Field::make( $fieldSchema ['type'], $fieldSchema ['slug'], $fieldSchema ['title'] );
            if($fieldSchema ['type'] === 'select' || $fieldSchema ['type'] === 'radio' || $fieldSchema ['type'] === 'checkbox') {

                $choiceOptionsTranslated = self::getTranslatedChoiceOptions($fieldSchema["choice_options"]);
                $field->add_options($choiceOptionsTranslated);
            }
            array_push($subFields, $field);
        }

        $fieldContainer->add_fields($subFields);
    }

    private static function generateFieldConfigs($field, $configs = []) {
        if(array_key_exists('max-subfields', $configs )) {
            $field->set_max( $configs['max-subfields'] );
        }
        if(array_key_exists('min-subfields', $configs )) {
            $field->set_min( $configs['min-subfields'] );
        }
        if(array_key_exists('layout', $configs )) {
            $field->set_layout( $configs['layout'] );
        }
        if(array_key_exists('required', $configs )) {
            $field->set_required($configs['required']);
        }
        if(array_key_exists('type', $configs )) {
            $field->set_type(  $configs['type']);
        }
        if(array_key_exists('duplicates_allowed', $configs )) {
            $field->set_duplicates_allowed(  $configs['duplicates_allowed']);
        }
    }

    private static function getTranslatedChoiceOptions($options) {
        $choiceOptions = $options;
        if(is_callable($choiceOptions)) {
            $choiceOptions = call_user_func($options);
        }
        $choiceOptionsTranslated = array_map(function($optionLabel) {
            return __($optionLabel, WpBackendBase::$backendTextDomain);
        }, $choiceOptions);
                
        return $choiceOptionsTranslated;
    }
}
?>