<?php

namespace Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator;

use Superskrypt\WpBackendBase\WpBackendBase as WpBackendBase;
use \Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator\StructuresSchema;

class TSStyles {
    const mainStyleFileName =  "bb-main.scss";
    private static $stylesModified = false;
    private static $wpStyles = array(
        'base' => array(
            '_partials/0_base/colors.scss',
            '_partials/0_base/const.scss',
            '_partials/0_base/fonts.scss',
            '_partials/1_mixins/global-unit.scss',
            '_partials/1_mixins/colors.scss',
            '_partials/1_mixins/media.scss',
            '_partials/1_mixins/grid.scss',
            '_partials/1_mixins/buttons-const.scss',
            '_partials/1_mixins/buttons-base.scss',
            '_partials/1_mixins/buttons.scss',
            '_partials/1_mixins/fonts.scss',
            '_partials/1_mixins/helpers.scss',
            '_partials/1_mixins/hover.scss',
            '_partials/1_mixins/links.scss',
            '_partials/1_mixins/typography.scss',
            '_partials/1_mixins/wcag.scss',
            '_partials/1_mixins/backgrounds.scss',
            '_partials/1_mixins/blocks-const.scss', // zmiana dla bloków. Funkcji min. global-unit(2) i muszą być wywołane po deklaracji tych funkcji
            '_partials/1_mixins/blocks.scss',
            '_partials/1_mixins/components-const.scss',
            '_partials/2_elements/_reset.scss',
            '_partials/2_elements/backgrounds.scss',
            '_partials/2_elements/backgrounds-theme.scss',
            '_partials/2_elements/body.scss',
            '_partials/2_elements/p.scss',
            '_partials/2_elements/texts.scss',
        ),
        'blocks' => array(
            'audio' => '_partials/3_blocks/audio.scss',
            'big_links' => '_partials/3_blocks/big-links.scss',
            'big_links_theme' => '_partials/3_blocks/big-links-theme.scss',
            'cards' => '_partials/3_blocks/cards.scss',
            'cards_theme' => '_partials/3_blocks/cards-theme.scss',
            'cta' => '_partials/3_blocks/cta.scss',
            'highlight' => '_partials/3_blocks/highlight.scss',
            'image' => '_partials/3_blocks/image.scss',
            'lead' => '_partials/3_blocks/lead.scss',
            'quick_links' => '_partials/3_blocks/quick-links',
            'subheading' => '_partials/3_blocks/subheading.scss',
            'text' => '_partials/3_blocks/text.scss',
            'slider' => '_partials/3_blocks/slider.scss',
            'gallery' => '_partials/3_blocks/gallery.scss',
            // 'multi_column_text' => '_partials/4_blocks/multi-column-text.scss',
        ),
        'components' => array(
            '_partials/4_components/consent-box',
            '_partials/4_components/footer',
            '_partials/4_components/header',
            '_partials/4_components/hero',
            '_partials/4_components/language-switcher',
            '_partials/4_components/main-container',
            '_partials/4_components/menu-burger.scss',
            '_partials/4_components/menus',
            '_partials/4_components/page-section',
            '_partials/4_components/skip-links',
            '_partials/4_components/auto-cards-filters',
        ),
    );
    const MODULE_STYLES = array(
        'lazy-loading' => array(
            'base' => array(
                '_partials/2_elements/lazy-images.scss',
            )
            ),
        'preloader' => array(
            'components' => array(
                '_partials/4_components/preloader',
            )
        ),
        'video' => array(
            'components' => array(
                '_partials/4_components/video-player',
            )
        ),
    );
    /**
     * Metoda inicjująca mechanizm generowania domyślnych styli dla html generowanego przez wp-backend-base. 
     * Wygenerowany plik scss z odpowiednimi importami dostępny jest dla bundleów np,.: @superskrypt/npm-tasks
     * @param array $structureSchema
     * @return void
     */
    public static function setup($structureSchema) {
        if(!self::$stylesModified){
            foreach(WpBackendBase::$modulesEnabled as $module => $val){
                if(array_key_exists($module,self::MODULE_STYLES)){
                    self::$wpStyles = array_merge_recursive(self::$wpStyles,self::MODULE_STYLES[$module]);
                }
            }
            self::$stylesModified = true;
        }
        // FIXME - Wyłączenie generowania bb_main.scss w oparciu o zdefiniowane bloki - Mechanizm prawdopodobnie do usunięcia

        // $stylesConfig = self::getStylesConfig($structureSchema);
        // self::generateScssFileWithImports(self::getScssStylesImports($stylesConfig));
    }

    /**
     * @param Array $currentSectionsSchema - schema struktury sekcji i bloków (przekazana przez użytkownika)
     */
    public static function getStylesConfig($currentSectionsSchema) {
        $defaultSectionsSchema = StructuresSchema::SECTION_OPTIONS_SCHEMA;
        $defaultBlocksSchema = StructuresSchema::SCHEMA;
        $blockStylesConfig = [];
        foreach($defaultBlocksSchema as $blockName => $blockConfig) {
            if(is_array($defaultBlocksSchema[$blockName])) {
                //TODO  tutaj prawdopodobnie trzeba będzie dodać mechanizm pobierania wersji styli dla danego bloku np styles => narrow-text-block albo styles => full-width-text-block
                $blockStylesConfig[$blockName] = isset($currentSectionsSchema['section_blocks'][$blockName]['styles']) ? $currentSectionsSchema['section_blocks'][$blockName]['styles'] :  isset($defaultBlocksSchema[$blockName]['styles']);
            }
        }
        $stylesConfig = array(
            'base' => true,
            'blocks' => $blockStylesConfig,
            'components' => true, 
        );
        return $stylesConfig;
    }
    /**
     * @param array $enabledStylesList tablica ze stylami aktywowanymi dla aktualnego projektu (na bazie struktury sekcji i bloków przekazanych przez użytkownika)
     * @return array tablica zawierająca definicje importów scss 
     * Przykład zawartości zwracanej tablicy:
     *  <code>
     *  <?php
     *  array(
     *      @import "_partials/0_base/colors.scss";
     *      @import "_partials/0_base/const.scss";
     *      @import "_partials/0_base/fonts.scss";
     *      ... 
     *      'lead' => '_partials/3_blocks/lead.scss',
     *      'quick_links' => '_partials/3_blocks/quick-links',
     *      'subheading' => '_partials/3_blocks/subheading.scss',
     *      ...
     *      );
     *  ?>
     * </code>
     */
    public static function getScssStylesImports($enabledStylesList) {

        $imports = [];
        foreach($enabledStylesList as $k => $configVal) {
            if(is_array($enabledStylesList[$k]) ) {
                foreach($configVal as $key => $enabled) {
                    $partialPathToInclude = $enabled && isset(self::$wpStyles[$k][$key]) ? self::$wpStyles[$k][$key] : "";
                    array_push($imports ,$partialPathToInclude );
                }
            }
            else {
                $partialPathToInclude = $configVal ? self::$wpStyles[$k] : "";
                array_push($imports ,$partialPathToInclude );
            }
        }
        return TSHelpers::flattenArray(array_filter($imports));
    }

    /**
     * Generuje plik scss z importami w oparciu o parametru przekazane w konfiguracji bloków. Plik wynikowy zostaje zapisany w głównym katalogu styli dla wp-backend-base (wp-backend-base/scss)
     * @param array $importsPaths tablica z importami plików składowych scss
     * @return void
     */
    public static function generateScssFileWithImports($importsPaths) {
        $fileContent = array_reduce($importsPaths, function($carry, $i) {
            $carry .= '@import "' . $i . '";' . PHP_EOL;
            return $carry;
        },  "");

        $scssFolderPath = \Superskrypt\WpBackendBase\WpBackendBase::getWpBackendBasePaths()['scss'];
        $scssFilePath = $scssFolderPath . self::mainStyleFileName;
        file_put_contents($scssFilePath, $fileContent);
    }
}