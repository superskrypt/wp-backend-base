<?php
namespace Superskrypt\WpBackendBase\TemplateEngine\ContentProcessor;

use Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator;

use WP_Query;

class AutoCards {

    public static function processAutoCards(&$data){
        foreach($data['page_sections_container'] as $sectionKey => $section){
            foreach($section['sections__blocks'] as $blockKey => $block){

                $currentSchema = TemplateStructureGenerator::$structureConfigCache[get_post_type()]['section_blocks'][$block['_type']];
                
                if($block['_type']=='auto_cards' || (!($currentSchema instanceof \Closure) && isset($currentSchema['use_schema']) && $currentSchema['use_schema']=='auto_cards')){
                    
                    self::processAutoCardsBlock($currentSchema,$data['page_sections_container'][$sectionKey]['sections__blocks'][$blockKey]);
                }
            }
        }
    }
    private static function processAutoCardsBlock($currentSchema,&$block){
        foreach($block as $field_key => $field_val){
            $keys = explode('--',$field_key);
            
            if($keys[0]=='settings'){
                $reference = &$currentSchema;
                foreach ($keys as $key) {
                    if (!array_key_exists($key, $reference)) {
                        $reference[$key] = [];
                    }
                    $reference = &$reference[$key];
                }
                $reference = $field_val;
                // unset($reference);
                unset($block[$field_key]);
            }
        }
        $acSettings = $currentSchema['settings']['auto_cards'];
        $args = array(
            'post_type'   => $acSettings['post_type'],
            'numberposts' => -1,
            'post_status' => 'publish',
            'suppress_filters' => false,
            'lang' => defined('ICL_LANGUAGE_CODE') ? ICL_LANGUAGE_CODE : get_bloginfo("language"),

        );
        if(isset($acSettings['list']) && is_array($acSettings['list'])){
            if($acSettings['list'][0] && is_array($acSettings['list'][0]) && $acSettings['list'][0]['id']){
                $acSettings['list'] = array_map(function($item){
                    return $item['id'];
                }, $acSettings['list']);
            }
            $args['post__in'] = $acSettings['list'];
            $args['orderby'] = 'post__in';
        }
        if(isset($acSettings['taxonomies']) && is_array($acSettings['taxonomies'])){
            foreach($acSettings['taxonomies'] as $tax=>$terms){
                if(count($terms)==0){
                    unset($acSettings['taxonomies'][$tax]);
                }
            }
            if(count($acSettings['taxonomies'])>0){
                $args['tax_query'] = [];
                if(count($acSettings['taxonomies'])>1 && $acSettings['taxonomy_relation']){
                    if($acSettings['taxonomy_relation']=='intersection'){
                        $args['tax_query']['relation'] = "AND";
                    }else{
                        $args['tax_query']['relation'] = "OR";
                    }
                }
                if(! array_key_exists('show_descendants',$acSettings)){
                    $acSettings['show_descendants'] = true;
                }
                foreach($acSettings['taxonomies'] as $tax=>$terms){
                    foreach($terms as $termKey => $term){
                        if(is_array($term)){
                            $term = get_term_by('id',$term['id'],$tax);
                        }else{
                            $term = get_term_by('slug',$term,$tax);
                        }
                        if($term!==false){
                            $terms[$termKey] = $term->term_id;
                        }
                    }
                    if($acSettings['show_descendants'] === 'only'){
                        $newTerms = [];
                        foreach($terms as $term){
                            $newTerms = array_merge($newTerms,get_term_children($term,$tax));
                        }
                        $terms = $newTerms;
                        //TODO: opcja 'only' wymaga przetestowania
                        $acSettings['show_descendants'] = true;
                    }
                    $args['tax_query'][] = 
                        array(
                            'taxonomy' => $tax,
                            'field' => 'term_id',
                            'terms' => $terms,
                            'include_children' => ($acSettings['show_descendants'] === true ? true : false)
                        );
                }
            }
        }
        $my_current_lang = apply_filters( 'wpml_current_language', NULL );
        $query = new WP_Query($args);
        $posts = $query->get_posts();
        $processed_posts = array();
        // mapowanie z prefixem: post/, postmeta/, carbon/ albo taxonomy/
        
        // wartość pola określa, z jakiego pola w bazie mają być pobierane dane do wypełnienia go. Domyślnie dane są pobierane z pól posta, ale mogą być te pobrane z przypisanych do danego posta pól meta: postmeta_nazwaklucza, albo z carbon_nazwapola
        foreach($posts as $post){
            $processed_post = array();
            foreach($acSettings['fields'] as $key=>$field){
                if(is_array($field) && isset($field[0])){
                    $field = $field[0];
                }
                if($field){
                    if(substr($field,0,7)=='carbon/'){
                        $processed_post[$key] = carbon_get_post_meta( $post->ID, substr($field,7) );
                    }elseif(substr($field,0,9)=='postmeta/'){
                        $meta = get_post_meta( $post->ID, substr($field,9) );
                        if(is_array($meta) && array_key_exists(0,$meta)){
                            $meta = $meta[0];
                        }
                        $processed_post[$key] = $meta;
                    }elseif(substr($field,0,9)=='taxonomy/'){
                        $terms = wp_get_post_terms( $post->ID, substr($field,9) );
                        if(is_array($terms) && array_key_exists(0,$terms)){
                            $processed_post[$key] = array_map( function($term) { return $term->name;}, $terms );
                        }
                    }elseif(substr($field,0,9)=="function/"){
                        $functionName = substr($field,9);
                        if(function_exists($functionName)){
                            $processed_post[$key] = call_user_func($functionName,$post);
                        }
                    }elseif(substr($field,0,5)=="post/"){
                        $propertyName = substr($field,5);
                        if(property_exists($post,$propertyName)){
                            $processed_post[$key] = $post->$propertyName;
                        }
                    }elseif($field){
                        $processed_post[$key] = $field;
                    }
                }else{
                    $processed_post[$key] = '';
                }
            }
            $limit_check = true;
            if(is_array($acSettings['limits'])){
                foreach($acSettings['limits'] as $limit){
                    if(array_key_exists('field',$limit) && array_key_exists($limit['field'],$processed_post) && (array_key_exists('lower_limit',$limit) || array_key_exists('upper_limit',$limit))){
                        if(!array_key_exists('type',$limit)){
                            $limit['type'] = 'text';
                        }
                        switch($limit['type']){
                            case 'bool':
                                if(array_key_exists('lower_limit',$limit) && array_key_exists('upper_limit',$limit) && $limit['lower_limit']==$limit['upper_limit'] && $processed_post[$limit['field']] != $limit['lower_limit']){
                                    $limit_check = false;
                                    break 2;
                                }
                                break;
                            case 'date':
                                if(! is_string($processed_post[$limit['field']])){
                                    $processed_post[$limit['field']] = '';
                                }
                                if(array_key_exists('lower_limit',$limit) && $limit['lower_limit']!='' && strtotime($limit['lower_limit']) > strtotime($processed_post[$limit['field']])){
                                    $limit_check = false;
                                    break 2;
                                }
                                if(array_key_exists('upper_limit',$limit) && $limit['upper_limit']!='' && strtotime($limit['upper_limit']) < strtotime($processed_post[$limit['field']])){
                                    $limit_check = false;
                                    break 2;
                                }
                                break;
                            case 'number':
                                if(! is_numeric($processed_post[$limit['field']])){
                                    $processed_post[$limit['field']] = 0;
                                }
                                if(array_key_exists('lower_limit',$limit) && $limit['lower_limit']!='' && (float) $limit['lower_limit'] > (float) $processed_post[$limit['field']]){
                                    $limit_check = false;
                                    break 2;
                                }
                                if(array_key_exists('upper_limit',$limit) && $limit['upper_limit']!='' && (float) $limit['upper_limit'] > (float) $processed_post[$limit['field']]){
                                    $limit_check = false;
                                    break 2;
                                }
                                break;
                            case 'text':
                            default:
                                if(! is_string($processed_post[$limit['field']])){
                                    $processed_post[$limit['field']] = '';
                                }
                                if(array_key_exists('lower_limit',$limit) && $limit['lower_limit']!='' && strcasecmp($limit['lower_limit'],$processed_post[$limit['field']])>0){
                                    $limit_check = false;
                                    break 2;
                                }
                                if(array_key_exists('upper_limit',$limit) && $limit['upper_limit']!='' && strcasecmp($limit['upper_limit'],$processed_post[$limit['field']])<0){
                                    $limit_check = false;
                                    break 2;
                                }

                        }
                    }
                }
            }
            if($limit_check){
                $processed_posts[] = $processed_post;
            }
        }
        if(is_array($acSettings['sort']) && count($processed_posts) > 0){
            foreach($acSettings['sort'] as $sortParameters){
                if(array_key_exists('field',$sortParameters) && $sortParameters['field']){
                    foreach($processed_posts as &$processed_post){
                        if(!array_key_exists($sortParameters['field'],$processed_post) || !is_string($processed_post[$sortParameters['field']])){
                            $processed_post[$sortParameters['field']] = '';
                        }
                    }
                    if(!array_key_exists('type',$sortParameters)){
                        $sortParameters['type'] = 'text';
                    }
                    switch($sortParameters['type']){
                        case 'date':
                            if(array_key_exists('direction',$sortParameters) && $sortParameters['direction']=='DOWN'){
                                uasort($processed_posts,function($a,$b) use ($sortParameters) {
                                    return strtotime($b[$sortParameters['field']]) <=> strtotime($a[$sortParameters['field']]);
                                });
                            }elseif(array_key_exists('direction',$sortParameters) && $sortParameters['direction']=='UP'){
                                uasort($processed_posts,function($a,$b) use ($sortParameters) {
                                    return strtotime($a[$sortParameters['field']]) <=> strtotime($b[$sortParameters['field']]);
                                });
                            }
                            break;
                        case 'number':
                            if(array_key_exists('direction',$sortParameters) && $sortParameters['direction']=='DOWN'){
                                uasort($processed_posts,function($a,$b) use ($sortParameters) {
                                    return (float) $b[$sortParameters['field']] <=> (float) $a[$sortParameters['field']];
                                });
                            }elseif(array_key_exists('direction',$sortParameters) && $sortParameters['direction']=='UP'){
                                uasort($processed_posts,function($a,$b) use ($sortParameters) {
                                    return (float) $a[$sortParameters['field']] <=> (float) $b[$sortParameters['field']];
                                });
                            }
                            break;
                        case 'text':
                        default:
                            if(array_key_exists('direction',$sortParameters) && $sortParameters['direction']=='DOWN'){
                                uasort($processed_posts,function($a,$b) use ($sortParameters) {
                                    return strcasecmp($b[$sortParameters['field']], $a[$sortParameters['field']]);
                                });
                            }elseif(array_key_exists('direction',$sortParameters) && $sortParameters['direction']=='UP'){
                                uasort($processed_posts,function($a,$b) use ($sortParameters) {
                                    return strcasecmp($a[$sortParameters['field']], $b[$sortParameters['field']]);
                                });
                            }
                            break;

                    }
                }
            }
        }
        if($acSettings['max'] > -1){
            $processed_posts = array_slice($processed_posts,0,$acSettings['max']);
        }
        $block[$block['_type'].'__block'] = $processed_posts;
        $block['post_type'] = $acSettings['post_type'];
    }
    private static function filterLimits($limits,$processed_posts){

    }

    private static function sort($sort,$processed_posts){

    }
}