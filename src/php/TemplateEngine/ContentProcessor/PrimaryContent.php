<?php
namespace Superskrypt\WpBackendBase\TemplateEngine\ContentProcessor;

use Superskrypt\WpBackendBase\TemplateEngine\TemplateStructureGenerator;

use WP_Query;

class PrimaryContent {
    public static function processPrimaryContent(&$data){
        $primaryContentData = carbon_get_the_post_meta("primary_content");

        $primaryContent = array();
        if(is_array($primaryContentData) && count($primaryContentData) > 0){
            //
            // Budujemy tablicę primaryContent i ustawiamy typ poszczególnych pól
            //
            foreach(TemplateStructureGenerator::$structureConfigCache[get_post_type()]["primary_content"] as $block){
                $primaryContent[$block] = array("_type" => $block);
            }
            //
            // Wypełniamy tablicę primaryContent zawartością hierarchiczną na podstawie listy pól z nazwą bloku w kluczu
            //
            foreach($primaryContentData[0] as $key=>$val){
                self::addField($primaryContent, $key, $val);
            }
            //
            // Usuwamy bloki bez zawartości
            //
            foreach($primaryContent as $blockName=>$fields){
                self::removeBlockIfEmpty($primaryContent, $blockName, $fields);
            }
        }
        $data["primary_content"] = $primaryContent;
    }

    private static function addField(&$primaryContent, $key, $val){
        $keyNameParts = explode("__",$key);
        if($keyNameParts[0]=="primary_content"){
            $blockName = $keyNameParts[1];
            $fieldName = implode("__",array_slice($keyNameParts,2));
            if(array_key_exists($blockName,$primaryContent) && ! array_key_exists($fieldName,$primaryContent[$blockName])){
                $primaryContent[$blockName][$fieldName] = $val;
            }
        }
    }
    private static function removeBlockIfEmpty($primaryContent, $blockName, $fields){
        $check = false;
        foreach($fields as $key=>$field){
            if($key!="_type" && ((!is_array($field) && trim($field)) || (is_array($field) && count($field)>0))){
                $check = true;
            }
        }
        if(!$check){
            unset($primaryContent[$blockName]);
        }
    }
}