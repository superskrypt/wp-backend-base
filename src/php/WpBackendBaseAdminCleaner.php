<?php

namespace Superskrypt\WpBackendBase;


class WpBackendBaseAdminCleaner {
    public static function hideTemplateSelection() {
    	add_action("admin_footer", function() {
    		?>
		    <script type="text/javascript">
		        jQuery(function ($) {
                    console.log('sdvsdvsdv')
		            $('#page_template').prop('disabled', 'true').css('display', 'none').prevUntil('select').css('display', 'none');
		            $('select[name="page_template"]').prop('disabled', 'true').parents('label').css('display', 'none');
		        });
		    </script>
    		<?php
    	}, 11);   
    }

    public static function hideThemesSubmenu() {
    	add_action('admin_head', function() {
    		global $submenu;
            if(isset($submenu['themes.php']) && is_array($submenu['themes.php'])) {
                foreach ( $submenu['themes.php'] as $i => $item ) {
                    if ( $item[1] === 'customize' || $item[1] === 'edit_themes' || (!is_super_admin() && $item[1]=== 'switch_themes')) {
                        unset( $submenu['themes.php'][ $i ] );
                    }
                }
            }
		    // if (true || !is_super_admin()) {
		    //     remove_submenu_page( 'themes.php', 'themes.php' );
		    //     remove_submenu_page( 'themes.php', 'theme-editor.php' );
		    //     remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Findex.php' );
		    // }
        }, 11);   
    }
        public static function disableComments() {
            add_action('admin_init', function() {
                $post_types = get_post_types();
                foreach ($post_types as $post_type) {
                    if(post_type_supports($post_type, 'comments')) {
                        remove_post_type_support($post_type, 'comments');
                        remove_post_type_support($post_type, 'trackbacks');
                    }
                }
            });
            add_filter('comments_open', function() {
                return false;
            }, 20, 2);
            add_filter('pings_open', function() {
                return false;
            }, 20, 2);
            add_filter('comments_array', function($comments) {
                $comments = array();
                return $comments;
            }, 10, 2);
            add_action('admin_menu', function() {
                remove_menu_page('edit-comments.php');
            });
            add_action('admin_init', function() {
                global $pagenow;
                if ($pagenow === 'edit-comments.php') {
                    wp_redirect(admin_url()); exit;
                }
            });
            add_action('admin_init', function() {
                remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
            });
            add_action('init', function() {
                if (is_admin_bar_showing()) {
                    remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
                }
            });
         }

         public static function disablePosts() {
            add_action('admin_menu', function () {
                remove_menu_page('edit.php');
            });
        }
    
        public static function reorderMenuPagesToTop() {
            add_filter( 'custom_menu_order', '__return_true' );
            add_filter( 'menu_order', function() {
                return array( 'index.php', 'separator1', 'edit.php?post_type=page' );
            } );
        }
        public static function themeSetup($options) {

            update_option('image_default_link_type', 'none');

            add_action('init', function() use ($options) {

                if(!$options['show_admin_bar']) {
                    add_filter('show_admin_bar', '__return_false');
                }
                
                foreach ($options['add_theme_support'] as $feature => $post_type) {
                    if(is_array($post_type)) {
                        add_theme_support( $feature, $post_type );
                    }
                    else {
                        add_theme_support( $post_type);
                    }
                
                }
                foreach ($options['add_post_type_support'] as $feature => $post_type) {
                    add_post_type_support( $post_type, $feature );
                }
                foreach ($options['remove_post_type_support'] as $feature => $post_type) {
                    remove_post_type_support( $post_type, $feature );
                }
                foreach ($options['update_default_images_sizes'] as $feature => $imageSize) {

                    self::update_default_image_size($feature, $imageSize);
                }
                foreach ($options['add_custom_images_sizes'] as $feature => $imageSize) {  
                    self::addCustomImageSize($feature, $imageSize);
                }

            });
            
        }

        public static function addCustomImageSize($name, $imageSize) {
            $width = null;
            $height = null;
            if(is_array($imageSize)) {
                $width = isset($imageSize['width']) ? $imageSize['width'] : null;
                $height = isset($imageSize['height']) ? $imageSize['height'] : null;
            }
            else {
                $width =  $imageSize;
                $height = $imageSize;
            }
            add_image_size( $name, $width, $height );
        }

        public static function update_default_image_size($type, $sizeWidth, $sizeHeight = null) {
            $suffix_for_width = '_size_w';
            $suffix_for_height = '_size_h';
            $height = $sizeHeight ? $sizeHeight : $sizeWidth;

            update_option($type . $suffix_for_width, $sizeWidth);
            update_option($type . $suffix_for_height, $height);
        }
}
?>