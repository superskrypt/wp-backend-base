<?php

namespace Superskrypt\WpBackendBase;

class Shortcodes {
    const REGISTERED_SHORTCODES = array(
        'lang'
    );

    /**
     * @param array $shortCodesList tablica ze slugami shotrcodów.
     * @return void
     */
    public static function enable($shortCodesList) {
        if(!is_array($shortCodesList)) {
            return;
        }
        foreach($shortCodesList as $shortCodeName) {
            $shortCodeFilePath = __DIR__ . '/Shortcodes/' . $shortCodeName . '_shortcode.php';
            if(file_exists($shortCodeFilePath)) {
                include_once $shortCodeFilePath;
            }
        }
    }
}