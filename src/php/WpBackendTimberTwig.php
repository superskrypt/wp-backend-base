<?php
namespace Superskrypt\WpBackendBase;

use Timber;

class WpBackendTimberTwig {
    static $wpBaseUri = ""; // relatywna ścieżka do instalacji wtyczki/motywu z wp-backend-base
	public static function setupTimberTwig($wpBaseUri) {
        self::$wpBaseUri = $wpBaseUri;
        add_filter('timber/twig', array(__CLASS__, 'addToTwig'));
        add_filter( 'timber/context', array(__CLASS__, 'addToContext'));
    }

    public static function addToContext($context) {
        $context['backend_base_text_domain'] = WpBackendBase::$backendTextDomain;

        $backendBaseInstallationRelPath = '/vendor/superskrypt/wp-backend-base/src';
        $context['gallery_icons_file_path'] = self::$wpBaseUri . $backendBaseInstallationRelPath . '/svg-icons/gallery-icons.svg';
        return $context;
    }

    public static function addToTwig($twig) {
        $twig->addFunction( new Timber\Twig_Function(
            'getCurrentLanguage', array(__CLASS__, 'getCurrentLanguage')
        ));
        $twig->addFunction( new Timber\Twig_Function(
            'getActiveLanguages', array(__CLASS__, 'getActiveLanguages')
        ));
        $twig->addFilter(new Timber\Twig_Filter(
            'getLanguageAriaLabel', array(__CLASS__, 'getLanguageAriaLabel')
        ));
        $twig->addFilter( new Timber\Twig_Filter(
            'formatDate', array(__CLASS__, 'formatDate')
        ));
        $twig->addFilter(new Timber\Twig_Filter(
            'prepareSlideList', array(__CLASS__, 'prepareSlideList')
        ));
        $twig->addFunction(new Timber\Twig_Function(
            'getTaxonomies', array(__CLASS__,'getTaxonomies')
        ));
        $twig->addFunction(new Timber\Twig_Function(
            'getPostTaxonomies', array(__CLASS__,'getPostTaxonomies')
        ));
        $twig->addFunction(new Timber\Twig_Function(
            'getEnabledFiltersFromBlock', array(__CLASS__,'getEnabledFiltersFromBlock')
        ));
        
        $twig->addFunction(new Timber\Twig_Function(
            'generateDataAttributesForFiltering', array(__CLASS__,'generateDataAttributesForFiltering', array('alternative' => 'generateDataAttributesForFilteringTHEME'))
        ));
        $twig->addFilter(new Timber\Twig_Filter(
            'normalizeUrl',array(__CLASS__, 'normalizeUrl')
        ));

        // $twig->addFunction( new Timber\Twig_Function( 'isCurrentURL', array(__CLASS__, 'isCurrentURL') ) );
        // $twig->addFilter( new Timber\Twig_Filter( 'addNoFollowTagToExternalLinks', array(__CLASS__, 'addNoFollowTagToExternalLinks') ) );
        // $twig->addFilter( new Timber\Twig_Filter( 'nl2ol', array(__CLASS__, 'nl2ol') ) );
        // $twig->addFilter( new Timber\Twig_Filter( 'ndashSpace', array(__CLASS__, 'ndashSpace') ) );
        return $twig;
    }
    
    public static function getCurrentLanguage() {
        return substr(get_locale(),0,2);
    }

    public static function getActiveLanguages() {
        $languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );

        return $languages ? $languages : [];
    }

    public static function getLanguageAriaLabel($languageCode = 'pl') {
        $text = carbon_get_theme_option('switch_to_'. $languageCode . '_label');
        return !empty($text) ? $text : "";
    }

    public static function formatDate($date) {
        return date('d.m.Y',strtotime($date));
    }

    public static function prepareSlideList($slides){
        foreach($slides as $key=>$slide){
            $imagePost = get_post($slide);
            $imageMeta = wp_get_attachment_metadata($slide);
            $youtubeID = get_post_meta($slide,'media_video_link');
            if(is_array($youtubeID) && array_key_exists(0,$youtubeID) ){
                $youtubeID = $youtubeID[0];
            }else{
                $youtubeID = false;
            }
            $type = 'picture';
            if($youtubeID){
                $type = 'video';
            }
            if(array_key_exists('fileformat',$imageMeta) && $imageMeta['fileformat'] == 'mp3'){
                $type = 'audio';
            }
            $slides[$key] = array('image' => $slide,'description' => $imagePost->post_content,'video' => $youtubeID, 'type' => $type);
        }
        return $slides;
    }

    //TODO tutaj trzeba by jeszcze dać możliwość pobierania specyficznych termów
    public static function getTaxonomies($taxonomySlug, $postType) {
        // $terms = get_terms( array(
        //     'taxonomy'   => $taxonomySlug,
        //     'hide_empty' => true,
        // ) );
        // return $terms;
        $posts_in_post_type = get_posts( array(
            'fields' => 'ids',
            'post_type' => $postType,
            'posts_per_page' => -1,
        ) );
        $terms = wp_get_object_terms( $posts_in_post_type, $taxonomySlug, array( 'ids' ) );
        return $terms;
    }

    /**
     * @param string $postUrl
     * @param string $taxonomyType
     * @return WP_TERM[]
     */
    public static function getPostTaxonomies($postUrl, $taxonomyType) {
        $postId = url_to_postid($postUrl);
        return wp_get_post_terms($postId, $taxonomyType );
    }

    /**
     * @param array $blockFields
     * @return array Zwraca tablice zawierająca slugi taksonomii, po których powinno odbywać sie filtrowanie. Pola aktywacji filtrów powinny posiadać slugi według schematu "enable_filters_TAXONOMY_SLUG"
     */
    public static function getEnabledFiltersFromBlock($blockFields) {
        $filter_checkbox_field_slug_prefix = 'enable_filters';
        return array_filter(array_map(function ($blockFieldName, $blockFieldValue) use ($filter_checkbox_field_slug_prefix) {
            if(str_contains($blockFieldName, $filter_checkbox_field_slug_prefix)) {
                return !empty($blockFieldValue) && $blockFieldValue !== false  ? ltrim(str_replace($filter_checkbox_field_slug_prefix, '', $blockFieldName), '__') : false;
            }
            return false;
        }, array_keys($blockFields), array_values($blockFields)));

    }

    /**
     * @param string $postUrl
     * @param string[] $taxonomySlugs
     * @return string
     */
    public static function generateDataAttributesForFiltering($postUrl, $taxonomySlugs) {
        if(empty($postUrl) || empty($taxonomySlugs) ) {
            return "";
        }
        $dataFilterAttrs = [];
        foreach($taxonomySlugs as $taxonomySlug) {
            $terms = self::getPostTaxonomies($postUrl, $taxonomySlug );
            $dataAttr = 'data-terms-' .  $taxonomySlug . '="' . implode(', ', array_map(function($t) {
                return $t->term_id;
            }, $terms)) . '"';
            array_push($dataFilterAttrs, $dataAttr);
        }
        return implode(' ', $dataFilterAttrs);
    }

    public static function normalizeUrl($url) {
        return rtrim($url, '/'); 
    }

    // public static function isCurrentURL($link) {
    //     if(substr($link,-1) == '/'){
    //         $link = substr($link,0,-1);
    //     }
    //     $full_url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    //     if(substr($full_url,-1) == '/'){
    //         $full_url = substr($full_url,0,-1);
    //     }
    //     $end_url = "$_SERVER[REQUEST_URI]";
    //     if(substr($end_url,-1) == '/'){
    //         $end_url = substr($end_url,0,-1);
    //     }
    //     if($link == $full_url || $link == $end_url){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }
    // public static function addNoFollowTagToExternalLinks($url = "") {
    //     if(!$url) return;
    //     $homeUrl = get_home_url();
    //     $siteDomain = parse_url($homeUrl, PHP_URL_HOST);
    //     if(substr($url,0, 1) === '/') {
    //         $url = $homeUrl . $url;
    //     }
    //     return preg_match("/\/{$siteDomain}/i", $url) === 1 ?  "" : 'rel="nofollow"';
    // }
    // public static function nl2ol($txt) {
    //     $lines = explode(PHP_EOL,$txt);
    //     if(count($lines)<=1){
    //         return $txt;
    //     }else{
    //         $newtxt = "<ol>";
    //         foreach($lines as $line){
    //             $newtxt .= "<li>".$line."</li>";
    //         }
    //         $newtxt .= "</ol>";
    //     }
    //     return $newtxt;
    // }
    // public static function ndashSpace($txt){
    //     return str_replace('– ','–&nbsp;',$txt);
    // }
}