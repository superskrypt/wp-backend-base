<?php

namespace Superskrypt\WpBackendBase;

class WpBackendBaseHelpers {

    /**
     * Metoda pomocnicza pozwalająca na pobranie względnej ścieżki do wtyczki lub theme w którym inicjalizowany jest wp-backend-base.
     * Zwraca ścieżkę z dwoma poziomami od /wp-content np.: /wp-content/plugins/nazwa-wtyczki lub /wp-content/themes/nazwa-motywu.
     * @abstract
     * @return string; 
     */
    public static function getBackendBaseContextPath () {

        $pathEls = explode('/', __DIR__);
        $wpContentPath = '';
        foreach($pathEls as $index => $pathEl) {
            if($pathEl === 'wp-content') {
                $wpContentPath = "/" . implode('/',array_slice($pathEls, $index, 3));
                break;
            }
        }

        // Zabezpieczenie gdy wp-backend-base jest podpinany jako dowiązanie symboliczne np. podczas developmentu.
        if(empty($wpContentPath)) {
            return self::getBackendBaseContextPathWithBacktrace();
        }
        return $wpContentPath;
    }

    /**
     * Metoda pomocnicza pozwalająca na pobranie względnej ścieżki do wtyczki lub theme w którym inicjalizowany jest wp-backend-base.
     * Metoda ta dedykowana do użycia podczas developmentu gdy wp-backend-base podpięty jest jako dowiązanie symboliczne
     * Zwraca ścieżkę z dwoma poziomami od /wp-content np.: /wp-content/plugins/nazwa-wtyczki lub /wp-content/themes/nazwa-motywu
     * @abstract
     * @return string; 
     */
    public static function getBackendBaseContextPathWithBacktrace() {
        $wpContentFolderName = 'wp-content';
        $backtrace = debug_backtrace();

        if(count($backtrace) > 0) {
            $path = count($backtrace) > 0 && isset($backtrace[0]['file']) ? $backtrace[0]['file'] : "";
            //sprawdź w którym fragmencie jest wywołanie ze ścieżki z wp-content
            foreach($backtrace as $traceEl) {
                if(isset($traceEl['file']) && str_contains( $traceEl['file'], $wpContentFolderName)) {
                    $path = $traceEl['file'];
                    break;
                }
            }
        }
        $pathEls = explode('/', $path);
        $wpContentPath = '';
        foreach($pathEls as $index => $pathEl) {
            if($pathEl === 'wp-content') {
                $wpContentPath = "/" . implode('/',array_slice($pathEls, $index, 3));
                break;
            }
        }
        return $wpContentPath;
    }

    public static function flattenArray($arr) {
        $flattenArr= [];
        array_walk_recursive($arr, function($item, $key) use (&$flattenArr) {
            array_push($flattenArr, $item);
        });
        return $flattenArr;
    }
}