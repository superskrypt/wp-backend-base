<?php
namespace Superskrypt\WpBackendBase;

use Superskrypt\WpBackendBase\TemplateStructureBuilder;
class WpBackendAdminConfig {


    public static function showMenuToEditor() {
		add_action('after_switch_theme', function() {
		    $roleObject = get_role( 'editor' );
		    if (!$roleObject->has_cap( 'edit_theme_options' ) ) {
		        $roleObject->add_cap( 'edit_theme_options' );
		    }
		});
    }
    
    public static function addSupportForSvg() {
		add_filter('upload_mimes', function( $existing_mimes=array() ) {
		    $existing_mimes['svg'] = 'image/svg+xml';
		    return $existing_mimes;
		});
 	}
	public static function fixSvgImageAttachment() {
        add_filter( 'wp_get_attachment_image_src', function($image, $attachment_id, $size, $icon) {
           if (is_array($image) && preg_match('/\.svg$/i', $image[0]) && $image[1] <= 1) {
               if(is_array($size)) {
                   $image[1] = $size[0];
                   $image[2] = $size[1];
               } elseif(($xml = @simplexml_load_file($image[0])) !== false) {
                   $attr = $xml->attributes();
                   $viewbox = explode(' ', $attr->viewBox);
                   $image[1] = isset($attr->width) && preg_match('/\d+/', $attr->width, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[2] : null);
                   $image[2] = isset($attr->height) && preg_match('/\d+/', $attr->height, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[3] : null);
               } else {
                   $image[1] = $image[2] = null;
               }
           }
           return $image;
       }, 10, 4 );
   }

    public static function registerMenu($options) {
		add_action('init', function() use ( $options ) {
       		foreach ($options as $slug => $name) {
		    	register_nav_menus(array(
					$slug => __( $name )
			    ));
			}
		});
	 }

}

?>