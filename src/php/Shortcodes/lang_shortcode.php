<?php
/**
 * Przykład wywołania
 * [lang l=en] This is content in english[/lang]
 * 
 * @param array $atts parametry przekazane w shortcode
 * @param string $content treść zawarta pomiędzy znacznikiem otwierającym a zamykającym shortcode
 * @return string
 */
function langShortCode($atts, $content) {
    $atts = shortcode_atts (array(
        'l' => null, 
    ), $atts);
    if(!empty($atts['l'])) {
        return '<span lang="' . $atts['l'] . '">' . do_shortcode($content) . '</span>';
    }
    return do_shortcode($content);

}
add_shortcode( 'lang', 'langShortCode' );