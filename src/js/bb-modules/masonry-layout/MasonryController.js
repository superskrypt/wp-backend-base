import Macy from 'macy';

export default class MasonryController {
    
    constructor(configOptions = {}) {
        const defaultConfigOptions = {
            mobileBreakpoint: '900',
            margin: 24, //gap pomiedzy kolumnami
            columns: 3,
            containers: ['.block-cards .block__inner'],
            trueOrder: false,
            waitForImages: false,
            breakAt: {
                [900] : { columns: 1 }
            }
        }
        this.configOptions = Object.assign({}, defaultConfigOptions, configOptions);
        this.config();
        window.addEventListener('spa:mount', this.mount.bind(this) );
        window.addEventListener('spa:unmount', this.unmount.bind(this) );
        this.mount();
    }

    config() {
        this.layoutInstances = [];
        this.elementInstances = new Map();
    }

    init() {
        this.masonryInstances = [];
        this.configOptions?.containers.forEach(masonryContainerClass => {
            const masonryContainers = document.querySelectorAll(`${masonryContainerClass}`);
            if(masonryContainers && masonryContainers.length > 0) {
                [...masonryContainers].forEach((el) => {
                    this.setupMasonryLayout(el, this.configOptions?.columns);
                });
            }
        });
        document.addEventListener('cards:filter', this.reload.bind(this))
    }

    setupMasonryLayout(containerSelector,numberOfCols = 2) {
        const macyInstanceOptions = Object.assign(
            {}, 
            this.configOptions,
            { container: containerSelector, columns: numberOfCols }
        );
        const instance = Macy(macyInstanceOptions);
        this.masonryInstances.push(instance);
        this.layoutInstances.push(instance);
        this.elementInstances.set(containerSelector, instance);
        instance.recalculate(true, true);
        this.setMacyEventImageCompleteHandler(instance);

        /* instance.runOnImageLoad(function () {
            instance.recalculate(true, true);
        }, true); */
    }

    setMacyEventImageCompleteHandler(instance) {
        if (instance != null && typeof instance === 'object') {
            instance.on(instance.constants.EVENT_IMAGE_COMPLETE, () => {
                setTimeout(function() {
                    instance.recalculate(true, true);
                }, 1500);
            });
        }
    }

    reload() {
        this.masonryInstances.forEach(instance => {
            instance.recalculate(true, true);
        });
    }

    mount() {
        this.init();
    }
    unmount() {
        this.layoutInstances.map((layoutInstance) => {
            layoutInstance.remove();
        });
    }
}