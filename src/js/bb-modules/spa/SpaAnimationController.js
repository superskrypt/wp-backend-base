import SpaController from './SpaController.js';

export default class SpaAnimationController extends SpaController {

    animationInit(){
        window.addEventListener('preloader:startloop',this.animationNext.bind(this));
    }

    curtainClose(){
        window.preloader.restart();
    }

    animationNext(){
        this.nextItem();
    }

    curtainOpen(){
        window.preloader.onLoaded();
        this.closeMenu();
        this.nextItem();
    }
}