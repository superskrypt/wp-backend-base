import SwupBodyClassPlugin from '@swup/body-class-plugin';
import SwupDebugPlugin from '@swup/debug-plugin';
import SwupFormsPlugin from '@swup/forms-plugin';
import SwupJsPlugin from '@swup/js-plugin';
import Swup from 'swup';

export default class SpaController {

    constructor() {
        this.customOptions = [];
        this.debug = false;
        this.reloadList = [];
        this.init();
    }

    init() {
        this.animationInit();
        this.configSPA();
    }

    animationInit(){

    }

    configSPA() {
        this.body = document.getElementsByTagName('body')[0];
        this.hash = '';
        this.closeMenuEvent = new Event("spa:close-menu");
        this.unmountEvent = new Event("spa:unmount");
        this.mountEvent = new Event("spa:mount");
        this.finishedEvent = new Event("spa:finished");
        this.components = new Array();

        this.options = {
            // linkSelector:
            // 'a[href^="' +
            // window.location.origin +
            // '"]:not([data-no-swup]):not([href$=".pdf"]):not([href$=".zip"]):not([href$=".docx"]):not([href$=".txt"]), a[href^="/"]:not([data-no-swup])',
            linkSelector:
            'a[href^="' +
            window.location.origin +
            '"]:not([data-no-swup]):not([href$=".pdf"]):not([href$=".zip"]):not([href$=".docx"]):not([href$=".txt"]), a[href^="\\/"]:not([data-no-swup]), a[href^="\\\\"]:not([data-no-swup])',
            containers: [".main-container"],
            animateHistoryBrowsing: true,
            plugins: [
                new SwupJsPlugin([
                    {
                        from: '(.*)',
                        to: '(.*)',
                        out: (next) => {
                            this.nextItem = next;
                            this.curtainClose();
                        },
                        in: (next) => {
                            this.nextItem = next;
                            next();
                        }
                    }
                ]),
                new SwupBodyClassPlugin(),
                new SwupFormsPlugin(),
            ]
        }
        this.options = { ...this.options, ...this.customOptions };
        if(this.debug){
            this.options.plugins.push(new SwupDebugPlugin());
        }
        this.swup = new Swup.default(this.options);
        this.swup.hooks.on('link:click',this.handleClick.bind(this));
        this.swup.hooks.on('content:replace',this.handleUnmounts.bind(this));
        this.swup.hooks.on('content:replace',this.handleMounts.bind(this));
        this.swup.hooks.on('history:popstate',this.handlePopState.bind(this));

    }
    handlePopState(event){
        if ('scrollRestoration' in history) {
            history.scrollRestoration = 'manual';
        }
    }
    handleClick(event){
        if(Object.hasOwn(event,'target')){
            if(Object.hasOwn(event.target,'hash') && event.target.hash){
                this.hash = event.target.hash;
            }else if(Object.hasOwn(event.target.parentElement,'hash') && event.target.parentElement.hash){
                this.hash = event.target.parentElement.hash;
            }
        }
    }
    handleUnmounts(event) {
        window.dispatchEvent(this.unmountEvent);
    }
    handleMounts(event) {
        const regex = /html lang\=\"([A-Za-z0-9 -_]*?)\"/;

        const str = this.swup.cache.get(this.swup.getCurrentUrl()).html;
        this.reloadScripts(str);
        let m;
        if ((m = regex.exec(str)) !== null) {
            if(m[1] != undefined){
                document.getElementsByTagName('html')[0].setAttribute('lang',m[1]);
                this.lang = m[1];
            }
        }
        window.dispatchEvent(this.mountEvent);
        console.log('scroll SPA');
        this.handleGA();
        // window.scrollTo(0,0);
        if(this.hash){
            const target = document.querySelector(this.hash);
            if(target != null){
                window.scrollTo(0,target.getBoundingClientRect().top);
            }
            this.hash = '';
        }
    }

    handleGA(loc = false){
        if(typeof ga === 'function') {
            if(!loc){
                loc = location.pathname;
            }
            const currentLanguage = document.getElementsByTagName('html')[0].getAttribute('lang').toLowerCase();
            // console.log('ga');
            // console.log(ga);
            ga('set', 'language', currentLanguage);
            // console.log(ga.getAll);
            ga.getAll()[0].set('page',loc);
            ga.getAll()[0].send('pageview');
        }
    }
    handleSamePage(event){
        if(event.target.classList.contains('menu-link')){
            this.closeMenu();
        }
    }
    closeMenu(){
        window.dispatchEvent(this.closeMenuEvent);
    }
    nextItem(){
        
    }
    curtainClose(){
        this.nextItem();
    }
    curtainOpen(){
        this.closeMenu();
        this.nextItem();
    }
    reloadScripts(str) {
        const parser = new DOMParser();
        this.newDom = parser.parseFromString(str, "text/html");
        let oldScripts = [];
        let oldScriptsIds = [];
        let newScripts = [];
        for(let place of ['head','body']){
            oldScripts = document.getElementsByTagName(place)[0].getElementsByTagName('script');
            for(let i = oldScripts.length - 1;i>=0;i--){
                let x = oldScripts[i];
                if(x.id != undefined && x.id){
                    if(this.reloadList.includes(x.id)){
                        x.remove();
                    }else{
                        oldScriptsIds.push(place+'_'+x.id);
                    }
                }
            }
            let newScriptsForPlace = [...this.newDom.getElementsByTagName(place)[0].getElementsByTagName('script')];
            newScripts = newScripts.concat(newScriptsForPlace.map((x)=>{x.place = place;return x}));
        }
        Promise.all([...newScripts].map(function(x){ 
            if(x.place && x.id && !oldScriptsIds.includes(x.place+'_'+x.id)){
                let scriptEle = document.createElement("script");

                if(x.id != undefined && x.id){
                    scriptEle.setAttribute("id", x.id);
                }
                if(x.src != undefined && x.src){
                    scriptEle.setAttribute("src", x.src);
                }
                scriptEle.setAttribute("type", "text/javascript");
                scriptEle.setAttribute("async", x.getAttribute('async'));
            
                document.getElementsByTagName(x.place)[0].appendChild(scriptEle);

                return new Promise(function(resolve, reject){

                    // success event 
                    scriptEle.addEventListener("load", (e) => {
                        // console.log("File loaded");
                        // console.log(e);
                        resolve();
                    });
                    // error event
                    scriptEle.addEventListener("error", (ev) => {
                        console.log("Error on loading file", ev);
                        resolve();
                    });
                });
            }
            else{
                return new Promise(function(resolve, reject){ resolve(); });
            }
        }))
        .then(() => { 
            console.log("All scripts loaded");
            this.curtainOpen();
            // Everything is loaded at this point.
        });

        
    }
}