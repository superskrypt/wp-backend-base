import { Howl } from 'howler';

export default class AudioPlayerController {

    static createInstances(target = document, selector = '.audio-player') {
        const audioPlayers = target.querySelectorAll(selector);
        audioPlayers.forEach(audioPlayer => {
            new AudioPlayerController(audioPlayer);
        });
    }

    constructor(audioPlayer) {
        this.player = audioPlayer;
        console.log(this.player);
        this.player.controller = this;
        this.init();
    }

    init()
    {
        this.loaded = this.loaded.bind(this);
        this.play = this.play.bind(this);
        this.stop = this.stop.bind(this);

        this.player.url = this.player.dataset.src;
        if(this.player.url) {
            this.player.button = this.player.getElementsByClassName('audio-player__button')[0];
            this.player.audio  = new Howl({
                src: [this.player.url],
                html5: true,
                volume: 1,
                autoplay: false,
                loop: false,
                onload: this.loaded,
                onloaderror: (sound_id,err) => {
                    console.log("sound id: "+sound_id);
                    console.log(err);
                },
                onend: () => {
                }
            });
            this.player.button.addEventListener('click',() => {
                if(this.player.audio.playing()){
                    this.stop();
                }else if(this.player.audio.state() == 'loaded'){
                    this.play();
                }

            });
            this.player.bar = this.player.getElementsByClassName('audio-player__bar')[0];
            this.player.progressBar = this.player.getElementsByClassName('audio-player__bar--progress')[0];
            this.player.timeField = this.player.getElementsByClassName('audio-player__time')[0];
            this.player.durationField = this.player.getElementsByClassName('audio-player__duration')[0];
            this.player.timeLabel = this.player.timeField.getAttribute('aria-label');
            this.player.durationLabel = this.player.durationField.getAttribute('aria-label');
            this.player.ticker = setInterval( () => {
                if(this.player.audio.playing()){
                    let s = Math.round(this.player.audio.seek());
                    this.player.progressBar.style.width = (100 * s / this.player.duration ) + '%';
                    this.player.timeField.innerHTML = (s-(s%=60))/60+(9<s?':':':0')+s;
                    this.player.timeField.setAttribute('aria-label',this.player.timeLabel + ': ' + (s-(s%=60))/60 + ' min ' + s + ' s');
                }
            },300);
            this.player.bar.addEventListener('click',(e) => {
                if(this.player.audio.state() == 'loaded'){
                    const target = e.offsetX / this.player.bar.offsetWidth;
                    this.player.audio.seek(target * this.player.duration);
                    let s = Math.round(this.player.audio.seek());
                    this.player.progressBar.style.width = (100 * s / this.player.duration ) + '%';
                    this.player.timeField.innerHTML = (s-(s%=60))/60+(9<s?':':':0')+s;
                    this.player.timeField.setAttribute('aria-label',this.player.timeLabel + ': ' + (s-(s%=60))/60 + ' min ' + s + ' s');
                }                    
            });
        }
    }
    loaded(){
        this.player.duration = this.player.audio.duration();
        let s = Math.round(this.player.duration);
        this.player.getElementsByClassName('audio-player__button--loading')[0].style.display = 'none';
        this.player.getElementsByClassName('audio-player__button--play')[0].style.display = 'flex';
        this.player.getElementsByClassName('audio-player__duration')[0].innerHTML = (s-(s%=60))/60+(9<s?':':':0')+s;
        s = Math.round(this.player.duration);
        this.player.durationField.setAttribute('aria-label',this.player.durationLabel + ': ' + (s-(s%=60))/60 + ' min ' + s + ' s');
    }
    play(){
        this.player.audio.play();
        this.player.getElementsByClassName('audio-player__button--pause')[0].style.display = 'flex';
        this.player.getElementsByClassName('audio-player__button--play')[0].style.display = 'none';
        this.player.button.setAttribute('aria-label',this.player.button.dataset.pause);
    }
    pause(){
        this.player.audio.pause();
        this.player.getElementsByClassName('audio-player__button--pause')[0].style.display = 'none';
        this.player.getElementsByClassName('audio-player__button--play')[0].style.display = 'flex';
        this.player.button.setAttribute('aria-label',this.player.button.dataset.play);
    }
    stop(){
        this.player.audio.stop();
        this.player.getElementsByClassName('audio-player__button--pause')[0].style.display = 'none';
        this.player.getElementsByClassName('audio-player__button--play')[0].style.display = 'flex';
        this.player.button.setAttribute('aria-label',this.player.button.dataset.play);
        this.player.progressBar.style.width = 0;
    }
}