export * as videoModule from './video/index.js';
export * as LazyLoadingModule from './lazy-loading/index.js';
export * as SPAModule from './spa/index.js';
export * as AccessibilityModule from './accessibility/index.js';