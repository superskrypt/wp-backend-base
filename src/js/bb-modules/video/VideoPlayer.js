import Plyr from "plyr";

const playerDefaultOptions = {
    ratio: "16:9",
    autopause: true,
    'debug': false,
    controls: [
        'play-large',
        'play',
        'progress',
        'current-time', 
        'mute', 
        'volume', 
        'captions', 
        'settings', 
        'pip', 
        'airplay', 
        'fullscreen'
    ]
    }
export class VideoPlayer {

    constructor(playerEl, playerCover, playerActivator, options = {} ) {

        this.playerEL = playerEl;
        this.playerCover = playerCover;
        this.playerActivator = playerActivator;

        this.options = Object.assign({},playerDefaultOptions, options )
        this.player = new Plyr(this.playerEL, this.options);
        this.setupEventsListeners();
    }

    

    setupEventsListeners() {
        
        this.player.on('ended', (e) => {
            this.hideCover()
        } );

        this.player.on('ended', (e) => {
            this.showCover();
        });

        this.player.on('play', (e) => {
            this.hideCover();
        });

        if(this.playerActivator) {
            this.playerActivator.addEventListener("click", this.onActivatorClickHandler.bind(this));
        }
        if(this.player.autoplay) {
            this.player.muted = true;
        }
    }

    onActivatorClickHandler() {
        this.player.play();
    }

    pausePlayer() {
        this.player.pause();
        this.showCover()
    }

    hideCover() {
    
        this.playerCover && this.playerCover.classList.add('hidden');
        this.playerActivator && this.playerActivator.classList.add('hidden');
    }
    showCover() {
        this.playerCover && this.playerCover.classList.remove('hidden');
        this.playerActivator && this.playerActivator.classList.remove('hidden');
    }
}

export default VideoPlayer;