
import VideoPlayer from "./VideoPlayer.js";

export class VideoController {
    
    constructor() {
        window.addEventListener('spa:mount', this.mount.bind(this) );
        window.addEventListener('spa:unmount', this.unmount.bind(this) );
        this.init();
    }

    init() {
        this.setupPlayers();
    }

    setupPlayers(playerOptions = {}) {
        this.playersInstances = [];
        this.playersWrappers = document.querySelectorAll('.block .player-wrapper');

        this.playersWrappers.forEach((playerWrapper) => {
            const playerCover = playerWrapper.querySelector(' .player__cover');
            const playerActivator = playerWrapper.querySelector(' .player__play-btn');
            const playerEl =  playerWrapper.querySelector('.player');
            this.playersInstances.push( new VideoPlayer( playerEl, playerCover, playerActivator, playerOptions));
        });
        this.autoPauseSetup();
    }

    autoPauseSetup() {
        this.playersInstances.forEach((playerInstance, instanceIndex) => {
            playerInstance.player.on('play', () => {
                this.playersInstances.map((instance, idx) => {
                    if(idx !== instanceIndex ) {
                        instance.pausePlayer();
                    }
                    
                })
            })
        })
    }

    // TESTOWA FUNKCJONALNOSĆ...
    setupHeroPlayer (playerOptions = {}) {
        const heroPlayerWrapper = document.querySelector('.hero__video .player-wrapper');
        if(heroPlayerWrapper) {
            const heroPlayerCover = heroPlayerWrapper.querySelector(' .player__cover');
            const heroPlayerActivator = heroPlayerWrapper.querySelector(' .player__play-btn');
            const heroPlayerEl =  heroPlayerWrapper.querySelector('.player');
            heroPlayerActivator.style.display = 'none'
            const defaultOptions = {
                autopause: false,
                autoplay: true,
                muted: true,
                controls: false,
                loop: { active: true},
                clickToPlay: false,
                youtube: {rel: 0, showinfo: 0, iv_load_policy: 3, modestbranding: 1}
            }
            const options =  Object.assign({}, defaultOptions, playerOptions );
            const heroPlayer = new VideoPlayer(heroPlayerEl, heroPlayerCover, null, options);
        }

    }
    mount () {
        this.init();
    }
    unmount() {
        this.observer.disconnect();
    }

}

export default VideoController;
