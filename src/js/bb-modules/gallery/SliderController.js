export default class SliderController {

    static createInstances(target = document, selector = '.slider-component') {
        const sliderComponents = target.querySelectorAll(selector);
        if(!Array.isArray(document.sliderControllers)){
            document.sliderControllers = [];
        }
        sliderComponents.forEach(sliderComponent => {
            document.sliderControllers.push(new SliderController(sliderComponent));
        });
    }
    static destroyInstances(target = document, selector = '.slider-component') {
        const sliderComponents = target.querySelectorAll(selector);
        sliderComponents.forEach(sliderComponent => {
            new SliderController(sliderComponent);
        });
    }

    constructor(sliderComponent) {
        this.init(sliderComponent);
    }

    init(sliderComponent) {
        this.swipeThresholdInPercent = 10;
        this.currentSlideIndex = 0;
        this.prevBtn = sliderComponent.querySelector('.block-slider__navigation__btn-left');
        this.nextBtn = sliderComponent.querySelector('.block-slider__navigation__btn-right');
        this.slides = sliderComponent.querySelectorAll('.block-slider__slide');
        this.initEvents(sliderComponent);
        this.setNavigationButtonsVisibility();
        sliderComponent.classList.add('ready');
        sliderComponent.removeAttribute('style');
        sliderComponent.controller = this;
    }

    initEvents(sliderComponent) {
        this.nextBtn.addEventListener('click', this.nextSlide.bind(this), false);
        this.prevBtn.addEventListener('click', this.prevSlide.bind(this), false);
        const slidesContainer = sliderComponent.querySelector('.block-slider__slides');
        slidesContainer.addEventListener('touchstart',this.startSwipe.bind(this));
        slidesContainer.addEventListener('touchmove',this.moveSwipe.bind(this));
    }

    getSlideByIndex(slideIndex) {
        return slideIndex >= 0 && slideIndex < this.slides.length ? this.slides[slideIndex] : null;
    }

    nextSlide() {
        if(this.currentSlideIndex  >= this.slides.length - 1 || this.navigationLocked) {
            return;
        }
        this.cleanCurrentSlide();
        this.disableNavigation();
        this.currentSlideIndex = this.currentSlideIndex + 1;
        const newCurrentSlide = this.getSlideByIndex(this.currentSlideIndex);
        const newPrevSlide = this.getSlideByIndex(this.currentSlideIndex - 1);
        newCurrentSlide?.classList.remove('next');
        newPrevSlide?.classList.add('prev');
        this.setNavigationButtonsVisibility();
        this.setFocusToCurrentSlide(newCurrentSlide);
        this.startSlide(newCurrentSlide);
    }

    prevSlide() {
        if(this.currentSlideIndex <= 0 || this.navigationLocked) {
            return;
        }
        this.cleanCurrentSlide();
        this.disableNavigation();
        this.currentSlideIndex = this.currentSlideIndex - 1;
        const newCurrentSlide = this.getSlideByIndex(this.currentSlideIndex);
        const newNextSlide = this.getSlideByIndex(this.currentSlideIndex + 1);
        newCurrentSlide?.classList.remove('prev');
        newNextSlide?.classList.add('next');
        this.setNavigationButtonsVisibility();
        this.setFocusToCurrentSlide(newCurrentSlide);
        this.startSlide(newCurrentSlide);
    }
    
    goToSlide(targetSlideIndex) {
        if(targetSlideIndex < 0) {
            targetSlideIndex = 0;
        }
        if(targetSlideIndex > this.slides.length - 1) {
            targetSlideIndex > this.slides.length - 1;
        }
        this.cleanCurrentSlide();
        this.disableNavigation();
        this.currentSlideIndex = targetSlideIndex;
        const newCurrentSlide = this.getSlideByIndex(this.currentSlideIndex);
        for(let i in this.slides){
            if(typeof this.slides[i] == 'object'){
            this.slides[i].style.transition = 'none';
                if(i < this.currentSlideIndex){
                    this.slides[i].classList.remove('next');
                    this.slides[i].classList.add('prev');
                }else if(i > this.currentSlideIndex){
                    this.slides[i].classList.remove('prev');
                    this.slides[i].classList.add('next');
                }else{
                    this.slides[i].classList.remove('prev');
                    this.slides[i].classList.remove('next');
                }
                setTimeout(() => this.slides[i].style.transition = '',10);
            }
        }
        this.setNavigationButtonsVisibility();
        this.setFocusToCurrentSlide(newCurrentSlide);
        this.startSlide(newCurrentSlide);
    }

    cleanCurrentSlide(){
        const audio = this.getSlideByIndex(this.currentSlideIndex).querySelector('.audio-player');
        if(audio){
            audio.controller.stop();
        }
        const video = this.getSlideByIndex(this.currentSlideIndex).querySelector('.video-player');
        if(video){
            video.controller.stop();
        }
    }

    startSlide(currentSlide){
        const video = currentSlide.querySelector('.video-player');
        if(video){
            video.controller.play();
        }
    }

    setNavigationButtonsVisibility() {
        this.currentSlideIndex <= 0 ? this.prevBtn.classList.remove('visible') : this.prevBtn.classList.add('visible');
        this.currentSlideIndex >= this.slides.length - 1 ? this.nextBtn.classList.remove('visible') : this.nextBtn.classList.add('visible');
    }
    
    setFocusToCurrentSlide(currentSlide) {
        setTimeout(() => { 
            currentSlide?.focus(); 
        }, 250);
    }

    disableNavigation() {
        this.navigationLocked = true;
        setTimeout(() => {
            this.navigationLocked = false
        }, 650);
    }

    startSwipe(e) {
        this.isSwiping = true;
        this.swipeInitialX = e.touches[0].clientX;
        if(!this.currentSlide) {
            this.currentSlide = this.slides.length > 0 ? this.slides[this.currentSlideIndex] : null;
        }
        this.swiperCanvasWidth = this.currentSlide?.getBoundingClientRect().width;
    }

    moveSwipe(e) {
        const touchX = e.changedTouches[0].clientX;
        const swipeDistanceXPercent = this.swiperCanvasWidth ? Math.abs(touchX - this.swipeInitialX) / this.swiperCanvasWidth * 100 : 0;
        if(swipeDistanceXPercent > this.swipeThresholdInPercent&& this.isSwiping) {
            if(this.swipeInitialX - touchX > 0) {
                this.nextSlide();
            }
            else {
                this.prevSlide();
            }
            this.isSwiping = false;
        }
    }
}