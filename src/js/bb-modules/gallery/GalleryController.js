import { clearFocusTrap, focusTrap } from 'bb-modules/accessibility';
import SliderController from 'bb-modules/gallery/SliderController';
import AudioPlayerController from 'bb-modules/audio/AudioPlayerController';
// import VideoPlayerController from '../controllers/VideoPlayerController';

export default class GalleryController {
    static firstInit(){
        this.createInstances();
        window.addEventListener('spa:mount', this.mount.bind(this) );
        window.addEventListener('spa:unmount', this.unmount.bind(this) );
    }

    static mount(){
        this.createInstances();
    }
    static unmount(){
        this.destroyInstances();
    }

    static createInstances() {
        const popupTemplates = document.querySelectorAll('.popup-component');
        popupTemplates.forEach(popupTemplate => {
            // new PopupController(popupTemplate);
            new GalleryController(popupTemplate);
        });
    }
    static destroyInstances() {
        const popups = document.querySelectorAll('body>.popup-component');
        for(let i=popups.length;i>=0;i--){
            if(popups[i] != undefined){
                popups[i].controller = undefined;
                popups[i].remove();
            }
        }
    }

    constructor(popupTemplate) {
        this.popupTemplate = popupTemplate;
        this.popupTemplate.controller = this;
        this.body = document.querySelector('body');
        this.init();
    }

    init() {
        this.callers = document.querySelectorAll('.popup-component-caller[data-target='+this.popupTemplate.getAttribute('id')+']');
        this.callers.forEach(popupCaller => {
            this.initCaller(popupCaller);
        });
    }

    initCaller(popupCaller) {
        popupCaller.addEventListener('click',this.openPopup.bind(this),true);
    }

    initializePopup(){
        this.popupComponent = this.popupTemplate.cloneNode(true);
        this.body.appendChild(this.popupComponent);

        SliderController.createInstances(this.popupComponent);
        //FIXME PRZYWRCIĆ OBSŁUGĘ AUDIO I VIDEO
        AudioPlayerController.createInstances(this.popupComponent);
        // VideoPlayerController.createInstances(this.popupComponent);


        this.audios = this.popupComponent.querySelectorAll('.audio-player');
        this.videos = this.popupComponent.querySelectorAll('.video-player');
        this.closer = this.popupComponent.querySelector('.popup-component-closer');
        this.closer.addEventListener('click',this.closePopup.bind(this));
        //
        this.slider = this.popupComponent.querySelector('.slider-component');
        if(this.slider){
            this.sliderController = this.slider.controller;
        }
    }

    openPopup(e){
        e.preventDefault();
        this.initializePopup();
        if(this.sliderController && e.target.dataset['index']){
            this.sliderController.goToSlide(e.target.dataset['index'] * 1);
        }
        this.popupComponent.classList.add('popup--open');
        this.body.classList.add('scroll-locked');
        setTimeout(() => {
            focusTrap(this.popupComponent);
            if(this.sliderController && e.target.dataset['index']){
                this.sliderController.getSlideByIndex(e.target.dataset['index'] * 1).focus();
            }
        },500);
    }
    closePopup(e){
        e.preventDefault();
        if(this.audios.length){
            this.audios.forEach(audio => audio.controller.stop());
        }
        if(this.videos.length){
            this.videos.forEach(video => video.controller.stop());
        }
        this.popupComponent.classList.remove('popup--open');
        this.body.classList.remove('scroll-locked');
        clearFocusTrap(this.popupComponent);
        this.callers[this.sliderController.currentSlideIndex].focus();
        const popups = document.querySelectorAll('body>.popup-component');
        for(let i=popups.length;i>=0;i--){
            if(popups[i] != undefined){
                popups[i].remove();
            }
        }
    }
}