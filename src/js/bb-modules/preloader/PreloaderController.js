export default class PreloaderController {

    constructor(options) {
        this.options = {
            allowSkip: false,
            cutLoop: true,
            minLoops: 1,
            introDuration: 0.5,
            loopDuration: 0.5,
            outroDuration: 0.5,
        }
        if(typeof options == 'object'){
            for(let i in this.options){
                if(options[i] != undefined){
                    this.options[i] = options[i];
                }
            }
        }
        this.loopCount = 0;
        this.loaded = false;
        this.state = 'unstarted';
        this.startLoopEvent = new Event("preloader:startloop");
        this.finishedEvent = new Event("preloader:finished");
        window.preloader = this;

        this.startLoop = this.startLoop.bind(this);
        this.keepLoop = this.keepLoop.bind(this);
        this.endOutro = this.endOutro.bind(this);

        this.body = document.querySelector('body');
        this.init();
    }

    init() {
        this.setAnimatedElements();
        if(this.options.allowSkip && document.readyState == 'complete'){
            this.endLoop();
        }else{
            window.addEventListener('load',this.onLoaded.bind(this));
            this.startIntro();
        }
    }
    onLoaded(){
        this.loaded = true;
        console.log('loaded');
        console.log(this.options.cutLoop);
        console.log(this.state);
        console.log(this.loopCount);
        console.log(this.options.minLoops);
        if(this.options.cutLoop && this.state=='loop' && this.loopCount >= this.options.minLoops){
            console.log("going to end loop");
            this.endLoop();
        }
    }
    setAnimatedElements(){
        this.preloaderElement = document.querySelector('.preloader');
        this.preloaderCurtain = document.querySelector('.preloader__curtain');
        this.preloaderAnimation = document.querySelector('.preloader__animation');
    }
    startIntro(){
        this.state = 'intro';
        // setTimeout(this.startLoop.bind(this),this.options.introDuration * 1000);
        this.startIntroAnimation();
        this.preloaderAnimation.addEventListener("animationend", this.startLoop);
    }
    startIntroAnimation(){
        this.preloaderAnimation.classList.add('preloader__animation--intro');
        this.body.classList.remove('scroll-unlocked');
    }
    startLoop(){
        console.log("starting loop");
        this.state = 'loop';
        this.preloaderAnimation.removeEventListener("animationend", this.startLoop);
        // this.loopInterval = setInterval(this.keepLoop.bind(this),this.options.loopDuration * 1000);
        this.startLoopAnimation();
        console.log("animation iteration count");
        let animationStyle = getComputedStyle(this.preloaderAnimation);
        console.log(animationStyle.animationIterationCount);
        this.preloaderAnimation.addEventListener("animationiteration", this.keepLoop);
        window.dispatchEvent(this.startLoopEvent);
    }
    startLoopAnimation(){
        console.log("starting loop animation");
        this.preloaderAnimation.classList.remove('preloader__animation--intro');
        this.preloaderAnimation.classList.add('preloader__animation--loop');
        this.preloaderCurtain.classList.remove('preloader__curtain--intro');
        this.preloaderCurtain.classList.add('preloader__curtain--loop');
    }
    keepLoop(){
        console.log("keeping loop");
        this.loopCount++;
        console.log(this.loopCount);
        console.log(this.options.minLoops);
        if(this.loaded && this.loopCount >= this.options.minLoops){
            this.endLoop();
        }
    }
    endLoop(){
        console.log("ending loop");
        // if(this.loopInterval){
        //     clearInterval(this.loopInterval);
        // }
        this.preloaderAnimation.removeEventListener("animationiteration", this.keepLoop);
        setTimeout(() => {
            this.endLoopEvent = new Event("preloader:endloop", { cancelable: true });
            if(window.dispatchEvent(this.endLoopEvent)){
                this.startOutro();
            }
        });
    }
    startOutro(){
        console.log("starting outro");
        this.state = 'outro';
        // setTimeout(this.endOutro.bind(this),this.options.outroDuration * 1000);
        this.startOutroAnimation();
        this.preloaderAnimation.addEventListener("animationend", this.endOutro);
    }
    startOutroAnimation(){
        this.preloaderAnimation.classList.remove('preloader__animation--loop');
        this.preloaderAnimation.classList.add('preloader__animation--outro');
        this.preloaderCurtain.classList.remove('preloader__curtain--loop');
        this.preloaderCurtain.classList.add('preloader__curtain--outro');
    }
    endOutro(){
        console.log("ending outro");
        this.state = 'finished';
        this.preloaderAnimation.removeEventListener("animationend", this.endOutro);
        window.dispatchEvent(this.finishedEvent);
        this.endOutroAnimation();
    }
    endOutroAnimation(){
        this.preloaderElement.classList.add('preloader--finished');
        this.body.classList.add('scroll-unlocked');
    }
    restart(){
        this.loaded = false;
        this.loopCount = 0;
        this.preloaderAnimation.classList.remove('preloader__animation--outro');
        this.preloaderCurtain.classList.remove('preloader__curtain--outro');
        this.preloaderCurtain.classList.add('preloader__curtain--off');
        this.preloaderElement.classList.remove('preloader--finished');
        setTimeout(() => {
            this.preloaderCurtain.classList.remove('preloader__curtain--off');
            this.preloaderCurtain.classList.add('preloader__curtain--intro');
            this.preloaderAnimation.classList.add('preloader__animation--intro');
            this.startIntro();
        });
    }
}