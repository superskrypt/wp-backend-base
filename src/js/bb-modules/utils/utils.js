export const throttle = (func, interval) => {
    let execute = true;
    return function() {
            if (execute) {
                    func();
                    execute = false;
                    setTimeout(() => {
                        execute = true;
                    }, interval)
                }
            }
}

export const randomInRange = (minimum, maximum ) => {
    return Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
}