import { toggleAriaState } from '../accessibility';

export default class AutoCardsFiltersController {
    constructor() {
        window.addEventListener('spa:mount', this.mount.bind(this) );
        window.addEventListener('spa:unmount', this.unmount.bind(this) );
        this.init();
    }

    init() {
        this.onFilterEvent = new Event('cards:filter');
        const blocks = document.querySelectorAll('.block');
        [...blocks].forEach(block => {
            const filtersContainer = block.querySelector('.filters');

            if(filtersContainer) {
                block.classList.add('filterable');
                const items = block.querySelectorAll('.filter-item');
                console.log(items);
                const filtersBtns = filtersContainer.querySelectorAll('.filters__btn');
                const blockFiltersState = new Map();
                [...filtersBtns].forEach(filterBtn => {
                    filterBtn.addEventListener('click', this.doFilter.bind(this, filterBtn, items, blockFiltersState));
                });
            }
        });
    }

    /**
     * 
     * @param {HTMLButtonElement} filterBtn 
     * @param {NodeList} items 
     * @param {Map<string, Number[]>} filterState 
     */
    doFilter(filterBtn, items, filterState) {
        toggleAriaState(filterBtn, 'aria-pressed');
        const filterId = filterBtn.dataset.filterId;
        const filterTermSlug = filterBtn.dataset.filterTaxonomy;
        const filterTermState = filterState.has(filterTermSlug) ? filterState.get(filterTermSlug) : [];
        const filterIsActive = filterTermState.includes(filterId);
        if(filterIsActive ) {
            filterBtn.classList.remove('active');
            filterTermState.splice(filterTermState.indexOf(filterId),1);
            if(filterTermState.length === 0) {
                filterState.delete(filterTermSlug);
            }
        }
        else {
            filterBtn.classList.add('active');
            filterTermState.push(filterId)
        }
        // Zaktualizuj wewnętrzny stan filtrów
        filterState.set(filterTermSlug, filterTermState);

        [...items].map(item => {
            let itemShouldBeShown = false;
            filterState.forEach((ids, filterType, map) => {
                const itemsTerms = item.getAttribute(`data-terms-${filterType}`);
                const termsIds = itemsTerms ? itemsTerms.split(',').map(el => el.trim()) : [];
                itemShouldBeShown = itemShouldBeShown ? itemShouldBeShown : termsIds.some(el => ids.includes(el));
            });
            
            if(this.checkIfFilterStateIsEmpty(filterState)) {
                //reset
                item.classList.remove('hidden')
            }
            else if(itemShouldBeShown) {
                item.classList.remove('hidden')
            }
            else {
                item.classList.add('hidden')
            }
        });
        document.dispatchEvent(this.onFilterEvent);
    }

    checkIfFilterStateIsEmpty(filterState) {
        return ![...filterState.values()].some(e => e.length > 0)
    }

    mount() {
        this.init();
    }

    unmount() {
        /* DO IMPLEMENTACJI */
    }
}