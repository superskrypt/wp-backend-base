/**
 * @typedef { {dataSrcAttr: String, targets: Array, threshold: Number } } LazyLoadSettingsType
 */


const defaultSettings = {
    dataSrcAttr: "data-lazy-loading-src",
    targets: [],
    threshold: 0, // wartości 0 -> 1 określające jaka część elementu jest w widoku (0.5 - 50% jest w widoku)
}
const IMAGE_CONTAINER_LOADING_CLASS = 'lazy-img-container--loading';
const BACKGROUND_IMAGE_CLASS = 'lazy-img--background';
export default class ImageLazyLoadingController {

    /**
     * @param {(LazyLoadSettingsType|{})} settings Główne ustawienia mechanizmu lazy loading. Pozwala na nadpisanie domyślnych ustawień
     */
    constructor(settings = {}) {
        
        this.settings = Object.assign({}, defaultSettings , settings);
        if(this.settings.targets.length === 0) {
            this.settings.targets.push(`[${this.settings.dataSrcAttr}]`)
        }
        else {
            this.settings.targets = this.settings.targets.join(',');
        }
        window.addEventListener('spa:mount', this.mount.bind(this) );
        window.addEventListener('spa:unmount', this.unmount.bind(this) );
        this.init();
    }

    init() {
        this.intersectionElement = document.createElement('div');
        this.intersectionElement.classList.add('lazy-loading-intersection');
        document.getElementsByTagName('body')[0].append(this.intersectionElement);
        let options = {
            root: null,
            rootMargin: "0px",
            threshold:  this.settings.threshold, 
        };
        
        this.observer = new IntersectionObserver(this.observerCallback.bind(this), options);
        let targets = document.querySelectorAll(this.settings.targets);

        targets = [...targets].map((target) => {
            return this.checkIfIsImageInSvg(target) ? this.getParentForSvgImage(target)  : target;
        }).filter(Boolean);

        targets.forEach(target => {
            target.classList.add('lazy-img');
            if(target.nodeName !== 'IMG') {
                target.classList.add(BACKGROUND_IMAGE_CLASS);
            }
            else {
                const container = target.parentElement;
                container.classList.add(IMAGE_CONTAINER_LOADING_CLASS);
            }
            this.observer.observe(target);
        });
    }

    /**
     * 
     * @param {IntersectionObserverEntry[]} entries 
     * @param {IntersectionObserver} observer
     */
    observerCallback (entries, observer) {
        entries.forEach((entry) => {
            if (entry.isIntersecting) {
                this.inViewHandler(entry, observer )
            }
            else {
                this.notInViewHandler(entry, observer);
            }
        });
    }

    /**
     * 
     * @param {IntersectionObserver} observer Instancja Intersection Observer
     * @param {IntersectionObserverEntry} entry 
     */
    inViewHandler(entry, observer ) {
        let lazyImage = null;
        if(entry.target.nodeName === 'IMG') {
            lazyImage = entry.target;
            lazyImage.src = entry.target.getAttribute(`${this.settings.dataSrcAttr}`);
        }
        else if(entry.target.nodeName === 'svg') {
            lazyImage = new Image();
            lazyImage.src = entry.target.getAttribute(`${this.settings.dataSrcAttr}`);
            this.handleImageInSvg(entry.target);
        }
        else {
            lazyImage = new Image();
            lazyImage.src = entry.target.getAttribute(`${this.settings.dataSrcAttr}`);
            entry.target.style.backgroundImage = `url(${lazyImage.src})`;
        }
        lazyImage.addEventListener('load', (e)=> {
            entry.target.classList.add('loaded');
            const container = entry.target.closest(`.${IMAGE_CONTAINER_LOADING_CLASS}`);
            container?.classList.remove(IMAGE_CONTAINER_LOADING_CLASS);
        });
        observer.unobserve(entry.target);
    }

    notInViewHandler(item, observer ) {
        /* DO IMPLEMENTACJI */
    }

    checkIfIsImageInSvg (element) {
        return element?.nodeName === 'image';
    }

    /**
     * @param {SVGImageElement} imageEl 
     * @returns {(SVGElement|false)}
     */
    getParentForSvgImage(imageEl) {
        const imageElParent = imageEl.closest('svg');
        if(imageElParent) {
            imageElParent.setAttribute(`${this.settings.dataSrcAttr}`, imageEl.getAttribute(`${this.settings.dataSrcAttr}`));
            return imageElParent;
        }
        return false;

    }
    /**
     * Metoda ustawia niezbędne atrybuty dla taga <image> jeśli element svg, który ustawiony jest jako obserwowany, znajdzie sie w viewporcie.
     * 
     * @param {SVGElement} svgParent 
     * @returns void
     */
    handleImageInSvg(svgParent) {
        let svgImage = svgParent.querySelector('image');
        if(!svgImage) {
            let svgImage = document.createElement('image');
            svgImage.setAttribute('preserveAspectRatio', 'xMidYMid slice' );
        }
        svgImage.setAttribute('href', svgParent.dataset.lazyLoadingSrc);
    }

    mount () {
        this.init();
    }
    unmount() {
        this.observer.disconnect();
    }
}