import { focusTrap, clearFocusTrap, toggleExpandedElementState }  from 'bb-modules/accessibility';
import { throttle } from 'bb-modules/utils/utils';

export default class MenuController {
    constructor() {
        this.init();
    }

    init() {
        this.body = document.querySelector('body');
        this.burger = document.querySelector('.menu-toggle_button');
        this.siteMenu = document.querySelector('.site-menu');
        this.burgerOpenedStateLabel = this.burger.dataset.openStateLabel;
        this.burgerClosedStateLabel = this.burger.dataset.closeStateLabel;
        this.header = document.querySelector('.site-header__inner');
        if(!this.burger ) {
            return;
        }
        this.burger.addEventListener('click', this.handleBurgerClick.bind(this));
        
        const mobileBreakpoint = 900;
        const resizeHandler = throttle(()=> {
            // Usuń klasy menu popupu na desktopie
            const isMobile = window.matchMedia(`(max-width: ${mobileBreakpoint}px)`).matches;
            if(!isMobile && this.body.classList.contains('menu-opened')) {
                this.body.classList.remove('menu-opened');
            }
        }, 500)
        window.addEventListener('resize', resizeHandler );
        window.addEventListener('spa:close-menu', this.close.bind(this) );
    }

    handleBurgerClick(e) {
        toggleExpandedElementState(this.burger,this.burgerClosedStateLabel, this.burgerOpenedStateLabel);
        if(this.body.classList.contains('menu-opened')) {
            this.close();
        }
        else {
            this.open();
        }
    }
    open() {
        if(!this.body.classList.contains('menu-opened')){
            this.body.classList.add('menu-opened', 'scroll-locked');
            focusTrap(this.siteMenu, this.burger, null, this.burger);
        }
    }
    close() {
        if(this.body.classList.contains('menu-opened')){
            this.body.classList.remove('menu-opened', 'scroll-locked');
            clearFocusTrap(this.siteMenu);
        }
    }
}