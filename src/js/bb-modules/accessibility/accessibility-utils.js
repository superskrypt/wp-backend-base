
/**
 * @desc Umożliwia zapętlone nawigowanie przy użyciu klawisza TAB w obrębie określonego kontenera.
 * @param {HTMLElement} expandableElement Kontener elementów pomiędzy, którymi ma przebiegać nawigacja np.: popup z elementami menu.
 * @param {HTMLElement} customFirstElement Optional. Default null. Opcjonalny element HTML który powinien być traktowany jako pierwszy w kolejności nawigacji klawiszem TAB. Zazwyczaj element ten jest w DOM poza elementem "expandableElement". Może to byc np. guzik włączający okno modalne.
 * @param {HTMLElement} customLastElement Optional. Default null. Opcjonalny element HTML który powinien być traktowany jako ostatni w kolejności nawigacji klawiszem TAB. Zazwyczaj element ten jest w DOM poza elementem "expandableElement". Może to byc np. guzik włączający okno modalne albo jakiś element headera.
 * @param {HTMLElement} switcherEl Optional.Opcjonalny element na którym powinien byc ustawiony focus po uruchomieniu mechanizmu trap. Parametr jest przydatny gdy np. guzik włączający okno modalne znajduje się w środku elementów, które objęte są trapem.
 * @return void
 */
export const focusTrap = (expandableElement, customFirstElement = null,  customLastElement = null , switcherEl = null,) => {
    const bodyChildren = document.querySelectorAll('body > *:not(script):not(style)');
    const focusableElements = `div[role=group], button:not([disabled]), a:not([disabled]), textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="radio"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled]) `;
    const focusableHtmlEls = [...expandableElement.querySelectorAll(focusableElements)];
    focusableHtmlEls.forEach(el => el.setAttribute('tabindex', 0));
    const firstFocusableEl = customFirstElement ? customFirstElement : focusableHtmlEls[0];
    const lastFocusableEl = customLastElement ? customLastElement : focusableHtmlEls[focusableHtmlEls.length - 1];
    if(switcherEl) {
        switcherEl.setAttribute('tabindex', 0);
        switcherEl.focus();
    }
    else {
        firstFocusableEl.setAttribute('tabindex', 0);
        firstFocusableEl.focus();
    }

    window.trappedEls = customFirstElement ? [...focusableHtmlEls, customFirstElement] : focusableHtmlEls ;
    const onKeydownCallback = (e) => { 
        focusTrapKeyEventHandler(e, firstFocusableEl, lastFocusableEl ); 
    }
    window.onKeydownCallback = onKeydownCallback;
    document.addEventListener('keydown', onKeydownCallback  );

    bodyChildren.forEach((node) => {
        if(!node.contains(expandableElement)){
            node.setAttribute('aria-hidden', true);
        }
    });
}

/**
 * @desc Funkcja do wykorzystania razem z funkcją "focusTrap" – usuwa atrybuty nadane podczas działania "TrapFocus"
 * @return void
 */
export const clearFocusTrap = () => {
    const bodyChildren = document.querySelectorAll('body > *:not(script)');
    if(window.trappedEls) {
        window.trappedEls.forEach(node => node.removeAttribute('tabindex'));
    }
    document.removeEventListener('keydown', onKeydownCallback);
    bodyChildren.forEach(node => node.removeAttribute('aria-hidden'));
    delete window.focusTrapCallback;
}
/**
 * @param {KeyboardEvent}} e 
 * @param {HTMLElement} firstFocusableEl pierwszy element w kolejności nawigacji za pomocą klawisza TAB
 * @param {HTMLElement} lastFocusableEl ostatni element w kolejności nawigacji za pomocą klawisza TAB
 * @returns void
 */
export const focusTrapKeyEventHandler = (e, firstFocusableEl, lastFocusableEl ) => {   
    const KEYCODE_TAB = 9;
    const isTabPressed = (e.key === 'Tab' || e.keyCode === KEYCODE_TAB);
    if (!isTabPressed) { 
        return; 
    }

    if ( e.shiftKey ) {
        if (document.activeElement === firstFocusableEl) {
            lastFocusableEl.focus();
            e.preventDefault();
        }
    } else  {
        if (document.activeElement === lastFocusableEl) {
            firstFocusableEl.focus();
            e.preventDefault();
        }
    }
}

/**
 * 
 * @param {HTMLButton} htmlEl 
 * @param {string} closeStateLabel
 * @param {string} openStateLabel 
 */
export const toggleExpandedElementState = (htmlButton, closeStateLabel, openStateLabel) => {
    const currentState = htmlButton.getAttribute('aria-expanded');
    if(currentState == 'false') {
        htmlButton.setAttribute('aria-expanded', true);
        htmlButton.setAttribute('aria-label', closeStateLabel );
    }
    else {
        htmlButton.setAttribute('aria-expanded', false);
        htmlButton.setAttribute('aria-label', openStateLabel);
    }
}

/**
 * Pozwala na przełączenie stanów aria gdy atrybut aria przyjmuje wartości true|false
 * @param {HTMLButton} htmlEl 
 * @returns void
 */
export const toggleAriaState = (htmlButton, ariaState) => {
    const currentState = htmlButton.getAttribute(ariaState);
    currentState === 'true' ? htmlButton.setAttribute(ariaState, false) : htmlButton.setAttribute(ariaState, true);
}

/**
 * Funkcja pomocnicza dla wymuszania wypowiedzenia komunikatu przez czytnik ekranowy w elemencie aria-live
 * @param {String} text 
 * @returns {String} tekst komunikatu wraz z twardymi spacjami
 */
const insertSpacesBefore =  (text) => {
    let spaces = [];
    const spacesCount = Math.floor(Math.random() * 10);
    for(let x = 0; x < spacesCount; x++) {
        spaces.push('&nbsp;');
    }
    return `${spaces.join(" ")} ${text}`;         
}

export const addAriaLiveMassage = (container, text, delay = 500) => {
    container.innerHTML = " ";
    setTimeout(() => { 
        container.innerHTML = `${insertSpacesBefore(text)}` ;
    }, delay );
}


