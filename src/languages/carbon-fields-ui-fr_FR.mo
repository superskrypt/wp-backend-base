��          �       �       �      �   7   �      ,  '   9  
   a  .   l  	   �     �     �     �     �     �  #  �        V   /     �  ,   �     �  -   �     �               %     ;     Z   Add %s An error occurred while trying to fetch oembed preview. Collapse All Couldn't create the label of group - %s Expand All Minimum number of rows not reached (%1$d %2$s) Not Found Select Date Select Image Select Time There are no entries yet. This field is required. Project-Id-Version: 
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 
PO-Revision-Date: 
Language: fr_FR
X-Generator: Poedit 2.4.1
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajouter une %s Une erreur est survenue lors de la tentative de récupération de l'aperçu du oembed. Tout replier Impossible de créer le label du groupe - %s Tout déplier Le nombre minimum de lignes n'est pas atteint Introuvable Sélectionner une date Image Sélectionnez l'heure Il n'y a pas encore d'entrée. Ce champ est requis. 