��    6      �      |      |      }     �     �     �     �  5   �  <   !  3   ^  7   �  ,   �     �        0     '   >  	   f     p     |  
   �  1   �     �  5   �       *     .   F  .   u     �     �  	   �     �  )   �       	             (     4     @     M     Y     h     u     �  )   �     �  =   �          9  )   Q  B   {  7   �     �     	  	   	     	  E   	     f
  #   �
     �
     �
     �
  ;   �
  ?     9   ^  =   �  %   �     �       1     1   C     u     �      �     �  6   �       &        4  ?   C  A   �  6   �     �          !     0  1   @     r  	   x     �     �     �     �  
   �     �     �     �       (        5  ?   L     �     �  )   �  J   �  <   0     m  
   �     �     �   %1$s %2$s is already registered. %1$s type must be a string. %s %s isn't registered. Add %s An error occured. An error occurred while trying to create the sidebar. An error occurred while trying to fetch association options. An error occurred while trying to fetch files data. An error occurred while trying to fetch oembed preview. An unknown field is used in condition - "%s" Collapse Collapse All Could not find DOM element for container "%1$s". Couldn't create the label of group - %s Duplicate Error alert Error loading block: %s Expand All Field of type '%s' is not supported in Gutenberg. Fields Geocode was not successful for the following reason:  Hide preview Maximum number of items reached (%s items) Minimum number of items not reached (%s items) Minimum number of rows not reached (%1$d %2$s) No options. No results found. Not Found Please choose Please enter the name of the new sidebar: Remove Search... Select Attachments Select Date Select File Select Image Select Time Select a color Show preview Showing %1$d of %2$d results Text The "component" param must be a function. The address could not be found. The provided context isn't a valid one. Must be one of - %s . There are no entries yet. This field is required. Unsupported container condition - "%1$s". Unsupported container condition comparison operator used - "%1$s". Unsupported container condition relation used - "%1$s". Use Attachments Use File Use Image Visual Project-Id-Version: 
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: Jose Villalobos <jorovipe@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 
PO-Revision-Date: 
Language: es
X-Generator: Poedit 2.4.1
Plural-Forms: nplurals=2; plural=(n != 1);
 %1$s %2$s już zarejestrowano Typ %1$s musi być ciągiem znaków %s %s nie jest zarejestrowane Dodaj %s Wystąpił błąd. Wystąpił błąd podczas próby utworzenia paska bocznego. Wystąpił błąd podczas próby pobierania skojarzonych opcji. Wystąpił błąd podczas próby pobrania danych plików. Wystąpił błąd w trakcie próby pobrania podglądu oembed. Warunek używa nieznanego pola - "%s" Zwiń Zwiń wszystko Nie znaleziono elementu DOM dla pojemnika "%1$s". Nie udało się utworzyć etykiety dla grupy - %s Utwórz duplikat Ostrzeżenie o błędzie Błąd przy ładowaniu bloku: %s Rozwiń wszystko Pole typu '%s' nie jest obsługiwane przez Gutenberga. Pola Geocode się nie udał z tego powodu:  Ukryj podgląd Osiągnięto maksymalną liczbę przedmiotów (%s przedmiotów) Nie osiągnięto minimalnej liczby przedmiotów (%s przedmiotów) Nie osiągnięto minimalnej liczby wierszy (%1$d %2$s) Brak opcji. Nie znaleziono wyników. Nie znaleziono Proszę wybrać Proszę wprowadzić nazwę nowego paska bocznego: Usuń Szukaj... Wybierz załącznik Wybierz datę Wybierz plik Wybierz obrazek Ustaw czas Wybierz kolor Pokaż podgląd Pokazane wyniki %1$d z %2$d Tekst Parametr "component" musi być funkcją. Nie znaleziono adresu. Podany kontekst nie jest prawidłowy. Musi być jednym z - %s . Nie ma jeszcze wpisów. To pole jest wymagane. Nie wspierany warunek pojemnika - "%1$s". Użyto nie wspieranego operatora porównania w warunku pojemnika - "%1$s". Użyto nie wspieranego warunku relacji w pojemniku - "%1$s". Użyj załączniki Użyj plik Użyj obrazek Wygląd 