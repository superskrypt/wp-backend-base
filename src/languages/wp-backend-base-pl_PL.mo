��    c      4      L      L     M  
   e  
   p  	   {     �  
   �     �     �     �      �     �     �     �     �                    '  #   5     Y     f     n  	   u  	        �      �     �  
   �     �  $   �  "     !   1     S     d     u     �     �     �     �     �     �     �  
   �     �     �  #   �     	     +	     =	  !   I	     k	     q	     �	  
   �	     �	     �	     �	  	   �	     �	     �	     �	     �	     
     
     
     "
     ;
     I
     e
     n
     ~
     �
     �
     �
     �
  
   �
     �
     �
     �
        "        (     6  !   B     d     j     v  "   �     �     �     �     �  "   �     �                     (  �  .     �  
   �  
   �     �          '     ;     X     _  ?   e  =   �     �     �     �     �                 0   6     g     v     ~     �     �  9   �  <   �     '     /     <  1   T  '   �  '   �     �     �               3     C     J     W  	   l     v     �     �     �  0   �     �          )  -   =     k     p     ~     �     �     �     �     �     �     �                     @     F     U     j      w     �     �     �  /   �          	     !     4     B  	   U     _     f  5   l     �     �  .   �     �     �     	  /        O     \     k     x  0   �     �     �     �     �     �   Add heading for filters Audio file Auto Cards Big links Button text Button url CTA Caption Cards Click to listen (opens in popup) Click to view (opens in popup) Close Columns Content Credits Description Enable filters Facebook link Facebook link label (accessibility) Four columns Gallery Header Hero type Highlight Home link label (in main menu) Home logo link label (in header) Image Image file Instagram link Instagram link label (accessibility) Label "Close menu" (accessibility) Label "Open menu" (accessibility) Label: Switch to Landscape images Language switcher Language switcher  Languages list Layout Lead Lead content Link Link box color Link label Link url Linkedin link Linkedin link label (accessibility) List of images Listen audio file Medium link Medium link label (accessibility) Menu  Menu in footer Multi Column Text One column Page Section Page Sections Page content Page hero Page sections Pause playback Play Portrait images Quick Links Quote Section Blocks Section background image Section color Section headings text color Sections Show next slide Show previous slide Skip links (accessibility) Slide Social media links Square images Subheading Subheading (h3) Subtitle Tagline Text Text for main skip navigation link Three columns Tiktok link Tiktok link label (accessibility) Title Toggle menu Twitter link Twitter link label (accessibility) Two columns Video id Website options Youtube link Youtube link label (accessibility) hero of social media link social media links title Project-Id-Version: Backend Base Placeholder
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2022-04-15 12:40+0200
PO-Revision-Date: 2022-04-19 12:42+0200
Language: pl
Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ((n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14)) ? 1 : 2);
 Dodaj tytuł dla filtrów Pilk audio Auto-karty Duże odnośniki (big links) Tekst na przycisku Adres URL przycisku Wezwanie do działania (CTA) Podpis Karty Kliknij, żeby odsłuchać (Otwiera się w wyskakującym oknie) Kliknij, żeby obejrzeć (Otwiera się w wyskakującym oknie) Zamknij Kolumny Treść Prawa autorskie Opis Aktywuj filtry Odnośnik do Facebooka Etykieta odnośnika do Facebooka (dostępność) Cztery kolumny Galeria Nagłówek (belka) Typ hero Wyróżniony fragment Etykieta dla linka do strony głównej (w menu głównym) Etykieta dla linka do strony głównej (w nagłówku strony) Obrazek Plik obrazka Odnośnik do Instagrama Etykieta odnośnika do Instagrama (dostępność) Etykieta "Zamknij menu" (dostępność) Etykieta "Otwórz menu" (dostępności) Etykieta: Przełącz na język Poziome obrazki Przełacznik języków Przełacznik języków Lista języków Układ Wprowadzenie Treść wprowadzenia Odnośnik Kolor pola odnośnika Etykieta odnośnika Adres URL odnośnika Odnośnik do Linkedina Etykieta odnośnika do Linkedina (dostępność) Lista obrazków Odsłuchaj plik dźwiękowy Odnośnik do Medium Etykieta odnośnika do Medium (dostępność) Menu Menu w stopce Tekst wielokolumnowy Jedna kolumna Sekcja strony Sekcje strony Treść strony Hero na górze strony Sekcje strony Wstrzymaj odtwarzanie Odtwórz Pionowe obrazki Szybkie odnośniki (quick links) Cytat Bloki w sekcji Obrazek w tle sekcji Kolor sekcji Kolor tekstu w nagłówku sekcji Sekcje Wyświetl następny slajd Wyświetl poprzedni slajd Linki do przeskakiwania treści (dostępność) Slajd Odnośniki social media Kwadratowe obrazki Podnagłówek Podnagłówek (h3) Podtytuł Slogan Tekst Tekst dla głównego linka do porzeskakiwania treści Trzy kolumny Odnośnik do Tiktoka Etykieta odnośnika do Tiktoka (dostępność) Tytuł Przełacz menu Odnośnik do Twittera Etykieta odnośnika do Twittera (dostępność) Dwie kolumny Numer id wideo Opcje strony Odnośnik do Youtube'a Etykieta odnośnika do Youtube'a (dostępność) hero z odnośnik social media odnośniki social media tytuł 