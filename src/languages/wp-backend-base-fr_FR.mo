��    c      4      L      L     M  
   e  
   p  	   {     �  
   �     �     �     �      �     �     �     �     �                    '  #   5     Y     f     n  	   u  	        �      �     �  
   �     �  $   �  "     !   1     S     d     u     �     �     �     �     �     �  
   �     �     �  #   �     		     	     *	  !   6	     X	     ]	     l	  
   ~	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	     
     
     ,
     H
     Q
     ^
     n
     �
     �
     �
     �
  
   �
     �
     �
     �
     �
  "   �
          &  !   2     T     Z     f  "   s     �     �     �     �  "   �     �     �     �            �       �     �     �     �          $     2     D     M  1   T  1   �     �     �     �     �     �     �     �  ,        9     I     Q     Y     f  3   z  ,   �     �     �     �  -     ,   1  ,   ^     �     �     �     �     �     �     �               (     9     E  ,   X     �     �     �  6   �     �     �          '     3     C     T     g          �     �     �     �     �     �     �       +   *     V     _     w      �  .   �  	   �     �       
        "  
   2     =     D  1   J     |     �  /   �     �     �     �  +   �          +     >     R  +   d     �     �     �     �     �   Add heading for filters Audio file Auto Cards Big links Button text Button url CTA Caption Cards Click to listen (opens in popup) Click to view (opens in popup) Close Columns Content Credits Description Enable filters Facebook link Facebook link label (accessibility) Four columns Gallery Header Hero type Highlight Home link label (in main menu) Home logo link label (in header) Image Image file Instagram link Instagram link label (accessibility) Label "Close menu" (accessibility) Label "Open menu" (accessibility) Label: Switch to Landscape images Language switcher Languages list Layout Lead Lead content Link Link box color Link label Link url Linkedin link Linkedin link label (accessibility) List of images Listen audio file Medium link Medium link label (accessibility) Menu Menu in footer Multi Column Text One column Page Section Page Sections Page content Page hero Page sections Pause Play Portrait images Quick Links Quote Section Blocks Section background image Section color Section headings text color Sections Select Image Show next slide Show previous slide Skip links (accessibility) Slide Social media links Square images Subheading Subheading (h3) Subtitle Tagline Text Text for main skip navigation link Three columns Tiktok link Tiktok link label (accessibility) Title Toggle menu Twitter link Twitter link label (accessibility) Two columns Video id Website options Youtube link Youtube link label (accessibility) hero of social media link social media links title Project-Id-Version: Backend Base Placeholder
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2022-04-15 12:40+0200
PO-Revision-Date: 2022-04-19 12:42+0200
Language: fr
Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ((n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14)) ? 1 : 2);
 Ajoutez un titre pour filtres Fichier audio Cartes automatiques Grands liens (big links) Texte sur le bouton URL du bouton Appel à l'action Légende Cartes Cliquez pour écouter (Ouvre une fenêtre pop-up) Cliquez pour regarder (Ouvre une fenêtre pop-up) Fermer Colonnes Contenu Crédits Description Activer les filtres Lien vers Facebook Étiquette de lien Facebook (accessibilité) Quatre colonnes Galerie Entête Type de hero Fragment en vedette Libellé du lien d'accueil (dans le menu principal) Étiquette de lien avec le logo de la maison Image Fichier image Lien vers Instagram Étiquette de lien Instagram (accessibilité) Étiquette "Fermer le menu" (accessibilité) Étiquette "Ouvrir le menu" (accessibilité) Étiquette: Basculer vers Images horizontales Sélecteur de langue liste de langues Mise en page Introduction Introduction Lien Couleur de la boîte de lien Libellé du lien URL du lien Lien vers Linkedin Étiquette de lien LinkedIn (accessibilité) Liste des images Écouter le fichier audio Lien vers Medium Étiquette de référence pour Medium (accessibilité) Menu Menu en pied de page Texte multi-colonnes Une colonne Section du site Sections du site Contenu de la page Hero en haut de la page Sections du site Mettre en pause la lecture Jouer Pmages verticales Liens rapides (quick links) Citation Blocs dans la section Image de fond de section Couleur de section Couleur du texte dans l'en-tête de section Sections Sélectionner une image Afficher l'élément suivant Afficher l'élément précédent Liens pour ignorer le contenu (accessibilité) Élément Liens de médias sociaux Images carrées Sous-titre Sous-titre (h3) Sous-titre Slogan Texte Texte du lien principal pour parcourir le contenu Trois colonnes Lien vers Tiktok Étiquette de lien vers Tiktok (accessibilité) Titre Menu bascule Lien vers Twitter Étiquette de lien Twitter (accessibilité) Deux colonnes Identifiant vidéo Options du site Web Lien vers Youtube Étiquette de lien Youtube (accessibilité) hero de lien de médias sociaux liens de médias sociaux titre 