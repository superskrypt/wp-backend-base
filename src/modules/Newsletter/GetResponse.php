<?php
namespace Superskrypt\WpBackendBase\Modules\Newsletter;

use Superskrypt\WpBackendBase\WpBackendBase as WpBackendBase;

use Carbon_Fields\Field;
class GetResponse {
    public static function init() {
        self::registerEndpoints();
    }

    public static function registerEndpoints() {
        add_action('wp_ajax_addAddress', [__CLASS__, "addAddress"]);
        add_action('wp_ajax_nopriv_addAddress', [__CLASS__, "addAddress"]);
        add_filter('wp_backend_base_newsletter_theme_options', [__CLASS__, "themeOptions"]);
    }
    public static function themeOptions($options){

        $getresponse_options = 
        array(
            Field::make( 'separator', 'newsletter_separator', __('GetResponse Newsletter', Newsletter::$textDomain)),
            Field::make( 'text', 'getresponse_api_key' , __('GetResponse API key', Newsletter::$textDomain)),
            Field::make( 'select', 'getresponse_list_token' , __('GetResponse address list', Newsletter::$textDomain))
            ->add_options(GetResponse::getCampaigns()),
        );
        return array_merge($options,$getresponse_options);

    }
    public static function getCampaigns($current = false) {

        $api_key = get_option('_getresponse_api_key');

        if($api_key){
            $lists_formatted = array( '' => __('Not set',Newsletter::$textDomain) );
        }else{
            $lists_formatted = array( '' => __('Set API key first',Newsletter::$textDomain) );
        }
            
        $args = array(
            'method' => 'GET',
            'timeout'     => 15,
            'redirection' => 15,
            'headers' => array ( 
                "Content-Type" => "application/json",
                "X-Auth-Token" => "api-key $api_key" 
            ),
        );
            
        $url = "https://api.getresponse.com/v3/campaigns";

        $response = wp_remote_request( $url, $args );

        if($response['response']['code']==401){
            $lists_formatted = array( '' => __('API key is incorrect',Newsletter::$textDomain) );
        }

        if ( !empty ( $response['body'] ) ) {
            $campaigns = json_decode( $response['body'], true );
            
            if ( empty ( $campaigns['message'] ) ) {
                
                foreach ( $campaigns as $id => $list ) {
                    $lists_formatted[ $list['campaignId'] ] = $list['name'];
                }
            }
        }elseif($current){
            $lists_formatted[$current] = __('Current list',Newsletter::$textDomain);
        }
        return $lists_formatted;
    }

    public static function addAddress() {
        $ajax_response = array('code'=>0,'message'=>'');

        $name = trim($_POST['name']);
        $email = trim($_POST['email']);
        $rodo = $_POST['rodo'] == 'true' ? true : false;
        $api_key = carbon_get_theme_option('getresponse_api_key');
        $list_id = carbon_get_theme_option('getresponse_list_token');
        error_log("email: ".print_r(!$email,true));
        error_log("rodo: ".print_r(!$rodo,true));
        if(!$email){
            $ajax_response['message'] = crb_get_i18n_theme_option('newsletter_no_email');
        }elseif(!$rodo){
            $ajax_response['message'] = crb_get_i18n_theme_option('newsletter_no_rodo');
        }else{

            $body = array (
                'email' => $email,
                'dayOfCycle' => 0,
                'campaign' => array(
                    'campaignId' => $list_id,
                ),
            );
            if ( !empty( $name ) ) {
                $body['name'] = $name;
            }
            error_log(print_r($body,true));
                
            $args = array(
                'method' => 'POST',
                'timeout'     => 15,
                'redirection' => 15,
                'headers' => array ( "Content-Type" => "application/json", "X-Auth-Token" => "api-key $api_key" ),
                'body' => json_encode ( $body ),
            );

            $url = "https://api.getresponse.com/v3/contacts";

            $response = wp_remote_request( $url, $args);
            error_log(print_r($response,true));

            if( !is_wp_error( $response ) ) {
                $ajax_response['code'] = $response['response']['code'];
                if ( $response['response']['code'] == 202 ) {
                    $ajax_response['message'] = crb_get_i18n_theme_option('newsletter_ok');
                }
                elseif($ajax_response['code'] == 400 || $ajax_response['code'] == 409){
                    $ajax_response['message'] = crb_get_i18n_theme_option('newsletter_code_'.$ajax_response['code']);
                }else{
                    $ajax_response['message'] = crb_get_i18n_theme_option('newsletter_error');
                }
            }
        }
        echo json_encode($ajax_response);

        wp_die();
    }
}