<?php
// namespace Superskrypt\WpBackendBase\Newsletter;
namespace Superskrypt\WpBackendBase\Modules\Newsletter;

use Superskrypt\WpBackendBase\Modules\Newsletter\ThemeOptions as ThemeOptions;
use Superskrypt\WpBackendBase\Modules\Newsletter\GetResponse as GetResponse;
use Superskrypt\WpBackendBase\TemplateEngine\ContentProcessor as ContentProcessor;
use Timber\Timber;

class Newsletter {
    static $textDomain = 'newsletter';
    public static function shortcode(){
        $data = ContentProcessor::getData();
        Timber::render( array('newsletter.twig') , $data );
        return;
    }

    public static function init() {

        $result = load_textdomain( self::$textDomain, dirname( __FILE__ ) . '/languages/newsletter-'.determine_locale().'.mo' );

        add_action('carbon_fields_register_fields',function() {
            $system = get_option('_newsletter_system');
            if($system){
                call_user_func(["Superskrypt\\WpBackendBase\\Newsletter\\" . $system,'init']);
            }
        });
        new ThemeOptions();
        Timber::$locations[] = dirname(__FILE__) . '/twig';
        add_shortcode('newsletter',[__CLASS__,'shortcode']);
    }
}
