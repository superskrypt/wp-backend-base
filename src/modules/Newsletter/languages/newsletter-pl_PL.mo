��          �       |      |     }     �     �     �     �     �     �               5  +   T  !   �     �     �     �     �               "     0  .   F  �  u     �     
          3     S     n     �  !   �     �  3   �  *     6   B      y  )   �     �  5   �          %     3     I  +   b   Button label E-mail field label FreshMail API key FreshMail Newsletter FreshMail address list General error message GetResponse API key GetResponse Newsletter GetResponse address list Incorrect email format message Message after successful sending of address Message for contact already added Missing RODO agreement message Missing email message Newsletter block title Newsletter form enabled? Newsletter options Not set RODO fragment RODO rest of the text Text on a button to show the rest of RODO text Project-Id-Version: Backend Base Placeholder
POT-Creation-Date: 2023-11-20 18:49+0200
PO-Revision-Date: 2023-11-20 18:49+0200
Last-Translator: 
Language-Team: 
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ((n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14)) ? 1 : 2);
 Napis na przycisku Etykieta pola E-mail Klucz API FreshMail Newsletter w systemie FreshMail Lista wysyłkowa FreshMail Ogólna wiadomość o błędzie Klucz API GetResponse Newsletter w systemie GetResponse Lista wysyłkowa GetResponse Powiadomienie o niepoprawnym formacie adresu e-mail Wiadomość po skutecznym zapisaniu adresu Powiadomienie, że adres już znajduje się na liście Powiadomienie o braku zgody RODO Powiadomienie o nie podaniu adresu e-mail Tytuł bloku Newslettera Czy formularz zapisu na newsletter ma się pojawiać? Opcje newslettera Nie ustawiona Początek tekstu RODO Rozwinięcie tekstu RODO Napis na przycisku rozwijającym tekst RODO 