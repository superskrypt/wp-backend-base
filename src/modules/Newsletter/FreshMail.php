<?php
namespace Superskrypt\WpBackendBase\Newsletter;

use Superskrypt\WpBackendBase\WpBackendBase as WpBackendBase;
use FreshMail\ApiV2\Client;
use Exception;

use Carbon_Fields\Field;
class FreshMail {
    public static function init() {
        self::registerEndpoints();
    }

    public static function registerEndpoints() {
        add_action('wp_ajax_addAddress', [__CLASS__, "addAddress"]);
        add_action('wp_ajax_nopriv_addAddress', [__CLASS__, "addAddress"]);
        add_filter('wp_backend_base_newsletter_theme_options', [__CLASS__, "themeOptions"]);
    }
    public static function themeOptions($options){

        $freshmail_options = 
        array(
            Field::make( 'separator', 'newsletter_separator', __('FreshMail Newsletter', Newsletter::$textDomain)),
            Field::make( 'text', 'freshmail_api_key' , __('FreshMail API key', Newsletter::$textDomain)),
            Field::make( 'select', 'freshmail_list_token' , __('FreshMail address list', Newsletter::$textDomain))
            ->add_options(FreshMail::getCampaigns()),
        );
        return array_merge($options,$freshmail_options);

    }
    public static function getCampaigns($current = false) {

        $api_key = get_option('_freshmail_api_key');

        if($api_key){
            $lists_formatted = array( '' => __('Not set',Newsletter::$textDomain) );
        }else{
            $lists_formatted = array( '' => __('Set API key first',Newsletter::$textDomain) );
            return $lists_formatted;
        }
        try {
            $apiClient = new Client($api_key);

            $data = [
            ];

            $response = $apiClient->doRequest('subscribers_list/lists', $data);
            if(is_array($response) && array_key_exists('status',$response) && $response['status']=='OK' && array_key_exists('lists',$response)){
                $lists = $response['lists'];
            }else{
                return $lists_formatted;
            }
            foreach($lists as $list){
                $lists_formatted[$list['subscriberListHash']] = $list['name'];
            }
        }catch(Exception $exception){
            var_dump($exception->getMessage());
        }
        return $lists_formatted;
    }

    public static function addAddress() {
        $ajax_response = array('code'=>0,'message'=>'');

        $name = trim($_POST['name']);
        $email = trim($_POST['email']);
        $rodo = $_POST['rodo'] == 'true' ? true : false;
        $api_key = carbon_get_theme_option('freshmail_api_key');
        $list_id = carbon_get_theme_option('freshmail_list_token');
        if(!$email){
            $ajax_response['message'] = crb_get_i18n_theme_option('newsletter_no_email');
        }elseif(!$rodo){
            $ajax_response['message'] = crb_get_i18n_theme_option('newsletter_no_rodo');
        }else{

            $data = [
                'email' => $email,
                'list' => $list_id
            ];
            
            $apiClient = new Client($api_key);
            try{
                $apiClient->doRequest('subscriber/add', $data);
                $ajax_response['message'] = crb_get_i18n_theme_option('newsletter_ok');
                echo json_encode($ajax_response);
            }catch(Exception $exception){
                $ajax_response['code'] = $exception->getCode();
                switch($exception->getCode()){
                    case 1301:
                        $ajax_response['message'] = crb_get_i18n_theme_option('newsletter_code_400');
                        break;
                    case 1304:
                        $ajax_response['message'] = crb_get_i18n_theme_option('newsletter_code_409');
                        break;
                    default:
                        $ajax_response['message'] = crb_get_i18n_theme_option('newsletter_error') . ' kod błędu: ' . $exception->getCode();
                        

                }
                echo json_encode($ajax_response);
            }
        }

        wp_die();
    }
}