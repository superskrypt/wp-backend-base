export default class NewsletterController {
    constructor() {
        this.init();
    }
    init() {
        this.expanded = false;
        this.rodoExpanded = false;
        this.newsletter = document.getElementsByClassName('newsletter')[0];
        if(!this.newsletter) {
            return;
        }
        this.content = this.newsletter.getElementsByClassName('newsletter__content')[0];
        this.placeholder = this.content.getElementsByClassName('newsletter__placeholder')[0];
        this.emailInput = this.content.getElementsByClassName('newsletter__email')[0];
        this.checkbox = this.content.getElementsByClassName('newsletter__checkbox')[0];
        this.submitButton = this.content.getElementsByClassName('newsletter__button')[0];
        this.rodoButton = this.content.getElementsByClassName('newsletter__rodo__button')[0];
        this.rodoHidden = this.content.getElementsByClassName('newsletter__rodo__hidden')[0];
        this.successMessage = this.newsletter.getElementsByClassName('newsletter__success')[0];
        this.errorMessage = this.newsletter.getElementsByClassName('newsletter__error')[0];
        //
        this.placeholder.addEventListener('click',this.expand.bind(this));
        this.rodoButton.addEventListener('click',this.expandRodo.bind(this));
        this.submitButton.addEventListener('click',this.send.bind(this));
        window.addEventListener('resize',this.resize.bind(this));
        this.initialHide();
    }
    initialHide() {
        this.rodoShrunkHeight = this.rodoButton.offsetHeight - 2;
        this.rodoHidden.style.transitionProperty = '';
        this.rodoHidden.style.height = this.rodoShrunkHeight + 'px';
        this.shrunkHeight = this.placeholder.offsetHeight;
        this.content.style.transitionProperty = '';
        this.content.style.height = this.shrunkHeight + 'px';
    }
    expand() {
        this.expanded = true;
        this.placeholder.style.opacity = 0;
        this.placeholder.style.visibility = 'hidden';
        //
        this.content.style.height = 'auto';
        this.fullHeight = this.content.offsetHeight;
        this.content.style.height = this.shrunkHeight + 'px';
        this.content.style.transitionProperty = 'height'
        setTimeout(() => this.content.style.height = this.fullHeight + 'px',100);
        setTimeout(() => this.content.style.height = 'auto',600);
        this.emailInput.focus();
    }
    expandRodo() {
        this.rodoExpanded = true;
        this.rodoButton.style.opacity = 0;
        this.rodoButton.style.visibility = 'hidden';
        //
        this.rodoHidden.style.height = 'auto';
        this.rodoFullHeight = this.rodoHidden.offsetHeight;
        this.rodoHidden.style.height = this.rodoShrunkHeight + 'px';
        this.rodoHidden.style.transitionProperty = 'height'
        setTimeout(() => this.rodoHidden.style.height = this.rodoFullHeight + 'px',100);
        setTimeout(() => this.rodoHidden.style.height = 'auto',600);
    }
    resize() {
        if(this.expanded){
            if(!this.rodoExpanded){
                this.rodoShrunkHeight = this.rodoButton.offsetHeight - 2;
                this.rodoHidden.style.transitionProperty = '';
                this.rodoHidden.style.height = this.rodoShrunkHeight + 'px';
            }
        }
    }
    send() {
        const data = {
            action: 'addAddress',
            name: '',
            email: this.emailInput.value,
            rodo: this.checkbox.checked,
            nonce: superskrypt.nonce
        };

        const url = superskrypt.ajaxurl;
        this.timer = performance.now();
        return fetch(url, {
            method: "POST",
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Cache-Control': 'no-cache',
            },
            body: new URLSearchParams(data)
        })
        .then(response => {
            // console.log(response);
            return response.json();
        }
        )
        .then(data => {
            if(data.code == 202 || data.code == 409){
                this.successMessage.innerHTML = data.message;
                this.successMessage.style.height = 'auto';
                let successMessageHeight = this.successMessage.offsetHeight;
                this.successMessage.style.height = '0px';
                setTimeout(() => this.successMessage.style.height = successMessageHeight+'px',10);
                this.errorMessage.style.height = '0px';
                this.content.style.height = this.content.offsetHeight+'px';
                setTimeout(() => this.content.style.height = '0px',10);
            }else{
                this.errorMessage.innerHTML = data.message;
                this.errorMessage.style.height = 'auto';
                let errorMessageHeight = this.errorMessage.offsetHeight;
                this.errorMessage.style.height = '0px';
                setTimeout(() => this.errorMessage.style.height = errorMessageHeight+'px',10);
            }
        });
    }
}