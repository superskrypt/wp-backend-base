<?php
namespace Superskrypt\WpBackendBase\Modules\Newsletter;

use Superskrypt\WpBackendBase\WpBackendBase as WpBackendBase;
use Superskrypt\WpBackendBase\ThemeOptions as WpThemeOptions;
use Superskrypt\WpBackendBase\Modules\Newsletter\GetResponse as GetResponse;
use Carbon_Fields\Container;
use Carbon_Fields\Field;


class ThemeOptions {

    public function __construct() {
        
        add_filter( 'carbon_fields_theme_options_container_admin_only_access',function($enable) {
            if(current_user_can('editor') || current_user_can('administrator')) {
                $enable = false;
            }
            return $enable;
        }, 10, 1);

        add_action('carbon_fields_register_fields', function() {
            $this->language_code = apply_filters( 'wpml_current_language', NULL );
            $this->registerThemeOptionsCrb();
            $this->addToTimberContext();
        });
    }

    private function registerThemeOptionsCrb() {
        $this->options = array(
            Field::make('checkbox', 'is_newsletter_on', __('Newsletter form enabled?', Newsletter::$textDomain)),
            Field::make( 'select', 'newsletter_system', __('Newsletter system'))->set_options( array(
                'GetResponse' => 'GetResponse',
                'FreshMail' => 'FreshMail',
            ) ),
        );

        $this->options = apply_filters('wp_backend_base_newsletter_theme_options', $this->options, $this->options);
        
        $this->options = array_merge($this->options,
        array(
            Field::make( 'text', 'newsletter_title_' . $this->language_code, __('Newsletter block title', Newsletter::$textDomain))
            ->set_default_value( __('Subscribe to newsletter', Newsletter::$textDomain) ),
            Field::make( 'text', 'newsletter_label_' . $this->language_code, __('E-mail field label', Newsletter::$textDomain))
            ->set_default_value( __('Your e-mail address', Newsletter::$textDomain) ),
            Field::make( 'text', 'newsletter_button_label_' . $this->language_code, __('Button label', Newsletter::$textDomain))
            ->set_default_value( __('Subscribe', Newsletter::$textDomain) ),
            Field::make( 'textarea', 'newsletter_rodo_fragment_' . $this->language_code, __('GDPR fragment', Newsletter::$textDomain)),
            Field::make( 'textarea', 'newsletter_rodo_rest_' . $this->language_code, __('GDPR rest of the text', Newsletter::$textDomain)),
            Field::make( 'text', 'newsletter_rodo_button_' . $this->language_code, __('Text on a button to show the rest of GDPR text', Newsletter::$textDomain))
            ->set_default_value( __('See full GDPR disclaimer', Newsletter::$textDomain) ),
            Field::make( 'text', 'newsletter_no_rodo_' . $this->language_code, __('Missing GDPR agreement message', Newsletter::$textDomain))
            ->set_default_value( __('Please consent to the processing of personal data', Newsletter::$textDomain) ),
            Field::make( 'text', 'newsletter_no_email_' . $this->language_code, __('Missing email message', Newsletter::$textDomain))
            ->set_default_value( __('Please provide your e-mail address', Newsletter::$textDomain) ),
            Field::make( 'text', 'newsletter_code_400_' . $this->language_code, __('Incorrect email format message', Newsletter::$textDomain))
            ->set_default_value( __('E-mail format is incorrect', Newsletter::$textDomain) ),
            Field::make( 'text', 'newsletter_code_409_' . $this->language_code, __('Message for contact already added', Newsletter::$textDomain))
            ->set_default_value( __('Contact is already in our database', Newsletter::$textDomain) ),
            Field::make( 'text', 'newsletter_ok_' . $this->language_code, __('Message after successful sending of address', Newsletter::$textDomain))
            ->set_default_value( __('Thank you for subscribing to our newsletter!', Newsletter::$textDomain) ),
            Field::make( 'text', 'newsletter_error_' . $this->language_code, __('General error message', Newsletter::$textDomain))
            ->set_default_value( __('A problem occurred when saving address. Please try again later.', Newsletter::$textDomain) ),
        ));
        $this->optionsContainer = Container::make('theme_options', 'newsletter_options', __('Newsletter options',Newsletter::$textDomain))
            ->set_page_menu_position( 60 )
            ->set_icon( 'dashicons-email-alt' )
            ->add_fields($this->options);
    }

    private function addToTimberContext() {
        add_filter( 'timber/context', function ($context ) {
            foreach($this->options as $crbFieldInstance) {
                $fieldBaseName = $crbFieldInstance->get_base_name();
                $fieldName = str_replace('-','_', WpThemeOptions::getFieldNameWithoutLangSuffix($fieldBaseName));
                $context[$fieldName] = carbon_get_theme_option($fieldBaseName);
            }
            return $context;
        } );
    }
}

