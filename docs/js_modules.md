[Powrót do głównego pliku][main]

# Moduły JS w Wp BAckend Base

1. [Konfiguracja](#1) 
2. [Frontendowe filtrowanie(AUTOCARDS)](#2)  
    2.1 [Aktywacja frontendowego filtrowania](#2.1)
3. [Masonry layout] (#3)

<div id="1"></div>

## Konfiguracja 
**odpinanie w motywie**  
1. `.env BACKEND_BASE_JS_PATH=/Users/mactur/public_html/iccare/wp-content/dev/iccare-backend/vendor/superskrypt/wp-backend-base/src/js`  
2.  `w pliku js w motywie.... import { LazyLoad } from 'bb-modules/lazy-loading';` to ścieżka do folderu...*to z package.json zdaje sie nie działac....*

<div id="2"></div>

## Frontendowe filtrowanie(AUTOCARDS)

Dla bloków typu `autocards` można aktywować **mechanizm filtrowania po stronie frontendowej**. Filtrować można po **taksonomiach** przypisanych do postów wyświetlanych w ramach bloku `autocards`.  

<div id="2.1"></div>

### Aktywacja frontendowego filtrowania.

Aby aktywować mechanizm frontendowego filtrowania, należy w konfiguracji bloku autocardowego w sekcji `settings` dodać klucz `frontend_taxonomies_filtering` i przekazać do niego tablicę ze slugami typów taksonomii, po termach których powinno odbywać sie filtrowanie.

Przykład:  
```php
...
  "auto_blog_post" => array(
                    'use_schema' => 'auto_cards',
                    'settings' => array(
                        'frontend_taxonomies_filtering' => array('challenge_taxonomy', 'sector_taxonomy'),
                    ),
...
  )
```

<div id="3"></div>

## Masonry layout

Masonry layout zazwyczaj używany jest dla bloku Cards / Autocards. Mechanizm oparty jest na bibliotece [macy.js](https://github.com/bigbite/macy.js)

### Aktywacja

Aby aktywować layout masonry, należy:

1.  do głównego pliku projektu zaimportować klasę `MasonryController` z pliku `/bb-modules/masonry-layout/MasonryController.js`.

Przykład  
```javascript
import MasonryController from 'bb-modules/masonry-layout/MasonryController';
```   

2. Utworzyć instancje MasonryController. Mozna przekazać do konstruktora obiekt własnych opcji konfiguracyjnych (*opcjonalnie*). Oprócz właściwości `containers`,wszystkie właściwości powinny być zgodne z opcjami jakie przyjmuje macy.js.  
Właściwość `containers` zawiera tablicę selektorów, który elementy powinny być ułożone w stylu masonry. para

```javascript
  const masonryCustomOptions = {
        mobileBreakpoint: '900',
        margin: 24,
        columns: 4,
        containers: ['.block-cards .block__inner'],
    }
    new  MasonryController(masonryCustomOptions).init();
```  


[main]: ../README.md 