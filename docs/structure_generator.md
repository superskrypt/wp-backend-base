[Powrót do głównego pliku][main]

# Template Structure Generator – generowanie struktur 

Spis treści:  
* 1 [Inicjalizacja mechanizmu generowania struktur](#1)  
  * 1.1 [Dostepne opcje inicjalizacyjne](#1.1)  
* 2 [Definiowanie struktury](#2)  
  * 2.1 [Okreslanie gdzie struktura powinna być wygenerowana](#2.1)  
  * 2.2 [Okreslenie gdzie struktura nie powinna być renderowania](#2.2)  
  * 2.3 [Hero](#2.3)  
    * 2.3.1 [Standardowe Hero](#2.3.1)  
    * 2.3.2 [Customowe Hero](#2.3.2)  
  * 2.4 [Primary content](#2.4)  
    * 2.4.1 [Dodawanie dodatkowych pól do kontenera "Page Content"](#2.4.1)  
  * 2.5 [Sekcje strony](#2.5)  
    * 2.5.1 [Konfiguracja pól sekcji](#2.5.1)  
    * 2.5.2 [Dodatkowe pola w sekcji](#2.5.2)  
  * 2.6 [Bloki](#2.6)  
    * 2.6.1 [Lista bloków w sekcjach](#2.6.1)  
    * 2.6.2 [Definiowanie własnej kolejności pól](#2.6.2)  
    * 2.6.3 [Modyfikacja domyślnych bloków - dostępne opcje](#2.6.3)  
    * 2.6.4 [Tworzenie customowych bloków, które wykorzystują schemę defautowych bloków](#2.6.4)  
  * 2.7 [Szablony](#2.7)  
    * 2.7.1 [Nadpisywanie szablonów i makr](#2.7.1)  
  * 2.8 [Pełen przykład zdefiniowanej struktury](#2.8)  
* 3 [Theme Options][theme_options]  
* 4 [Komponenty][components]  
* 5 [Shortcodes][shortcodes]  
* 6 [Domyślne style css][default_styles]  
* 7 [Js modules][default_styles]  

[theme_options]: ./theme_options.md  
[components]: ./components.md  
[shortcodes]: ./shortcodes.md  
[default_styles]: ./default_styles.md  

___

<div id="1"></div>

## Inicjalizacja mechanizmu generowania struktur

```php
\Superskrypt\WpBackendBase\TemplateStructureGenerator::init(options);
```

<div id="1.1"></div>

### Dostepne opcje inicjalizacyjne

* ***twig_dir*** -  Można zdefiniować katalog w tworzonym motywie, który będzie zawierał szablony twig.
* ***textdomain*** - Można ustawić texdomain dla tłumaczeń etykiet pól. Textdomain powinna być taka sama jak textdomain we wtyczce/motywie. Powinno się ją załadować za pomocą funckcji ``` load_plugin_textdomain```.  
Szablon bazowy zawierający teksty etykiet, które mozna przetłumaczyc znajduje się w katalogu ```src/_translations/textdomain_placeholder-pl_PL.po``` - wstępnie ustawiony na język polski. Na bazie tego można stworzyć wymagane tłumaczenia i przekopiować je do odpowiedniego folderu we wtyczce/motywie skąd bedą pobierane.
Przy tworzeniu nowego szablonu tłumaczeń trzeba ustawić meta: "Plural-Forms" oraz "Language" oraz nadać nazwę plikowi według schematu MY_TEXT_DOMAIN-lang_code.po (np.: backendbase-en_GB). Plik po powinno skompilować się do pliku .mo np.: za pomocą programu poedit.

**Przykład** (załadowanie z subfolderu languages we wtyczce): 
```php
add_action( 'after_setup_theme', 'load_backend_textdomain' );

function load_backend_textdomain() {
    load_plugin_textdomain( 'MY_TEXT_DOMAIN', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' ); 
}
```

**Przykład** inicjalizacji mechanizmu generowania:

```php
\Superskrypt\WpBackendBase\TemplateStructureGenerator::init([
            'twig_dir' => get_template_directory() . '/twig',
            'textdomain' => 'MY_TEXT_DOMAIN'
]);
```
<div id="2"></div>

## Definiowanie struktury  

Wp Backend Base pozwala na szybkie wygenerowanie struktury strony. Pozwala na użycie zdefiniowanych w nim sekcji oraz bloków bądź stworzenie własnych bloków.
Każdy blok składa się z pól zdefiniowanych przy pomocy Carbon Fields oraz szablonu twig. 
Generowanie struktury polega na zdefiniowanie hero, zdefiniowaniu bloków jakie powinny być dostępne, określenia dla jakich typów posta struktura powinna być dostępna. Opcjonalnie można określić, kiedy struktura nie powinna być generowana.

<div id="2.1"></div>

### Okreslanie gdzie struktura powinna być wygenerowana ( klucz: '*show_on*' ) [xxx]

**Przykład 1:**  
```php
$structure = self::getPageStructure(); // struktura zdefiniowana w innym miejscu (array)
$structure['show_on'] => array('page', 'post', 'custom_post'); // renderuje pola dla typów postów "page", "post", "custom_post"
```

**Przykład 2:**  

```php
$structure = self::getPageStructure(); // struktura zdefiniowana w innym miejscu (array)
$structure['show_on'] => array('front_page', 'blog_home_page'); // renderuje pola dla strony zdefiniowaniej w wordpresie jako strona główna oraz dla strony postów 
```

<div id="2.2"></div>

### Okreslenie gdzie struktura nie powinna być renderowania ( klucz: '*dont_show_on*' ).

```php
$structure = self::getPageStructure(); // struktura zdefiniowana w innym miejscu (array)
$structure['dont_show_on'] = array('front_page', 'blog_home_page'); // nie renderuje struktury na stronie głównej oraz na stronie zdefiniowanej jako strona wyświetlania postów
```

<div id="2.3"></div>

### Generowanie hero

Wp Backend Base posiada domyślnie skonfigurowane Hero (hero_standard) tj. strukturę pól oraz szablon twig.
Aby użyć go dla stron należy dodać klucz "hero" do uprzednio zdefiniowanej struktury dla danego typu posta lub strony głównej.


<div id="2.3.1"></div>

#### Standardowe Hero

Standardowe hero może posiadać obrazek - do wyboru są:
 - obrazek jako tło hero (opcja default)
 - obrazek wyswietlajacy się pod tekstami w hero.

Do ustawiania odpowiedniego trybu służy kontrolka select `Image layout variant`

**Przykład 1:**  

```php
 $structure = self::getPageStructure(); // struktura zdefiniowana w innym miejscu
 $structure['hero'] = true;
  \Superskrypt\WpBackendBase\TemplateStructureGenerator::generateStructure($structure); // generowanie struktury dla danego typu posta/strony głównej
```

**Przykład** 2:**  
Kastomizacja ustawień defaultowego hero (dodawanie dodatkowych pól, zmiana kolejności pól, usunięcie defaultowych pól)  

```php
 $structure['hero'] = true;
 $structure['hero_settings'] = array(
            'additional_fields' => self::customHeroFields(), //metoda zwracająca pola carbon fields
            'disabled_fields' => array('video'), 
            'disabled_block_options' => array(),
            'order' => ['lead']
        );
```

<div id="2.3.2"></div>

#### Customowe Hero

Możliwe jest również zdefiniowanie własnych hero (pola + szablon) np.: dla typu posta lub strony głównej.

**Przykład 1:**
  
```php
$structure = getPageFrontStructure(); // struktura zdefiniowana w innym miejscu
$structure['hero'] = array(
    'front' => function () { 
            return  array(
                    \Carbon_Fields\Field::make( 'text', 'hero_front_lead', 'Lead' ),
                    ... pozostałe pola
            );
    };
)
\Superskrypt\WpBackendBase\TemplateStructureGenerator::generateStructure($structure); // generowanie struktury dla danego typu posta/strony głównej
```
 
Przykład 2 - dwa typy customowego hero do wyboru ( base_hero i alternate_hero )

```php
$structure = getPageFrontStructure(); // struktura zdefiniowana w innym miejscu
$structure['hero'] = array(
    'base_hero' => function () { 
            return  array(
                    \Carbon_Fields\Field::make( 'text', 'base_hero_lead', 'Lead' ),
                    ... pozostałe pola
            );
    };
    'alternate_hero' => function () { 
            return  array(
                    \Carbon_Fields\Field::make( 'text', 'hero_alternate_description', 'Description' ),
                    ... pozostałe pola
            );
    };
)
\Superskrypt\WpBackendBase\TemplateStructureGenerator::generateStructure($structure); // generowanie struktury dla danego typu posta/strony głównej
```
<div id="2.4"></div>

### Primary content


Primary content to sekcja znajdująca się pomiędzy hero a zwykłymi sekcjami strony. Od zwykłej sekcji różni się tym, że ma stały zestaw bloków.
Definiuje się ją przez dodanie do definicji struktury parametru w formie tablicy z nazwami pól, które mają się znaleźć w tej sekcji.

**Przykład:**   

```php
$structure['primary_content'] = array('lead','highlight','text','image','quick_links');
```  
<div id="2.4.1"></div>

#### Dodawanie dodatkowych pól do kontenera "Page Content"

Domyślnie w kontenerze "Page content" znajdują się tylko Sekcje oraz, jeśli jest zdefiniowana to również sekcja `Primary Content`. Aby dodać dodatkowe pola do kontenera (domyślnie znajdują tam sie tylko sekcje) nalezy, podobnie jak w przypadku sekcji "Primary Content", dodać do struktury parametru `page_content_additional_fields` closure zawierajace definicji dodatkowych pól. 

**Przykład:**    
```php
$structure['page_content_additional_fields'] = self::pageContentAdditionalFields();

 // w innym miejscu 
   public static function pageContentAdditionalFields() {
        return function () {
            return [
                \Carbon_Fields\Field::make( 'radio', 'page_color_type', __('Page type','textdomain') )
                    ->add_options(array(
                        'basic' => __('Basic','textdomain'),
                        'color' => __('Color','textdomain')
                    ))
                    ->set_default_value('bg-light-grey'),
            ];
        }
   }

```

**TemplateStructureGenerator** przetworzy te pola **dodając do sluga każdego pola prefix** `page_additional_`.  
 Wyjściowo slug takiego pola będzie `page_additional__SLUG_BAZOWY` np.:  `page_additional__page_color_type`.



<div id="2.5"></div>

### Sekcje strony

<div id="2.5.1"></div>

#### Konfiguracja pól sekcji

Sekcje posiadają predefiniowane pola. Podzielone są na dwie kategorie:
* *section_options*
* *section_heading*

***section_options*** - lista pól:
* *bg_color*
* *bg_image*

***section_heading*** - lista pól:
* *headings_text_color*
* *title*
* *subtitle*

ustawienie wartości klucza pola na true oznacza że będzie wyrenderowane

<div id="2.5.2"></div>

##### ***dodatkowe pola w sekcji***:

Istnieje możliwość dodania własnych pól do sekcji za pomocą klucza ***additional_fields*** 


Przykład konfiguracji pól sekcji:

```php
[
    "section" => array(
            "section_options" => array(
                    'bg_color' => false,
                    'bg_image' => false,
            ),
            "section_heading" => array(
                    'headings_text_color' => false,
                    'title' =>  true,
                    'subtitle' =>  false,
            ),
            "additional_fields" => self::getSectionCustomFields(),// funkcja zwracająca closure które zwraca tablicę pól Carbon Fields.
    ),
              
    ... konfiguracja bloków itd.
  ]           
```
<div id="2.6"></div>

## Bloki

 Wp backend base pozwala na zdefiniowanie tzw. bloków treściowych, które można umieszczać w sekcjach. Bloki takie moga zawierać różne rodzaje treści, różną strukturę. Stanowią bardzo elastyczny mechanizm tworzenia treści na stronie. Lista dostępnych bloków znajduje się poniżej. Dodatkowo `Wp Backend Base` pozwala na tworzenie własnych bloków, które mogą mieć całkowicie niezależną strukturę oraz bloków, które mogą wykorzystywać strukturę bloków standardowych.  


Domyślna ścieżka dla bloków  ```templates/twig/_partials/blocks```  

 <div id="2.6.1"></div>

 ### Lista Standardowych bloków w sekcjach  

* [cards](blocks/cards.md)
* [auto_cards](blocks/auto_cards.md)
* [quick_links](blocks/quick_links.md)
* [big_links](blocks/big_links.md)
* [subheading](blocks/subheading.md)
* [lead](blocks/lead.md)
* [text](blocks/text.md)
* ~~multi_column_text~~ – *przestarzały i wyłaczony*
* [cta](blocks/cta.md)
* [image](blocks/image.md)
* [highlight](blocks/highlight.md)


<div id="2.6.2"></div>

### Definiowanie własnej kolejności pól

Domyślnie pola w bloku mają zdefiniowaną kolejność. Gdy np.: dodajemy customowe pola, wówczas ustawiane są one na końcu listy pól. 
Mechanizm generowania pól pozwala na zdefiniowanie własnej kolejności pól zarówno w blokach zwykłych jak i blokach typu complex. W blokach typu complex możliwe jest sortowanie zarówno na poziomie itemu jak i całego bloku. Konfiguracja mechanizmu sortowania polega na dodaniu, w kluczu `order`, tablicy z listą pól (id pola carbon fields) w kolejności w jakiej chcemy aby się wyświetlały w cms.  
Aby móc sortować pola itemu trzeba w tablicy `order` pod kluczem `item_order` dodać tablicę z listą pól itemu.  

**Przykład:**   
*Przykład definicji bloku cards z dodatkowymi polami na pierwszym poziomie i w itemie. Pola wymienione w kluczu `order` i `item_order` będą renderowanie jako pierwsze. Wszystkie pozostałe będą dodanie na koniec; odpowiednio dla pierwszego poziomu jak i itemu*  

```php
"cards" => [
    ...
    'order' => array(  
        'cards_options__columns,
        'item_order' => [
                            'bg_color', 
                            'item_custom_field', // dodatkowe pole dodane na na poziomie itemu za pomocą klucza "additional_item_fields"
                            'subtitle_test',
                        ], )
        ],
    'button' // dodatkowe pole dodane na najwyższym poziomie za pomocą klucza "additional_fields"
    ),
    ...
```  
<div id="2.6.3"></div>


### Modyfikacja domyślnych bloków - dostępne opcje

 
 *  *additional_fields* - pozwala dodać do bloku dodatkowe pola, generowane za pomocą carbon fields
 
 Przykład: 
```php
  ...
  "additional_fields" =>  function () { 
            return  array(
                    \Carbon_Fields\Field::make( 'text', 'custom_field_slug', 'Additional text' ),
                );
        };
  ...   
```
 
 * *additional_item_fields* - pozwala na dodanie pól do itema gdy mamy blok typu complex
 
 Przykład:

```php
    ...
    "additional_item_fields" =>  function () { 
            return  array(
                    \Carbon_Fields\Field::make( 'checkbox', 'offset', 'Top offset enebled' ),  
                );
        };
    ...   
```
 
 
 * *disabled_fields* - pozwala wyłaczyć domyślne pola
 
 Przykład:

```php
"disabled_fields" => array('image', text) 
```
 
 * *disabled_block_options* - wyłączenie domyślnych opcji bloku. Działa tylko gdy takie są zdefiniowane dla bloku
  
Przykład:

```php
'disabled_block_options' => array('column')
```
 
 * *custom_block_label* - pozwala ustawić labelkę dla bloku, która będzie widoczna w panelu administracyjnym
 Przykład:
```php
'custom_block_label' => 'Cards'
```

<div id="2.6.4"></div>

### Tworzenie nowych bloków, które wykorzystują schemę defautowych bloków
  
Mechanizm tworzenia bloków pozwala na tworzenie własnych bloków z unikatowymi polami, definiowannymi jako *`closure zwracający tablicę pól carbon fields`* lub jako *`tablicę pól carbon fields`*.  
Istnieje również możliwość zdefinowania własnego bloku, który będzie używać schemy z bloków defaultowych. 
Aby skorzystać z tego mechanizmu należy:
1.  w definicji customowego bloku dodać opcję `use_schema` i ustawić jej wartoć na na nazwę któregoś z defaultowych bloków (lista bloków dostępna jest poniżej)  

**Przykład:**
```php
    'custom_block_name' => [
        'use_schema' => 'cards'
    ]
```
2. utworzyć w motywie wordpresowym szablon twig dla naszego bloku, a w nim inkludować szablon bloku z którego korzystamy.  
Przykład.  
*`_partials/blocks/custom_block_name.twig`*
```twig
    {% include "_partials/blocks/cards.twig" %}
```


<div id="2.7"></div>

## Szablony Twig

<div id="2.7.1"></div>

### Nadpisywanie szablonów i makr

Każdy szablon sekcji lub bloku można nadpisać we własnym motywie. Ścieżka względna do szablonu nadpisywanego musi pokrywać się ze ścieżką względną szablonu zdefiniowanego w Wp Backend Base.
W nadpisanym szablonie będą działały makra twigowe, które zdefiniowane są w Wp Backend Base. 
Makra można nadpisać analogicznie do nadpisywania szablonów, czyli ścieżka i nazwa makra musi się zgadzać z tymi nadpisywanymi




<div id="2.8"></div>

## Pełen przykład zdefiniowanej struktury

```php
   private static function getPageStructure() {
        return [
            "section" => array(
                  "section_options" => array(
                      'bg_color' => false,
                      'bg_image' => false,
                  ),
                  "section_heading" => array(
                      'headings_text_color' => false,
                      'title' =>  true,
                      'subtitle' =>  false,
                  ),
                  "additional_fields" => self::getSectionCustomFields(),// funkcja zwracająca closure które zwraca tablicę pól Carbon Fields.
              ),
            "sections_blocks" => array(
                "cards" => [
                    'disabled_fields' => array('column'), // usunięcie pola content (jedno z domyślnych pól bloku)
                    'disabled_block_options' => array('column'), // usunięcie pola z wyborem ilości kolumn (domyślne pole opcji bloku. )
                    'additional_item_fields' => self::getCustomItemFields(), // własna funkcja zdefiniowana gdzieś indziej zwracająca closure
                    'custom_block_label' => 'Custom Boxes' // tytuł (nazwa) bloku w CMS
                    'order' => array(  
                        'cards_options__columns,
                        'item_order' => [
                            'bg_color', 
                            'item_custom_field', // dodatkowe pole dodane na na poziomie itemu za pomocą klucza "additional_item_fields"
                            'subtitle_test',
                            ], 
                        'button' // dodatkowe pole dodane na najwyższym poziomie za pomocą klucza "additional_fields"
                        ),

                ],

                "quick_links" => array(
                    "additional_item_fields" => function () { 
                            return  array(
                                    \Carbon_Fields\Field::make( 'checkbox', 'offset', 'Top offset enebled' ),
                                );
                        };
                    "additional_fields" => self::getCustomQuickLinksBlockFields(), // własna funkcja zdefiniowana gdzieś indziej zwracająca closure
                ),
                "imagebox_links" => true,
                "subheading" => true, 
                "text" => true,
                "multi_column_text" => false, // dezaktywacja bloku. Taki blok nie pojawi się na liście bloków do wyboru. Można również nie dodawać go do tablicy. 
                "lead" => true, 
                "cta" => true, 
                "image" => true,
                
                "custom_block" => function () { 
                    return  array(
                            \Carbon_Fields\Field::make( 'text', 'text_fieldslug', 'text' ),
                            \Carbon_Fields\Field::make( 'text', 'custom_block_slug', 'text' ),
                        );
                    };
        szablonu w motywie ścieżka: _partials/blocks/custom_block.twig
                "highlight" => true,
                
            )
        ];
    }
```


[Powrót do głównego pliku][main]  

[main]: ../README.md 