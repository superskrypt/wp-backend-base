[Powrót do głównego pliku][main]

# THEME OPTIONS  

Wp-backend-base pozwala na dodanie domyślnych pól **Theme Options** (funkcjonalność biblioteki [Carbon Fields](https://docs.carbonfields.net/learn/containers/theme-options.html)). Domyślnie pola Theme Options zawsze są generowane, chyba że podczas inicjalizacji `wp-backend-base` zablokujemy te funkcjonalność poprzez ustawienie opcji `theme_options` na wartość `false`.
Domyślnie `Theme options` podzielone są na kilka grup:
 - skip_links
 - header
 - language_switcher
 - menu  

Każda z tych grup zawiera pola, które pojawią sie w menu w panelu administracyjnym wordpressa. 
Zawartość grup można modyfikować poprzez użycie filtra `wp_backend_base_theme_options`. Funkcja zwrotna tego filtra przyjmuje **jeden parametr, którym jest tablica asocjacyjna** gdzie kluczem jest **nazwa grupy** a wartością **definicje pól Carbon Fields**. Każda grupa posiada w panelu wordpress odpowiedni nagłówek, dzięki temu redaktor może się szybko zorientować jakiej funkcjonalności dane pola dotyczą.  
Domyślnie tablica ma następującą postać:
```php
array(
    'skip_links' => array (
        Field::make( 'separator', 'skip_links', __('Skip links (accessibility)', self::$textDomain)),
        Field::make( 'text', 'skip_link_main' . $fieldLanguageSuffix  , __('Text for main skip navigation link', self::$textDomain)),
    ),
    'header' => array(
        Field::make( 'separator', 'header_separator', __('Header', self::$textDomain)),
        Field::make( 'text', 'header_logo_link_label' . $fieldLanguageSuffix   , __('Home logo link label (in header)', self::$textDomain)),

    ),
    'language_switcher' => !empty(self::$languageCode)  ? array(
        Field::make( 'separator', 'language_switcher_separator', __('Language switcher ', self::$textDomain)),
        Field::make( 'text', 'switch_to_pl_label' , __('Label: Przełącz na język polski', self::$textDomain)),
        Field::make( 'text', 'switch_to_en_label' , __('Label: Switch to english', self::$textDomain)),
    ) : array(),
    'menu' => array(
        Field::make( 'separator', 'menu_separator', __('Menu ', self::$textDomain)),
        Field::make( 'text', 'menu_home_link_label' . $fieldLanguageSuffix , __('Home link label (in main menu)', self::$textDomain)),
        Field::make( 'text', 'menu_opened_label' . $fieldLanguageSuffix , __('Label "Open menu" (accessibility)', self::$textDomain) ),
        Field::make( 'text', 'menu_closed_label' . $fieldLanguageSuffix , __('Label "Close menu" (accessibility)', self::$textDomain) ),
    ),
);
```
Dzięki filtrowi `wp_backend_base_theme_options` można:
 - dodać dodatkowe pola do odpowiednich grup, tak aby nowe pola pojawiły się w panelu wordpress w odpowiedniej sekcji `theme options`
 - nadpisać/usunąć domyślne pola
 - dodać nowe pola poza grupami.

Przykład użycia filtra `wp_backend_base_theme_options`.
```php
// dodanie filtra...
add_filter('wp_backend_base_theme_options', 'registerThemeOptionFields', 9, 1);

function registerThemeOptionFields($defaultThemeOptions) {
    // SPOSÓB NA DODANIE NOWYCH PÓL DO GRUPY 'MENU':
    if(isset($defaultThemeOptions['menu'])) {
        $menuFields = array(
            Field::make( 'text', 'menu_custom_field' . self::$fieldLanguageSuffix , __('Jakieś dodatkowe pole w menu', 'text-domain-tłumaczeń')),
        );
        $defaultThemeOptions['menu'] = array_merge($defaultThemeOptions['menu'], $menuFields);
    }

    // DODANIE ZUPEŁNIE NOWEGO ZESTAWU PÓL. ZESTAW TEN BĘDZIE DODANY POZA DOMYŚLNYMI GRUPAMI.
    array_push($defaultThemeOptions, self::getFooterMenuThemeOptionsFields());

    return $defaultThemeOptions;
}

function getFooterMenuThemeOptionsFields() {
        return array(
            Field::make( 'text', 'footer_test_field' . self::$fieldLanguageSuffix , __('Jakieś zupełnie inne pole dodane we wtyczce', 'text-domain-tłumaczeń')),
        );
    }
```
### TŁUMACZENIA THEME OPTIONS
Mechanizm domyślnych `Theme Options` posiada integrację z `WPML`. Jeśli w wordpresie aktywowana jest wtyczka `WPML`, wówczas pola `Theme Options` będą uwzględniały wersję językową – slugi pól będą tworzone z dodaniem odpowiedniego kody języka np.: `header_logo_link_label_pl`. Wszystkie etykiety domyślnych pól `Theme Options` mają ustawioną text-domain na domyślną dla całego `wp-backend-base` czyli `wp-backend-base`.  
Etykiety nowych pól dodawanych do `Theme Options` powinny mieć ustawioną `text-domain` właściwą dla wtyczki/motywu w którym `wp-backend-base` jest aktywowany.  
### THEME OPTIONS – TWIG CONTEXT

Wszystkie pola domyślnie lub dodawane za pomocą filtra `wp_backend_base_theme_options` są automatycznie dodawane do `twig context`, czyli automatycznie dostępne są, za pośrednictwem zmiennych, z poziomu szablonu twiga. Zmienne te definiowane są według schematu: `slug_pola`. Dodatkowo podczas generowania nazw zmiennych stosowane jest kilka zasad:  
1. wszystkie `-` zamieniane są na `_`.
2. w nazwie zmiennej nie są uwzględniane sufiksy językowe (`_pl`, `_en` itd.)

Pobieranie wartości tłumaczonych pól, gdy używany jest `WPLM` odbywa się za pomocą funkcji globalnej zdefiniowanej w  `wp-backend-base` – `wp-backend-base/src/php/GlobalFunctions/support_for_wpml_theme_options.php`;


[Powrót do głównego pliku][main]

[main]: ../README.md 