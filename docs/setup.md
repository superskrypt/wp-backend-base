[Powrót do głównego pliku][main]

# Wp Backend Base  

Spis treści:  
1. [Instalacja i użycie](#1)  
2. [Uswanie ładowania skryptów](#2)  
   
___


<div id="1"></div>

## Instalacja i użycie.
```console
composer install superskrypt/wp-backend-base
```
  
**Przykładowa implementacja:**
```
https://bitbucket.org/superskrypt/wp-theme-example/src/master/  // Przykładowy motyw
https://bitbucket.org/superskrypt/wp-backend-base/src/master/   // przykładowa wtyczka zawierająca konfigurację wp-backend-base
```  
Przykład:    
```php
\Superskrypt\WpBackendBase\WpBackendBase::setupBackend( 
    array( 
        'social_media_links' => true,
        'shortcodes' => array('lang')
        'disable_posts' => false,
        'menu_pages_to_top' => true,
        'remove-embeds' => false,
        'themeSetup' => array(
            'add_theme_support' => array(
                'html5' => array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'style', 'script'),
                'responsive-embeds',
                'wp-block-styles',
                'align-wide',
                'editor-styles',
                'style-editor.css',
                'post-thumbnails' => array('post', 'page')
            ),
        ),
    )
);
```
<div id="2"></div>

## Uswanie ładowania skryptów
Z poziomu ustawień `WpBackendBase` mozna zadeklarować, które skrypty ładowane przez wordpressa lub wtyczki automatycznie powinno się usunąć.
Przykład:
```
\Superskrypt\WpBackendBase\WpBackendBase::setupBackend( 
    array( 
        ...
        'remove_scripts' => array('jquery'),
        ...
);
```  
[Powrót do głównego pliku][main]  
  
[main]: ../README.md 