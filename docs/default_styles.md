[Powrót do głównego pliku][main]

# Domyślne style

## Domyślne style - konwencja mechanizmu.

Wp-backend-base posiada mechanizm generowania pliku `scss` zawierającego importy plików składowych do domyślnych styli scss dla generowanego html-a.  Wśród tych plików znajduja sie definicje zmiennych, mixins dla grida i media-queries. zestawy styli dla ogólnego layoutu, sekcji, hero oraz bloków.  
Aby wykorzystać ten mechanim w projekcie należy, w projekcie w pliku `.env` dodać stałą `BACKEND_BASE_SCSS_MODULES_PATH` i wskazać foldes scss w katalogu instalacji wp-backend-base.
Domyśne ustawienia powodują dołaczenie styli dla wszystkich bloków. Style dla poszczególnych bloków można wyłączyć w konfiguracji bloku poprzez ustawienia klucza `styles` na wartość `false`.  
Przykład:
```php
    "lead" =>
         array(
            'styles' => false,
        ),
```
## Praca z domyślnymi stylami wp-backend-base

Kompilacja domyślnych plików styli wp-backend-base odbywa sie przypomocy odpowiedniego bundlera [@superskrypt/nps-tasks](https://www.npmjs.com/package/@superskrypt/npm-tasks).  
Bundler `@superskrypt/nps-tasks`, jeśli konfiguracja projektu jest poprawna (patrz wyżej `BACKEND_BASE_SCSS_MODULES_PATH` ), łaczy, grupuje i kompliuje pliki z importami z wp-backend-base oraz z katalogu z stylami w projekcie.
Można pracowac w dwóch trybach:
1.  **nadpisywania** - jeśli w katalogou projekty znajduje sie plik z tą samą nazwą oraz z tą samą ścieżka wzgledną to wówczas w pliku wynikowym dołaczane są definicje pliku w projekcie. **UWAGA!** jeśli nadpisujemy plik który definiuje zmienne scss wykorzystywane w dalszych plikach, to wówczas powinniśmy również zdefiniować/nadpisać te zmienne w naszym pliku – w 
2. **rozszerzania** - jeśli chcemy rozszerzyć domyślne style pobierane w `wp-backend-base` wówczas możemy zamiast nadpisywać bazowy plik utworzyć nowy - najlepiej trzymać się konwencji `NAZWA_PLIKU_PODSTAWOWEGO-theme`.

### ```1``` Bloki - konfiguracja ustawień layoutu
Każdy blok posiada zdefiniowane określone zmienne dotyczace jego podstawowych parametrów takich jak: marginesy, paddingi,wyrównanie teksty, gap, max-width itp. Każdy z tych parametów możliwy jest do nadpisania z poziomu motywu.

### ```2``` Nadpisywanie domyślnych parametów bloków
Aby nadpisać domyślne, konfigurowalne parametry bloków, należy w motywie w folderze `0_mixins` dodać plik, zawierający definicje zmiennych, których wartości chcemy nadpisać.
**UWAGA!** Plik ten nie może nazywać się `_blocks-const.scss` ponieważ wówczas w całości zostanie nadpisany, co może skutkować błedami w kompilacji. W celu uniniknięcia konfliktu i niezamierzonego nadpisania, plik taki może się nazywać np.: `_blocks-const-theme.scss`.

Szczegółowa dokumentacja działania @superskrypt/nps-tasks znajduje się pod tym [linkiem](https://www.npmjs.com/package/@superskrypt/npm-tasks?activeTab=readme)  

## System kolorów

## Mixiny
1. **Tła** - (`1_mixins/backgrounds.scss`) 
`element-on-bg` pozwala na wstawienie ostylowania obowiązującego tylko na jasnym, lub tylko na ciemnym tle (pierwszy parametr [`dark`,`light`]).
- W drugim parametrze można podać, jaki element html jest tłem, jeśli nie jest to domyślna sekcja strony (`.page-section`).
- Opcjonalny trzeci parametr pozwala podać ostylowywany element, kiedy mixin nie jest zagnieżdżony w opisie stylu elementu.
- Ustawienie trzeciego parametru na `true` usuwa zapis uwzględniający wszystkie obiekty nadrzędne z jasnym lub ciemnym tłem i ogranicza się tylko do sprecyzowanego w drugim parametrze.

`hover-on-bg` nadaje automatycznie odpowiedni kolor przy najechaniu na element, dostosowany automatycznie do ciemnego i jasnego tła, na podstawie definicji zmiennej mapowej `$color`.

2. **Typografia** - (`1_mixins/typography.scss`) mixiny czcionek są stworzone w taki sposób, aby móc w nich style typograficzne opisywać hierarchicznie przy pomocy mixinu text-base. W każdym mixinie dla poszczególnego stylu typograficznego na końcu dodana jest instrukcja @include, w której należy podać mixin stylu nadrzędnego, albo mixin text-base, jeśli dany styl jest stylem najwyższego rzędu. Poszczególne parametry ostylowania w mixinach styli typograficznych podawać należy w postaci parametrów mapy $opts. Poza standardowymi parametrami ostylowania jest jeszcze dostępny parametr "scaling", który może przyjąć wartości: "standard", "title" lub "box-title", co określa, który mixin ma być użyty do skalowania czcionki w zależności od szerokości ekranu.

3. **Skalowanie czcionek** - (`1_mixins/fonts.scss`) mixiny do skalowania czcionek: `scale-font-to-screen`, `scale-title-to-screen`, `scale-box-title-to-screen` 
ustawiają wielkość czcionki w zależności od szerokości ekranu. Korzystają z funkcji linear-function, żeby wygenerować wielkość czcionki w postaci Apx + Bvw na podstawie podanych wartości dla konkretnych szerokości ekranu.
- `scale-font-to-screen` – służy do skalowania standardowych styli typograficznych, w szczególności tekstów blokowych.
- `scale-title-to-screen` – służy do skalowania styli typograficznych używanych w tytułach, które przy dużych rozdzielczościach ekranu powinny być duże, ale na urządzeniach mobilnych powinny się mocno zmniejszyć.

4. **Siatka strony (grid)** - (`1_mixins/grid.scss`) zestaw mixinów pozycjonujących elementy html w siatce strony:
- `main-grid` – nadaje elementowi zarówno zdefiniowane dla projektu marginesy, jak i siatkę. Nadaje się do użycia w elementach, które rozciągają się na całą szerokość strony i mają bezpośrednio w sobie elementy, które powinny być ułożone na siatce, np. w bloku galerii, czy w stopce
- `inner-grid` - nadaje elementowi tylko siatkę zdefiniowaną dla całej strony
- `main-grid-margins` – nadaje elementowi tylko poziome marginesy zdefiniowane dla całej strony, nie definiując wewnętrznego układu. Dobre np. dla bloku `cards`, w którym elementy układane są przez skrypt Macy.
- `main-grid-left-margin`, `main-grid-right-margin` – odpowiednio lewy i prawy margines zdefiniowany dla całej strony.

5. **Bloki** - (`1_mixins/blocks.scss`)
`block-padding` ustawia paddingi dla elementu odpowiednio dla rozdzielczości desktopowej i mobilnej na podstawie przekazanej zmiennej mapowej.

6. **Hover** - (`1_mixins/hover.scss`) zestaw mixinów do ustawiania ostylowania elementów po najechaniu wskaźnikiem myszy.
- `hover` – pozwala na wstawienie ostylowania, które ma dotyczyć tylko stanu po najechaniu wskaźnikiem
- `mask-on-hover` – mixin do wstawienia standardowej maski, która przykrywa elementy pudełkowe i zmienia wygląd po najechaniu wskaźnikiem myszy
- `mask-no-hover` – standardowa maska w stanie bez najeżdżania wskaźnikiem
- `mask-hover-state` – standardowa maska w stanie po najechaniu

7. **Buttons** - (`1_mixins/button-base.scss`, `1_mixins/buttons.scss`) – zestaw mixinów do ostylowania przycisków typu `primary`, `secondary`, `link` oraz `filter`.
Zmiany w ostylowaniu przycisków charakterystyczne dla danego projektu należy wprowadzać przez nadpisanie lub rozszerzenie pliku `1_mixin/buttons-const.scss`. Podstawowe parametry ustawia się w nim w ramach zmiennych mapowych, a dodatkowe style można dołożyć w mixinach. `Uwaga!` Zmienna mapowa `$button` jest wykorzystywana zarówno w przyciskach primary, jak i secondary, przy czym zmienna `$button-secondary` nadpisuje jej wartości. Tak więc można ustawić style wspólne dla obu typów przycisków w zmiennej `$button`, a potem w zmiennej `$button-secondary` ustawić tylko style odróżniające przyciski secondary od primary.
- `button-base` – definiuje podstawowe style przycisków primary i secondary
- `button-base-secondary` – dodaje do podstawowych styli parametry zdefiniowane w projekcie dla przycisków secondary.
- `button-base-link` – definiuje podstawowe style przycisków typu link
- `button-base-filter` – definiuje podstawowe style przycisków typu filter
- `button-variant-style` – przypisuje kolor, kolor tła i kolor ramki do danego wariantu i stanu przycisku wedle przekazanej mapy. Jest wykorzystywany w większości poniższych mixinów. Nie jest przewidziany do wykorzystywania bezpośrednio gdzie indziej.
- `button-primary` – wstawia ostylowanie dla przycisku typu primary z automatycznym dostosowaniem do jasności tła i uwzględnieniem stanów bez najechania i po najechaniu (hover). Można przekazać do niego element służcy za tło, jeśli nie jest to element typu `.page-section`.
- `button-primary-dark` – wariant przycisku typu primary w wersji ciemnej – na jasnym tle, z uwzględnieniem stanu hover i bez hover. Można go stosować, kiedy w danym miejscu przycisk zawsze jest na jasnym tle, lub element tła nie ma nadanej klasy koloru tła (np. `.bg-black`).
- `button-primary-dark-no-hover`, `button-primary-dark-hover` – para mixinów definiująca ostylowanie przycisku primary w wariancie ciemnym (na jasnym tle) odpowiednio dla stany bez hover i z hover. Przydatne, jeśli przycisk znajduje się w większym elemencie aktywnym i ma reagować na najechanie na niego, a nie tylko na swoje pole aktywne.
- `button-primary-light`, `button-primary-light-no-hover`, `button-primary-light-hover` - mixiny analogiczne do trzech powyższych, tylko w wariancie jasnym (na ciemnym tle).
- `button-secondary`, `button-secondary-dark`, `button-secondary-dark-no-hover`, `button-secondary-dark-hover`, `button-secondary-light`, `button-secondary-light-no-hover`, `button-secondary-light-hover` – mixiny analogiczne dla powyższych mixinów primary, tyle że dla przycisków typu secondary.
- `button-link`, `button-link-dark`, `button-link-dark-no-hover`, `button-link-dark-hover`, `button-link-light`, `button-link-light-no-hover`, `button-link-light-hover` – mixiny analogiczne dla powyższych mixinów primary i secondary, tyle że dla przycisków typu link.
- `button-filter`






[main]: ../README.md 