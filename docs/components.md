[Powrót do głównego pliku][main]

# KOMPONENTY  
1. [Social media links](#1)  
    1.1 [Konfiguracja](#1.1)  
    1.2 [Nadpisywanie szablonów i ikon](#1.2)  
    1.3 [Dodawanie nowych mediów społecznościowych](#1.3)  
    1.4 [Lista prekonfigurowanych serwisów mediów społecznościowych](#1.4)
    1.5 [Integracja z motywem hooks](#1.5)

<div id="1"></div>

## Social media links

Wp-backend-base pozwala na dodanie do strony mechanizmu generowania i wyświetlania linków do mediów społecznościowych wraz z ich ikonami.

<div id="1.1"></div>

### Konfiguracja

Aby włączyć ten mechanizm trzeba przy inicjalizacji `WpBackendBase` dodać klucz `social_media_links` do tablicy konfiguracyjnej.
Klucz ten może mieć wartość typu `boolean` lub `array`, gdzie poszczególne wartości są nazwami serwisów społecznościowych – lista wszystkich prekonfigurowanych serwisów znajduje się poniżej.
Opis działania konfiguracji:
  - `social_media_links => true` – w cms renderowane sa pola do wszystkich predefiniowanych mediów społecznościowych.
  - `social_media_links => false` – mechanizm generowania mediów społecznościowych nie jest aktywowany.
  - `social_media_links => array('facebook', 'youtube', 'twitter')` – w cms renderowane sa pola do serwisów podanych w tablicy czyli `facebook`, `youtube` oraz `twitter`.

Przykłady:
```php
\Superskrypt\WpBackendBase\WpBackendBase::setupBackend( 
    array( 
        'social_media_links' => true,
        ...pozostałe parametry
        )
    )
```
```php
\Superskrypt\WpBackendBase\WpBackendBase::setupBackend( 
    array( 
        'social_media_links' => array('facebook', 'youtube', 'twitter'),
        ...pozostałe parametry
        )
    )
```
Mechanizm pozwala na wyrenderowanie na stronie uzupełnionych w cms-e linków, w formie ikon. 

<div id="1.2"></div>

### Nadpisywanie szablonów i ikon
Ikony i szablon `/_partials/components/social_media_links.twig` można nadpisywać w motywie – w tym celu trzeba w motywie umieścić szablon lub ikonę tak, żeby ich ścieżka względna była taka jak w wp-backend-base.  
Nazwy ikon są stworzone według schematu `icon-nazwasocialmedia` np.: icon-facebook. `nazwasocialmedia` musi zgadzać się z nazwą przekazaną w tablicy konfiguracyjnej
Jeśli potrzebujemy dodać dodatkowy zestaw ikon lub nadpisać defaultowe, wówczas należy dodać plik `social-media-icons.svg` zawierający symbole ikon do folderu `images` w projekcie. Można skopiować plik z backend base i uzupełnić go o dodatkowe symbole ikon zgodnie z opisem w sekcji **"Dodawanie nowych mediów społecznościowych"**

<div id="1.3"></div>

### Dodawanie nowych mediów społecznościowych
Aby dodać nowy serwis społecznościowy trzeba:  
1. dodać nazwę tego serwisu do tablicy z listą aktualnych serwisów społecznościowych.
2. dodać kod svg ikony do pliku `images/social-media-icons.svg`. Należy pamiętać, aby dodać do symbolu nowej ikony id zgodnie z konwencją nazewniczą np.: `id="icon-nowy-serwis-społecznościowy"`.   
Dodatkowo, nazwa w tablicy konfiguracyjnej  powinna zgadzać się z drugim członem `id` ikony tj.: `nowy-serwis-społecznościowy`  
Przykład: `array('facebook', 'nowy-serwis-społecznościow')`;

<div id="1.4"></div>

### Lista prekonfigurowanych serwisów mediów społecznościowych  
`array('facebook', 'twitter', 'linkedin', 'youtube', 'instagram', 'medium', 'tiktok', )`;

<div id="1.5"></div>

### Integracja z motywem hooks

Jeśli nasz serwis jest wielojęzyczny wówczas trzeba mieć oddzielnego cms-a dla linków serwisów społecznościowych. W tym celu należy za pomocą filtrów zmodyfikować slug kontenera dla pól social links w cms oraz przekazać rodzaj aktualnego języka aby móc wygenerować te pola oddzielnie da różnych wersji językowych.

Filtry:
 - `bb_social_media_links_slug` – pozwala na modyfikację sluga dla kontenera z polami dla poszczególnych serwisów społecznościowych. Można dzięki niemu dodać suffix językowy do tego sluga.

 Przykłady.
 Wywołanie w theme lub w backend
 ```php
// Modyfiacja sluga 
add_filter('bb_social_media_links_slug', function($slug) {
    $lang = apply_filters( 'wpml_current_language', NULL );
    return $slug . '_' . $lang;
});
```

[Powrót do głównego pliku][main]

[main]: ../README.md 