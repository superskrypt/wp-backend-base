[Powrót do głównego pliku][main]

# SHORTCODES

Shortcody dostępne w ramach BackendBase można aktywować poprzez przekazania klucza `shortcodes` w tablicy konfiguracyjnej., do której przekazuje sie **slugi** tych shortcodów.

Przykład.
```php
\Superskrypt\WpBackendBase\WpBackendBase::setupBackend( 
    array( 
        ...
        'shortcodes' => array('lang'),
        ...
    )
);
```

## Shortcodes - konwencja mechanizmu.

Mechanizm definiowania shotrcodów w BackendBase opiera się na następującej konwencji:  
  - poszczególne definicje shortcodów umieszczone są w katalogu `/php/Shortcodes`
  - nazwy plików z definicjami shortcodów mają postać `SLUG_SHORTCODEE`__`shortcode.php`.
  - każdy plik z definicją shortcode oprócz funkcji powinien zawierać wordpressową funkcję rejestracji tego shortcode w wordpressie eg. `add_shortcode( 'lang', 'langShortCode' );`
  - po utworzeniu pliku shortcode w BackendBase należ dodać jego slug do tablicy `REGISTERED_SHORTCODES` znajdującej się w klacie `Superskrypt\WpBackendBase\Shortcodes`. Będzie wówczas dostępny jako default, gdy podczas wywołania zamiast tablicy slugów ustawi sie watrość `true`.
  Przykład
  ```php
    \Superskrypt\WpBackendBase\WpBackendBase::setupBackend( 
        array( 
            ...
            'shortcodes' => true,
            ...
        )
    );
  ```  


[main]: ../README.md 