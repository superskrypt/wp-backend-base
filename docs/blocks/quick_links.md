[Powrót do głównego pliku][main]  
[Powrót do listy bloków][blocks]  
# Blok Quick Links  

* typ: complex
* domyślne opcje bloku (block_options)): brak
* domyślne pola: text, btn_label, url

[Powrót do listy bloków][blocks]  
[Powrót do głównego pliku][main]  

[main]: ../../README.md 
[blocks]: ../structure_generator.md#bloki