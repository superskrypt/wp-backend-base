[Powrót do głównego pliku][main]  
[Powrót do listy bloków][blocks] 
# Blok Big Links  

* typ: complex
* domyślne opcje bloku (block_options)): "columns" - możliwość wyboru pomiędzy układem 2 lub 3 kolunowym (można wyłączyć)
* domyślne pola: image, title, text, button_label, button_url


[Powrót do listy bloków][blocks]  
[Powrót do głównego pliku][main]  

[main]: ../../README.md 
[blocks]: ../structure_generator.md#bloki