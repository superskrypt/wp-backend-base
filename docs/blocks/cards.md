[Powrót do głównego pliku][main]  
[Powrót do listy bloków][blocks]  
# Blok Cards

* typ: complex
* domyślne opcje bloku (block_options)): "columns" - możliwość wyboru pomiędzy układem 2 lub 3 kolunowym (można wyłączyć)
* domyślne pola: bg_color, image, tagline, title, content, button_text,

[Powrót do listy bloków][blocks]  
[Powrót do głównego pliku][main]  

[main]: ../../README.md 
[blocks]: ../structure_generator.md#bloki