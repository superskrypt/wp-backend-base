[Powrót do głównego pliku][main]  
[Powrót do listy bloków][blocks]  
# Blok CTA

* typ: standard
* domyślne opcje bloku (block_options)): brak
* domyślne pola: title, text, href, button_text

[Powrót do listy bloków][blocks]  
[Powrót do głównego pliku][main]  

[main]: ../../README.md 
[blocks]: ../structure_generator.md#bloki