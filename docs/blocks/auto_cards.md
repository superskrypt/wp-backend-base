[Powrót do głównego pliku][main]  
[Powrót do listy bloków][blocks]

# Blok Auto Cards

#### auto_cards
* typ: complex
* wygląd i konstrukcja html bloku opiera się na schemacie istniejących bloków złożonych, np. cards lub big-links.
* Treści poszczególnych elementów bloku (np. kart) nie są bezpośrednio edytowane w panelu w miejscu wystąpienia danego bloku. Zamiast tego treści pobierane są z istniejących postów wybranego typu. Posty, które mają być pokazywane w dla każdego wystąpienia bloku auto-cards mogą być inaczej odfiltrowywane.
* Konfiguracja danego typu bloku według schematu *"*auto_cards*"* odbywa się przy definicji struktury w polu 'settings/auto_cards' (jak w poniższym przykładzie), które ma postać wielopoziomowej tabelicy i nadpisuje domyślną tabelę zdefiniowaną w WP Backend Base w pliku TemplateStructureGenerator/StructureSchema.php
* Przy definicji typu bloku według schematu *"auto_cards"* (w poniższym przykładzie typ *"news"*) podaje się domyślne parametry dla wszystkich instancji danego typu bloku. Żeby któryś z parametrów mógł być konfigurowalny w panelu (czyli żeby każda instancja danego typu bloku mogła mieć inne ustawienia) należy bezpośrednio w tablicy settings dodać parametr o kluczu *'additional_fields'* i wartości w postaci funkcji zwracającej closure z tablicą definicji pól Carbon Fields.


```php
public static function getPageStructure() {
    return [
        "section_blocks" => array(

'news' => array(
    'use_schema' => 'auto_cards',
    'settings' => array(
        'complex' => false,
        'layout' => 'tabbed-vertical',
        'block_label' => 'News',
        'show_descendants' => true,
        'additional_fields' => self::getNewsFields(),

        'auto_cards' => array(
            'post_type' => 'post', // typ postów, które mają być wyświetlane przez blok. Dopuszczalne wartośc: post, page, albo nazwa custom posta
            'taxonomies' => array('category'=>array()), // tablica, w której kluczami są nazwy taksonomii przypisanych do danego typu posta, a wartościami tablice termów z danych taksonomii, które post musi mieć przypisane, żeby został wyświetlony w tym bloku
            'taxonomy_relation' => 'sum', // relacja między wynikami wyborów z poszczególnych taksonomii. Dopuszczalne wartości: sum (posiada dowolny z listy termów z dowolnej taksonomii) oraz intersection (w każdej taksonomii musi posiadać przynajmniej jeden term z listy)
            'sort' => array( // zasada sortowania wyników
                'type' => 'date', // typ sortowania. Dostępne wartości: date, text, number
                'field' => 'created', // pole, po którym posty mają być sortowane. Tu należy podać nazwę pola, które jest kluczem w poniszej tablicy 'fields', czyli np. 'created', a nie 'post/post_date
                'direction' => 'UP' // kierunek sortowania. Dopuszczalne wartości: UP, DOWN
                // UP dla date oznacza od starszego do nowszego
                // UP dla text oznacza od A do Z
                // UP dla number oznacza od najniszego do najwyższego
            ),
            'limit' => array( // tablica z listą ograniczeń dla wyszukanych postów
                array(
                    'type' => 'date', // bool, text, number
                    'field' => 'created',// pole, według którego posty mają być filtrowane. Tu należy podać nazwę pola, które jest kluczem w poniszej tablicy 'fields', czyli np. 'created', a nie 'post/post_date
                    'lower_limit' => 'today',
                    'upper_limit' => '+3 months'
                )
            ),
            'max' => -1, // maksymalna liczba zwróconych postów. -1 oznacza brak limitu
            'fields' => array( // mapowanie pól związanych z wyszukanymi postami na pola wyświetlane w bloku 
                'title' => 'post/post_title', // dane wzięte z tablicy MySQL 'wp_posts'
                'image' => 'function/randomImage', // dane wygenerowane przez podaną funkcję, do której jest przekazywany obiekt post
                'created' => 'post/post_date',
                'tagline' => 'taxonomy/category', // tablica terms przypisanych danemu postowi, wzięta z podanej taksonomii. Uwaga! Dla taksonomii, które mają w nazwie słowo 'taxonomy' trzeba je także podać
                'content' => 'postmeta/intro_lead', // dane wzięte z tablicy postmeta
                'button_text' => '',
                'url' => 'carbon/hero_product_url' // dane wzięte z pola Carbon Fields
            ),

        ),
    ),
)

        )
    ]
}

```
* Definiowanie połączeń w TemplateStructure.php wygląda następująco:
  * nazwa przypisana do pola oznacza, z jakiego źródła i według jakiej nazwy ma być pobrana docelowa wartość.
  * jeśli wartość ma być pobrana z któregoś z pól w wordpressowej MySQL-owej tabeli wp_post, to powinien być dodany przedrostek 'post/'
  * jeśli wartość ma być pobrana z pola carbon field, to przed jego nazwą powinien być dodany przedrostek 'carbon/'
  * jeśli wartość ma być pobrana z tabeli wp_postmeta, to przed nazwą pola postmeta powinien być dodany przedrostek 'postmeta/'
  * jeśli wartość ma być wygenerowana przez funkcję, to powinien być dodany przedrostek 'function/' i nazwa globalnie dostępnej funkcji. Do funkcji jest przekazywany cały obiekt post

* Filtrowanie (ustawianie limitów):
    * filtrowanie odbywa się na podstawie zawartości pola podanego dla klucza *'field'*. nazwa pola powinna być wzięta z klucza wartości tablicy podanej dla klucza *'fields'*.
    * jeśli jako typ filtrowania jest podana wartość *'bool'*, to pola *'lower_limit'* i *'higher_limit'* powinny mieć wartość *'true'* lub *'false'*.
    * jeśli jako typ filtrowania jest podana wartość *'number'*, to pola *'lower_limit'* i *'higher_limit'* powinny mieć wartości liczbowe.
    * jeśli jako typ filtrowania jest podana wartość *'date'*, to pola *'lower_limit'* i *'higher_limit'* powinny mieć wartości zrozumiałe dla [funkcji PHP strtotime][strtotime].
    * jeśli jako typ filtrowania jest podana wartość *'text'*, lub jakakolwiek wyżej nie wymieniona to pola *'lower_limit'* i *'higher_limit'* powinny mieć wartości tekstowe, które będą porównywane przy pomocy [funkcji PHP strcasecmp][strcasecmp].
    * jeśli pola *'lower_limit'* i/lub *'higher_limit'* są edytowalne w panelu (tzn. powiązane z nimi pola zostały dodane w funkcji podanej przy *"additional_fields"*), to trzeba pamiętać, żeby lista wartości do wyboru zgadzała się z typem filtrowania według powyższych wytycznych.

* Tworzenie pól do dostosowywania instancji bloku danego typu w panelu
    * funkcja podana w konfiguracji w polu *"additional_fields"* powinna zwracać closure typowy dla Carbon Fields.
    * żeby pole wyświetlone w panelu zmieniało którąś wartość konfiguracyjną dla instancji bloku, trzeba podać umiejscowienie tej wartości w opisanej powyżej tablicy konfiguracyjnej. Umiejscowienie podane jest w drugim parametrze metody \Carbon_Fields\Field::make i składa się z kolejnych kluczy w tablicy konfiguracji zaczynając od 'settings'. Klucze są rozdzielone podwójnym minusem (--).

```php
    private static function getNewsFields(){
        return function() {
            return array(
                \Carbon_Fields\Field::make( 'association', 'settings--auto_cards--limit--0--lower_limit', __( 'News shown' ) )
                ->set_types( array(
                    array(
                        'type'      => 'post',
                        'post_type' => 'product',
                    )
                ) )
                ->set_min(1),
            );
        };
    }
```

[Powrót do listy bloków][blocks]  
[Powrót do głównego pliku][main]  

[main]: ../../README.md 
[blocks]: ../structure_generator.md#bloki
[strtotime]: https://www.php.net/manual/en/function.strtotime.php
[strcasecmp]: https://www.php.net/manual/en/function.strcasecmp