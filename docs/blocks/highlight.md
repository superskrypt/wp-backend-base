[Powrót do głównego pliku][main]  
[Powrót do listy bloków][blocks]  
# Blok Highlight

* typ: standard
* domyślne opcje bloku (block_options)): brak
* domyślne pola: text, caption

[Powrót do listy bloków][blocks]  
[Powrót do głównego pliku][main]  

[main]: ../../README.md 
[blocks]: ../structure_generator.md#bloki
