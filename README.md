# Wp Backend Base
## Spis treści.
1. [Instalacja i konfiguracja Wp Backend Base][config]
2. [Structure generator][structure_generator]  
    2.1 [Inicjalizacja][structure_generator_init]  
    2.2 [Definiowanie struktury  (tu dodać Pełen przykład zdefiniowanej struktury)][structure_generator_structure]  
    2.3 [Hero][structure_generator_hero]  
    2.4 [Primary Content][structure_generator_primary_content]   
    2.6 [Sekcje – konfiguracja][structure_generator_sections]  
    2.7 [Bloki][structure_generator_blocks]  
    2.8 [Szablony twig][structure_generator_templates] 
3. [Theme Options][theme_options]
4. [Komponenty][components]  
    5.1 [Social media links][components_social_links] 
5. [Domyślne style][default_styles]
6. [Moduły JS w Wp Backend Base][js_modules]


[config]: ./docs/setup.md
[structure_generator]: ./docs/structure_generator.md

[structure_generator_init]: ./docs/structure_generator.md#1
[structure_generator_structure]: ./docs/structure_generator.md#2
[structure_generator_hero]: ./docs/structure_generator.md#2.3
[structure_generator_primary_content]: ./docs/structure_generator.md#2.4
[structure_generator_sections]: ./docs/structure_generator.md#2.5
[structure_generator_blocks]: ./docs/structure_generator.md#2.6
[structure_generator_templates]: ./docs/structure_generator.md#2.7


[theme_options]: ./docs/theme_options.md
[components]: ./docs/components.md
[components_social_links]: ./docs/components.md#1

[default_styles]: ./docs/default_styles.md
[js_modules]: ./docs/js_modules.md